
puremvc.define(
    // CLASS INFO
    {
        name: 'helper.GameHelper'
    },

    // INSTANCE MEMBERS
    {



    },

    // STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */

        removeItem: function(arr, item) {
            //removes specific element from array
            for (var i = arr.length; i--;) {
                if (arr[i] === item) {
                    arr.splice(i, 1);
                }
            }
        },

        cache: function(mc,cacheUpdate,offset) {

            if(!offset)offset=5;

            if(cacheUpdate == true)
            {
                mc.updateCache(-mc.getBounds().width/2-offset,-mc.getBounds().height/2-offset,mc.getBounds().width+offset,mc.getBounds().height+offset);
            }
            else
            {
                mc.cache(-mc.getBounds().width/2-offset,-mc.getBounds().height/2-offset,mc.getBounds().width+offset,mc.getBounds().height+offset);
            }
        },

        setPos: function(mc,posX,posY,twn) {

            mc.x = posX;
            mc.y = posY;

        },

        deactivateObject: function(mc) {
            mc.mouseEnabled = false;
            mc.mouseChildren = false;

        },

        activateObject: function(mc) {
            mc.mouseEnabled = true;
            mc.mouseChildren = true;

        },

        setPivot: function(mc,pivotX,pivotY) {
            mc.regX = pivotX;
            mc.regY = pivotY;

        },


        scaleObject: function(mc, toScale) {
            mc.scaleX = toScale;
            mc.scaleY = toScale;

        },

        pauseAll: function(mcArr) {
            for(var j=0;j<mcArr.length;j++)
            {
                var mc = mcArr[j];
                console.log(mc);
                if(mc.stop)mc.stop();
                for(var i=0;i<mc.getNumChildren();i++)
                {
                    var child = mc.getChildAt(i);
                    child.stop();
                }
            }
        },

        resumeAll: function(mcArr) {

            for(var j=0;j<mcArr.length;j++)
            {
                var mc = mcArr[j];
                if(mc.play) mc.play();
                for(var i=0;i<mc.getNumChildren();i++)
                {
                    var child = mc.getChildAt(i);
                    child.play();

                }
            }
        },

        getBitmapTextField: function(width,height,text,font,size,color,hAlign,vAlign,autoscale) {

            autoscale =  autoscale || true;
            var bfTxt = new BitmapTextField(width,height,text,font,size,0,0,hAlign,vAlign,autoscale);
            bfTxt.text = text;
            if(color)bfTxt.setColor(color);
            return bfTxt;
        },

        setBitmapText:function(txtField,text){
            txtField.text = text;
            txtField.setText(text);

        },

        getTextField: function(text, size, font, color,align) {

            //color -> #ffffff
            var panelTxt = new createjs.Text(text, size + "px " + font, color);
            panelTxt.regX= panelTxt.getBounds().width/2;
            panelTxt.regY= panelTxt.getBounds().height/2;
            if(align){
                panelTxt.textAlign = align;
            }
            else
            {
                panelTxt.textAlign = "center";
            }
            return panelTxt;
        },

        setFontStyle: function(obj, font, size, color) {
            // body...
            obj.font = size + "px " + font;
            obj.color = color;

        },

        mobilecheck: function() {
            var check = false;
            (function(a) {
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true
            })(navigator.userAgent || navigator.vendor || window.opera);
            return check;

        },

        remove: function(obj) {
            // body...
            if (obj && obj.parent) {
                obj.parent.removeChild(obj);
                obj = null;
            }
        },

        removeAll: function(argument) {

            var mc;
            if (argument) {
                mc = argument;
            } else {
                mc = AppConstants.STAGE_REF;
                //clear canvas rect
                AppConstants.CONTEXT.clearRect(0, 0, AppConstants.CANVAS_WIDTH, AppConstants.CANVAS_HEIGHT);

            }

            for (var j = 0; j < mc.getNumChildren(); j++) {
                var obj = mc.getChildAt(j);
                helper.GameHelper.remove(obj);
            }

        },


        playSound: function(id, loop) {
            createjs.Sound.play(id, createjs.Sound.INTERRUPT_NONE, 0, 0, loop);
        },


        addEventListener: function(target, eventName, handler) {
            target.addEventListener(eventName, handler);
        },


        dispatchEvent: function(eventName, obj) {
            var evt = new createjs.Event(eventName);
            evt.data = obj;
            AppConstants.STAGE.dispatchEvent(evt);

        },

        getQueryVariable: function(variable) {
            //Parse Query String in htlm url
            var qs = URI(window.location.href).query(true);
            return qs[variable];
        },

        xmlLoader: function(xmlPath) {
            //load xml from xmlPath and returns config
            var xmlhttp=null;
            if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.open("GET",xmlPath, false);
            xmlhttp.send();
            return xmlhttp.responseXML;


        },

        shuffle:function(array){

                var currentIndex = array.length
                    , temporaryValue
                    , randomIndex
                    ;

                // While there remain elements to shuffle...
                while (0 !== currentIndex) {

                    // Pick a remaining element...
                    randomIndex = Math.floor(Math.random() * currentIndex);
                    currentIndex -= 1;

                    // And swap it with the current element.
                    temporaryValue = array[currentIndex];
                    array[currentIndex] = array[randomIndex];
                    array[randomIndex] = temporaryValue;
                }

                return array;

        }





    }
);
