puremvc.define(
    // CLASS INFO
    {
        name: 'helper.LevelHelper'
    },

    // INSTANCE MEMBERS
    {



    },

    // STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */


        getEqQuestion: function(type) {

            var distractors=[]; //distractors-arr : distractors[0]->step1 , distractors[1]->step2
            var maxP = 12;
            var maxQ = 10;
            var maxX = 20;
            var randIndex,arrX,step1,step2;
            var quesObj,eq,P,Q,R,X;
            var ca1,ca2;
            P =  parseInt(2 + (Math.random()*(maxP-1)));
            Q = parseInt(1 + Math.random()*maxQ);
            X = parseInt(1 + Math.random()*maxX);

            var expr= helper.EvalHelper.eval;

            switch(type) {
                case 'A':
                    //type : Px+Q=R
                    R = parseInt(P*X + Q);
                    eq = P+'x+'+Q+'='+R;

                    step1 = [' + '+Q,'*'+Q,'@'+Q,'*1/'+Math.abs(Q),'-1/'+Math.abs(Q),'+'+P,'-'+P,'*'+P,'+1/'+Math.abs(P),'-1/'+Math.abs(P),'+(-'+Math.abs(P)+')','+'+Math.abs(R),'-'+Math.abs(R),'+(-'+Math.abs(R)+')','-(+'+Math.abs(R)+')','-(-'+Math.abs(R)+')','*'+R,'@'+R,'+1/'+Math.abs(R),'- 1/'+Math.abs(R)];
                    step2 = ['+'+P,'-'+P,'*'+P,'+1/'+Math.abs(P),'-1/'+Math.abs(P),'+(-'+Math.abs(P)+')','+'+expr(R-Q),'-'+expr(R-Q),'*'+expr(R-Q),'*1/'+Math.abs(expr(R-Q)),'*'+expr(P+R-Q),'@'+Math.abs(expr(P+R-Q)),'+1/'+Math.abs(expr(P+R-Q))];

                    ca1 = '-'+Q;
                    ca2 = '@'+P;


                    distractors[0]= step1+"";
                    distractors[1]= step2+"";

                    break;
                case 'B':

                    //type : Px-Q=R
                    R = parseInt(P*X - Q);
                    eq = P+'x-'+Q+'='+R;

                    step1 = ['+'+Q,'*'+Q,'@'+Q,'*1/'+Math.abs(Q),'-1/'+Math.abs(Q),'+'+P,'-'+P,'*'+P,'+1/'+Math.abs(P),'-1/'+Math.abs(P),'+(-'+Math.abs(P)+')','+'+Math.abs(R),'-'+Math.abs(R),'+(-'+Math.abs(R)+')','-(+'+Math.abs(R)+')','-(-'+Math.abs(R)+')','*'+R,'@'+R,'+1/'+Math.abs(R),'- 1/'+Math.abs(R)];
                    step2 = ['+'+P,'-'+P,'*'+P,'+1/'+Math.abs(P),'-1/'+Math.abs(P),'+(-'+Math.abs(P)+')','+'+expr(R+Q),'-'+expr(R+Q),'*'+expr(R+Q),'*1/'+Math.abs(expr(R+Q)),'*'+expr(P+R+Q),'@'+expr(P+R+Q),'+1/'+Math.abs(expr(P+R+Q))];
                    ca1 = '+'+Q;
                    ca2 = '@'+P;


                    distractors[0]= step1+"";
                    distractors[1]= step2+"";

                    break;
                case 'C':
                    //type : x+Q=R
                    R = parseInt(X + Q);
                    eq = 'x+'+Q+'='+R;

                    step1 = ['+'+Q,'*'+Q,'@'+Q,'*1/'+Math.abs(Q),'-1/'+Math.abs(Q),'+'+Math.abs(R),'-'+Math.abs(R),'+(-'+Math.abs(R)+')','-(+'+Math.abs(R)+')','-(-'+Math.abs(R)+')','*'+Math.abs(R),'@'+Math.abs(R),'*1/'+Math.abs(R),'@1/'+Math.abs(R)];
                    ca1 = '-'+Q;
                    distractors[0]= step1+"";

                    break;
                case 'D':
                    //type : x-Q=R
                    R = parseInt(X - Q);
                    eq = 'x-'+Q+'='+R;

                    step1 = ['+'+Q,'*'+Q,'@'+Q,'*1/'+Math.abs(Q),'+1/'+Math.abs(Q),'-1/'+Math.abs(Q),'+'+Math.abs(R),'-'+Math.abs(R),'+(-'+Math.abs(R)+')','-(+'+Math.abs(R)+')','-(-'+Math.abs(R)+')','@'+Math.abs(R),'*1/'+Math.abs(R),'@1/'+Math.abs(R)];
                    ca1 = '+'+Q;

                    distractors[0]= step1+"";

                    break;
                case 'E':
                    //type : Px=R

                    R = parseInt(P*X);
                    eq = P+'x='+R;
                    step1 = ['+'+P,'-'+P,'*'+P,'@1/'+Math.abs(P),'+1/'+Math.abs(P),'-1/'+Math.abs(P),'+'+Math.abs(R),'-'+Math.abs(R),'*'+Math.abs(R),'@'+Math.abs(R),'+1/'+Math.abs(R),'-1/'+Math.abs(R),'*1/'+Math.abs(R),'@1/'+Math.abs(R)];
                    ca1 = '@'+P;

                    distractors[0]= step1+"";

                    break;
                case 'F':
                    //type : x/Q=R
                    Q = (Q == 1) ? 2: Q;

                    arrX= [];
                    for(var i=0;i<parseInt(maxX/Q);i++)
                    {
                        arrX.push(parseInt(Q*(i+1)));

                    }

                    randIndex = parseInt(Math.random()*arrX.length);
                    R = parseInt(randIndex +1);
                    X = arrX[randIndex];

                    eq = 'x/'+Q+'='+R;

                    step1 = ['+'+Q,'-'+Q,'@'+Q,'*1/'+Q,'+1/'+Q,'-1/'+Q,'+'+Math.abs(R),'-'+Math.abs(R),'*'+Math.abs(R),'@'+Math.abs(R),'+1/'+Math.abs(R),'-1/'+Math.abs(R),'*1/'+Math.abs(R),'@1/'+Math.abs(R)];
                    ca1 = '*'+Q;

                    distractors[0]= step1+"";

                    break;
                case 'G':
                    //type : x/Q+P=R
                    Q = (Q == 1) ? 2: Q;

                    arrX= [];
                    for(var j=0;j<parseInt(maxX/Q);j++)
                    {
                        arrX.push(parseInt(Q*(j+1)));

                    }

                    randIndex = parseInt(Math.random()*arrX.length);
                    R = P + parseInt(randIndex +1);
                    X = arrX[randIndex];
                    eq = 'x/'+Q+'+'+P+'='+R;


                    step1 = ['+'+Q,'-'+Q,'+(-'+Math.abs(Q)+')','*1/'+Q,'+1/'+Q,'-1/'+Q,'+'+P,'-(-'+Math.abs(P)+')','*'+P,'@'+P,'*1/'+P,'-1/'+P,'+'+Math.abs(R),'-'+Math.abs(R),'*'+Math.abs(R),'@'+Math.abs(R),'+1/'+Math.abs(R),'-1/'+Math.abs(R)];
                    step2 = ['+'+Q,'-'+Q,'@'+Q,'*1/'+Q,'+1/'+Q,'-1/'+Q,'+('+expr(R-P)+')','-('+expr(R-P)+')','+(-'+Math.abs(expr(R-P))+')','-(-'+Math.abs(expr(R-P))+')','*('+expr(R-P)+')','@('+expr(R-P)+')',expr(R-P) > 0 ? '+1/'+Math.abs(expr(R-P)) : '-1/'+Math.abs(expr(R-P)),expr(R-P) > 0 ? '-1/'+expr(R-P) : '+1/'+Math.abs(expr(R-P)),expr(R-P) > 0 ? '*1/'+expr(R-P) : '*-1/'+Math.abs(expr(R-P))];
                    ca1 = '-'+P;
                    ca2 = '*'+Q;

                    distractors[0]= step1+"";
                    distractors[1]= step2+"";

                    break;
                case 'H':
                    //type : x/Q-P=R
                    Q = (Q == 1) ? 2: Q;

                    arrX= [];
                    for(var k=0;k<parseInt(maxX/Q);k++)
                    {
                        arrX.push(parseInt(Q*(k+1)));

                    }

                    randIndex = parseInt(Math.random()*arrX.length);
                    R = parseInt(randIndex +1) - P;
                    X = arrX[randIndex];
                    eq = 'x/'+Q+'-'+P+'='+R;

                    step1 = ['+'+Q,'-'+Q,'+(-'+Math.abs(Q)+')','*1/'+Q,'+1/'+Q,'-1/'+Q,'+'+P,'-(-'+Math.abs(P)+')','*'+P,'@'+P,'*1/'+P,'-1/'+P,'+'+Math.abs(R),'-'+Math.abs(R),'*'+Math.abs(R),'@'+Math.abs(R),'+1/'+Math.abs(R),'-1/'+Math.abs(R)];
                    step2 = ['+'+Q,'-'+Q,'@'+Q,'*1/'+Q,'+1/'+Q,'-1/'+Q,'+('+expr(R-P)+')','-('+expr(R-P)+')','+(-'+Math.abs(expr(R-P))+')','-(-'+Math.abs(expr(R-P))+')','*('+expr(R-P)+')','@('+expr(R-P)+')',expr(R-P) > 0 ? '+1/'+expr(R-P) : '-1/'+Math.abs(expr(R-P)),expr(R-P) > 0 ? '-1/'+expr(R-P) : '+1/'+Math.abs(expr(R-P)),expr(R-P) > 0 ? '*1/'+expr(R-P) : '*-1/'+Math.abs(expr(R-P))];
                    ca1 = '+'+P;
                    ca2 = '*'+Q;


                    distractors[0]= step1+"";
                    distractors[1]= step2+"";

                    break;
            }

            console.log('P,Q,R,X,eq >',P,Q,R,X,eq);
            quesObj= {};
            quesObj.text = eq;
            quesObj.distractors = distractors;
            quesObj.ca1 = ca1;
            quesObj.ca2 = ca2;

            return quesObj;
        },

        getMcqQuestion: function(type) {

            var distractors=[]; //distractors-arr : distractors[0]->step1 , distractors[1]->step2
            var maxP = 10;
            var maxQ = 10;
            var maxR = 10;
            var maxS = 10;
            var quesObj,eq,P,Q,R,S,randArr,steps;
            var ca1,ca2,ca3,ca4,qtexts;
            console.log('getQuestion',type);
            switch(type) {
                case 'A':
                    //type : P + Q + R  //range 1-10
                    var min =1;
                    var max=10;
                    P =  this.getRandNumber(min,max);
                    Q =  this.getRandNumber(min,max);
                    while(Q == P)Q = this.getRandNumber(min,max);

                    if(AppConstants.GRADE == '1')
                    {
                        min = 1;
                        max = 20 - parseInt(P+Q);
                    }

                    console.log('min,max',min,max);
                    R =  this.getRandNumber(min,max);
                    while(R == Q || R == P)R = this.getRandNumber(min,max);


                    randArr =[];
                    for(var i=min;i<=max;i++)
                    {
                        if(i != P && i!=Q && i!=R)
                        {
                            randArr.push(i);
                        }
                    }

                    eq = P+' + '+Q+' + '+R;


                    ca1 = [P+' + '+Q,Q+' + '+P,Q+' + '+R,R+' + '+Q,P+' + '+R,R+' + '+P];
                    ca2 = {};
                    ca2[P+' + '+Q] = ca2[Q+' + '+P] = [parseInt(P+Q)+' + '+R,R+' + '+parseInt(P+Q)];
                    ca2[Q+' + '+R] = ca2[R+' + '+Q] = [parseInt(Q+R)+' + '+P,P+' + '+parseInt(Q+R)];
                    ca2[P+' + '+R] = ca2[R+' + '+P] = [parseInt(P+R)+' + '+Q,Q+' + '+parseInt(P+R)];
                    steps = 2;

                    distractors={};
                    distractors[0] = [ parseInt(P+1)+' + '+Q, parseInt(P+1)+' + '+R,P+' + '+parseInt(Q+1), P+' + '+parseInt(R+1),Q+' + '+parseInt(P+1), R+' + '+parseInt(P+1),parseInt(Q+1)+' + '+P, parseInt(R+1)+' + '+P,parseInt(Q+1)+' + '+R,Q+' + '+parseInt(R+1),parseInt(R+1)+' + '+Q];
                    distractors[1] ={};
                    distractors[1][parseInt(P+Q)+' + '+R] = [ parseInt(P+1)+' + '+R	,R+' + '+parseInt(P+1),	parseInt(P+Q)+' + '+P,	P+' + '+parseInt(P+Q),parseInt(P+Q) +' + '+ parseInt(R+1),	parseInt(R+1) +' + '+ parseInt(P+Q)];
                    distractors[1][R+' + '+parseInt(P+Q)] = [ parseInt(Q+1)+' + '+R,	R+' + '+parseInt(Q+1)	,parseInt(P+Q)+' + '+Q	,Q+' + '+parseInt(P+Q),parseInt(P+Q) +' + '+ parseInt(R+1),	parseInt(R+1) +' + '+ parseInt(P+Q)];
                    distractors[1][parseInt(Q+R)+' + '+P] = [P +' + '+ parseInt(R+1),	parseInt(R+1) +' + '+ P,	parseInt(Q+R) +' + '+ R,	R +' + '+ parseInt(Q+R),P +' + '+ parseInt(Q+1),	parseInt(Q+1) +' + '+ P];
                    distractors[1][P+' + '+parseInt(Q+R)] = [parseInt(P+1) +' + '+ parseInt(Q+R)	,parseInt(Q+R) +' + '+ parseInt(P+1),	parseInt(Q+R) +' + '+ Q	,Q +' + '+ parseInt(Q+R) ,P +' + '+ parseInt(Q+1),	parseInt(Q+1) +' + '+ P];
                    distractors[1][parseInt(P+R)+' + '+Q] = [Q +' + '+ parseInt(R+1)	,parseInt(R+1) +' + '+ Q	,parseInt(P+R) +' + '+ parseInt(Q+1)	,parseInt(Q+1) +' + '+ parseInt(P+R) ,P +' + '+ parseInt(P+R),	parseInt(P+R) +' + '+ P];
                    distractors[1][Q+' + '+parseInt(P+R)] = [ Q +' + '+ parseInt(P+1)	,parseInt(P+1) +' + '+ Q,	parseInt(P+R) +' + '+ R	,R +' + '+ parseInt(P+R),P +' + '+ parseInt(P+R),	parseInt(P+R) +' + '+ P];

                    break;
                case 'B':

                    //type : P + Q + R  //range 5 - 30
                    var min =5;
                    var max=15;
                    P =  this.getRandNumber(min,max);
                    Q =  this.getRandNumber(min,max);
                    while(Q == P)Q = this.getRandNumber(min,max);
                    R =  this.getRandNumber(min,max);
                    while(R == Q || R == P)R = this.getRandNumber(min,max);

                    randArr =[];
                    for(var i=min;i<=max;i++)
                    {
                        if(i != P && i!=Q && i!=R)
                        {
                            randArr.push(i);
                        }
                    }

                    eq = P+' + '+Q+' + '+R;
                    ca1 = [P+' + '+Q,Q+' + '+P,Q+' + '+R,R+' + '+Q,P+' + '+R,R+' + '+P];
                    ca2 = {};
                    ca2[P+' + '+Q] = ca2[Q+' + '+P] = [parseInt(P+Q)+' + '+R,R+' + '+parseInt(P+Q)];
                    ca2[Q+' + '+R] = ca2[R+' + '+Q] = [parseInt(Q+R)+' + '+P,P+' + '+parseInt(Q+R)];
                    ca2[P+' + '+R] = ca2[R+' + '+P] = [parseInt(P+R)+' + '+Q,Q+' + '+parseInt(P+R)];
                    steps = 2;
                    distractors={};
                    distractors[0] = [ parseInt(P+1)+' + '+Q, parseInt(P+1)+' + '+R,P+' + '+parseInt(Q+1), P+' + '+parseInt(R+1),Q+' + '+parseInt(P+1), R+' + '+parseInt(P+1),parseInt(Q+1)+' + '+P, parseInt(R+1)+' + '+P,parseInt(Q+1)+' + '+R,Q+' + '+parseInt(R+1),parseInt(R+1)+' + '+Q];
                    distractors[1] ={};
                    distractors[1][parseInt(P+Q)+' + '+R] = [ parseInt(P+1)+' + '+R	,R+' + '+parseInt(P+1),	parseInt(P+Q)+' + '+P,	P+' + '+parseInt(P+Q),parseInt(P+Q) +' + '+ parseInt(R+1),	parseInt(R+1) +' + '+ parseInt(P+Q)];
                    distractors[1][R+' + '+parseInt(P+Q)] = [ parseInt(Q+1)+' + '+R,	R+' + '+parseInt(Q+1)	,parseInt(P+Q)+' + '+Q	,Q+' + '+parseInt(P+Q),parseInt(P+Q) +' + '+ parseInt(R+1),	parseInt(R+1) +' + '+ parseInt(P+Q)];
                    distractors[1][parseInt(Q+R)+' + '+P] = [P +' + '+ parseInt(R+1),	parseInt(R+1) +' + '+ P,	parseInt(Q+R) +' + '+ R,	R +' + '+ parseInt(Q+R),P +' + '+ parseInt(Q+1),	parseInt(Q+1) +' + '+ P];
                    distractors[1][P+' + '+parseInt(Q+R)] = [parseInt(P+1) +' + '+ parseInt(Q+R)	,parseInt(Q+R) +' + '+ parseInt(P+1),	parseInt(Q+R) +' + '+ Q	,Q +' + '+ parseInt(Q+R) ,P +' + '+ parseInt(Q+1),	parseInt(Q+1) +' + '+ P];
                    distractors[1][parseInt(P+R)+' + '+Q] = [Q +' + '+ parseInt(R+1)	,parseInt(R+1) +' + '+ Q	,parseInt(P+R) +' + '+ parseInt(Q+1)	,parseInt(Q+1) +' + '+ parseInt(P+R) ,P +' + '+ parseInt(P+R),	parseInt(P+R) +' + '+ P];
                    distractors[1][Q+' + '+parseInt(P+R)] = [ Q +' + '+ parseInt(P+1)	,parseInt(P+1) +' + '+ Q,	parseInt(P+R) +' + '+ R	,R +' + '+ parseInt(P+R),P +' + '+ parseInt(P+R),	parseInt(P+R) +' + '+ P];


                    break;
                case 'C': //6+
                    //type : P + Q + R  //range 1 - 30
                    var min =1;
                    var max=20;
                    P =  this.getRandNumber(min,max);
                    Q =  this.getRandNumber(min,max);
                    while(Q == P)Q = this.getRandNumber(min,max);
                    R =  this.getRandNumber(min,max);
                    while(R == Q || R == P)R = this.getRandNumber(min,max);

                    randArr =[];
                    for(var i=min;i<=max;i++)
                    {
                        if(i != P && i!=Q && i!=R)
                        {
                            randArr.push(i);
                        }
                    }

                    eq = P+' + '+Q+' + '+R;
                    ca1 = [P+' + '+Q,Q+' + '+P,Q+' + '+R,R+' + '+Q,P+' + '+R,R+' + '+P];
                    ca2 = {};
                    ca2[P+' + '+Q] = ca2[Q+' + '+P] = [parseInt(P+Q)+' + '+R,R+' + '+parseInt(P+Q)];
                    ca2[Q+' + '+R] = ca2[R+' + '+Q] = [parseInt(Q+R)+' + '+P,P+' + '+parseInt(Q+R)];
                    ca2[P+' + '+R] = ca2[R+' + '+P] = [parseInt(P+R)+' + '+Q,Q+' + '+parseInt(P+R)];
                    steps = 2;
                    distractors={};
                    distractors[0] = [ parseInt(P+1)+' + '+Q, parseInt(P+1)+' + '+R,P+' + '+parseInt(Q+1), P+' + '+parseInt(R+1),Q+' + '+parseInt(P+1), R+' + '+parseInt(P+1),parseInt(Q+1)+' + '+P, parseInt(R+1)+' + '+P,parseInt(Q+1)+' + '+R,Q+' + '+parseInt(R+1),parseInt(R+1)+' + '+Q];
                    distractors[1] ={};
                    distractors[1][parseInt(P+Q)+' + '+R] = [ parseInt(P+1)+' + '+R	,R+' + '+parseInt(P+1),	parseInt(P+Q)+' + '+P,	P+' + '+parseInt(P+Q),parseInt(P+Q) +' + '+ parseInt(R+1),	parseInt(R+1) +' + '+ parseInt(P+Q)];
                    distractors[1][R+' + '+parseInt(P+Q)] = [ parseInt(Q+1)+' + '+R,	R+' + '+parseInt(Q+1)	,parseInt(P+Q)+' + '+Q	,Q+' + '+parseInt(P+Q),parseInt(P+Q) +' + '+ parseInt(R+1),	parseInt(R+1) +' + '+ parseInt(P+Q)];
                    distractors[1][parseInt(Q+R)+' + '+P] = [P +' + '+ parseInt(R+1),	parseInt(R+1) +' + '+ P,	parseInt(Q+R) +' + '+ R,	R +' + '+ parseInt(Q+R),P +' + '+ parseInt(Q+1),	parseInt(Q+1) +' + '+ P];
                    distractors[1][P+' + '+parseInt(Q+R)] = [parseInt(P+1) +' + '+ parseInt(Q+R)	,parseInt(Q+R) +' + '+ parseInt(P+1),	parseInt(Q+R) +' + '+ Q	,Q +' + '+ parseInt(Q+R) ,P +' + '+ parseInt(Q+1),	parseInt(Q+1) +' + '+ P];
                    distractors[1][parseInt(P+R)+' + '+Q] = [Q +' + '+ parseInt(R+1)	,parseInt(R+1) +' + '+ Q	,parseInt(P+R) +' + '+ parseInt(Q+1)	,parseInt(Q+1) +' + '+ parseInt(P+R) ,P +' + '+ parseInt(P+R),	parseInt(P+R) +' + '+ P];
                    distractors[1][Q+' + '+parseInt(P+R)] = [ Q +' + '+ parseInt(P+1)	,parseInt(P+1) +' + '+ Q,	parseInt(P+R) +' + '+ R	,R +' + '+ parseInt(P+R),P +' + '+ parseInt(P+R),	parseInt(P+R) +' + '+ P];


                    break;

                case '1C1':
                    //type : 3 + (6 * 4)
                    eq = '3 + (6 * 4)';
                    ca1 = ['Mutliply\n6 * 4'];
                    ca2 = ['Add\n3 + 24'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Add\n3 + 6','Multiply\n3 * 6','Multiply\n3 * 4','Add\n4 + 6','Add\n6 + 4'];
                    distractors[1] = ['Multiply\n3 * 24', 'Subtract\n24 - 6', 'Subtract\n27 - 3'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Mutliply\n6 * 4'] = '3 + 24';
                    qtexts[1] ={};
                    qtexts[1]['Add\n3 + 24'] = '27';

                    break;
                case '1C2':
                    //type : 35 - (21 @ 7 )
                    eq = '35 - (21 @ 7 )';
                    ca1 = ['Divide\n21 @ 7'];
                    ca2 = ['Subtract\n35 - 3'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Multiply\n21 * 7', 'Add\n7 + 21', 'Subtract\n21 - 7', 'Subtract\n35 - 21', 'Divide\n7 @ 21'];
                    distractors[1] = ['Divide\n35 @ 3', 'Add\n3 + 35', 'Multiply\n35 * 3', 'Subtract\n3 - 35'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Divide\n21 @ 7'] = '35 - 3';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n35 - 3'] = '32';

                    break;
                case '1C3':
                    //type :
                    eq = '(11 - 5) * 3';
                    ca1 = ['Subtract\n11 - 5'];
                    ca2 = ['Multiply\n6 * 3'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Subtract\n11 - 3', 'Multiply\n5 * 3', 'Add\n11 + 5', 'Multiply\n11 * 3']
                    distractors[1] = ['Divide\n6 @ 3', 'Add\n6 + 3', 'Subtract\n6 - 3', 'Multiply\n3 * 3'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Subtract\n11 - 5'] = '6 * 3';
                    qtexts[1] ={};
                    qtexts[1]['Multiply\n6 * 3'] = 18;


                    break;
                case '1C4':
                    //type :
                    eq = '8 * (18 - 6)';
                    ca1 = ['Subtract\n18 - 6'];
                    ca2 = ['Multiply\n12 * 8'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Multiply\n8 * 18', 'Divide\n18 @ 6', 'Subtract\n18 - 6', 'Add\n8 + 12']
                    distractors[1] = ['Divide\n12 @ 8', 'Add\n12 + 8', 'Subtract\n12 - 8'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Subtract\n18 - 6'] = '12 * 8';
                    qtexts[1] ={};
                    qtexts[1]['Multiply\n12 * 8'] = 96;


                    break;
                case '1C5':
                    //type :
                    eq = '(15 @ 3) + 4';
                    ca1 = ['Divide\n15 @ 3'];
                    ca2 = ['Add\n5 + 4'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Divide\n3 @ 15', 'Multiply\n15 * 3', 'Subtract\n5 - 4', 'Subtract\n4 - 5']
                    distractors[1] = ['Divide\n5 @ 4', 'Add\n4 + 4', 'Subtract\n5 - 4', 'Multiply\n5 * 4'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Divide\n15 @ 3'] = '5 + 4';
                    qtexts[1] ={};
                    qtexts[1]['Add\n5 + 4'] = 9;


                    break;
                case '1C6':
                    //type :
                    eq = '12 + (20 @ 4)';
                    ca1 = ['Divide\n20 @ 4'];
                    ca2 = ['Add\n5 + 12'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Add\n20 + 12', 'Multiply\n20 * 4', 'Divide\n20 @ 12', 'Subtract\n4 - 12', 'Divide\n5 @ 12'];
                    distractors[1] = ['Divide\n5 @ 12', 'Add\n6 + 12', 'Subtract\n5 - 12', 'Multiply\n5 * 12'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Divide\n20 @ 4'] = '5 + 12';
                    qtexts[1] ={};
                    qtexts[1]['Add\n5 + 12'] = 17;


                    break;
                case '1C7':
                    //type :
                    eq = '(9 * 3) - 13';
                    ca1 = ['Multiply\n9 * 3'];
                    ca2 = ['Subtract\n27 - 13'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Multiply\n3 * 13', 'Subtract\n9 - 13', 'Subtract\n3 - 13', 'Subtract\n9 - 3', 'Multiply\n27 * 13', 'Add\n27 + 13'];
                    distractors[1] = ['Divide\n27 @ 13', 'Add\n27 + 13', 'Subtract\n27 - 14', 'Multiply\n27 * 13'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Multiply\n9 * 3'] = '27 - 13';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n27 - 13'] = 14;


                    break;
                case '1C8':
                    //type :
                    eq = '(10 * 4) - 14';
                    ca1 = ['Multiply\n4 * 10'];
                    ca2 = ['Subtract\n40 - 14'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Subtract\n10 - 14', 'Subtract\n4 - 14', 'Subtract\n14 - 14', 'Multiply\n40 * 14', 'Add\n40 + 14']
                    distractors[1] = ['Divide\n40 @ 14', 'Add\n40 + 14', 'Subtract\n4 - 14', 'Multiply\n40 * 14'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Multiply\n4 * 10'] = '40 - 14';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n40 - 14'] = 26;


                    break;
                case '1C9':
                    //type :
                    eq = '(16 - 4) @ 3';
                    ca1 = ['Subtract\n16 - 4'];
                    ca2 = ['Divide\n12 @ 3'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Multiply\n16 * 3', 'Divide\n3 @ 12', 'Divide\n4 @ 16', 'Subtract\n16 - 7', 'Add\n16 + 4', 'Multiply\n12 * 3']
                    distractors[1] = ['Divide\n3 @ 3', 'Add\n12 + 3', 'Subtract\n12 - 3', 'Multiply\n12 * 3'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Subtract\n16 - 4'] = '12 @ 3';
                    qtexts[1] ={};
                    qtexts[1]['Divide\n12 @ 3'] = 4;


                    break;
                case '1C10':
                    //type :
                    eq = '(19 - 1) @ 6';
                    ca1 = ['Subtract\n19 - 1'];
                    ca2 = ['Divide\n18 @ 6'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Multiply\n19 * 1', 'Multiply\n18 * 6', 'Add\n19 + 1', 'Subtract\n19 - 6', 'Subtract\n18 - 6']
                    distractors[1] = ['Divide\n18 @ 9', 'Add\n18 + 6', 'Subtract\n18 - 6', 'Multiply\n18 * 6'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Subtract\n19 - 1'] = '18 @ 6';
                    qtexts[1] ={};
                    qtexts[1]['Divide\n18 @ 6'] = 3;


                    break;
                case '1C11':
                    //type :
                    eq = '(22 @ 2) - 9';
                    ca1 = ['Divide\n22 @ 2'];
                    ca2 = ['Subtract\n11 - 9'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Divide\n22 @ 9', 'Multiply\n22 * 2', 'Divide\n2 @ 9', 'Subtract\n2 - 9', 'Add\n22 + 9', 'Divide\n11 @ 9'];
                    distractors[1] = ['Divide\n11 @ 9', 'Add\n11 + 9', 'Subtract\n9 - 11', 'Multiply\n11 * 9'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Divide\n22 @ 2'] = '11 - 9';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n11 - 9'] = 2;


                    break;
                case '1C12':
                    //type :
                    eq = '(27 @ 3) - 5';
                    ca1 = ['Divide\n27 @ 3'];
                    ca2 = ['Subtract\n9 - 5'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Divide\n27 @ 5', 'Divide\n5 @ 27', 'Divide\n3 @ 27', 'Subtract\n27 - 3', 'Multiply\n3 * 5', 'Subtract\n5 - 9', 'Add\n9 + 5', 'Divide\n9 @ 5']
                    distractors[1] = ['Subtract\n9 - 5'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Divide\n27 @ 3'] = '9 - 5';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n9 - 5'] = 4;


                    break;
                case '1C13':
                    //type :
                    eq = '10 + 10 * (8 + 7)';
                    ca1 = ['Add\n8 + 7'];
                    ca2 = ['Multiply\n10 * 15'];
                    ca3 = ['Add\n10 + 150']
                    steps = 3;
                    distractors={};
                    distractors[0] = ['Multiply\n8 * 7', 'Divide\n8 @ 7', 'Subtract\n8 - 7', 'Add\n7 + 7']
                    distractors[1] = ['Multiply\n1 * 15', 'Divide\n15 @ 10', 'Subtract\n10 - 15', 'Add\n10 + 15'];
                    distractors[2] = ['Multiply\n10 * 150','Subtract\n10 - 150', 'Subtract\n150 - 10', 'Add\n10 + 15', 'Add\n10 + 160', 'Add\n160 + 150'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Add\n8 + 7'] = '10 + 10 * 15';
                    qtexts[1] ={};
                    qtexts[1]['Multiply\n10 * 15'] = '10 + 150';
                    qtexts[2] ={};
                    qtexts[2]['Add\n10 + 150'] = 160;

                    break;
                case '1C14':
                    //type :
                    eq = '12 @ (1 - 3) - 3';
                    ca1 = ['Subtract\n1 - 3'];
                    ca2 = ['Divide\n12 @ (-2)'];
                    ca3 = ['Subtract\n-6 - 3'];
                    steps = 3;
                    distractors={};
                    distractors[0] = ['Multiply\n1 * 3', 'Divide\n12 @ (-3)', 'Divide\n1 @ 3', 'Add\n12 @ 3', 'Subtract\n1 - (-3)'];
                    distractors[1] = ['Divide\n12 @ 2', 'Subtract\n12 - (-2)', 'Subtract\n-2 - 3', 'Multiply\n12 * 2'];
                    distractors[2] = ['Subtract\n6 - 3', 'Add\n-6 + 3', 'Add\n6 + 3', 'Subtract\n-9 - 3', 'Subtract\n-6 - 9', 'Subtract\n9 - 3', 'Multiply\n-6 * -3', 'Subtract\n3 - 6'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Subtract\n1 - 3'] = '12 @ (-2) - 3';
                    qtexts[1] ={};
                    qtexts[1]['Divide\n12 @ (-2)'] = '-6 - 3';
                    qtexts[2] ={};
                    qtexts[2]['Subtract\n-6 - 3'] = -9;


                    break;
                case '1C15':
                    //type :
                    eq = '18 @ (17 - 2>)';
                    ca1 = ['Evaluate\n2>'];
                    ca2 = ['Subtract\n17 - 8'];
                    ca3 = ['Divide\n18 @ 9'];
                    steps = 3;
                    distractors={};
                    distractors[0] = ['Multiply\n2 * 3', 'Add\n2 @ 3', 'Subtract\n17 - 6', 'Divide\n18 @ 2>']
                    distractors[1] = ['Add\n18 + 17', 'Divide\n18 @ 8', 'Divide\n17 @ 8', 'Subtract\n18 - 8', 'Multiply\n17 * 8'];
                    distractors[2] = ['Divide\n9 @ 18', 'Divide\n18 @ 2', 'Multiply\n18 * 9', 'Subtract\n18 - 9', 'Add\n18 + 9', 'Multiply\n9 * 2', 'Divide\n2 @ 9'];

                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Evaluate\n2>'] = '18 @ (17 - 8)';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n17 - 8'] = '18 @ 9';
                    qtexts[2] ={};
                    qtexts[2]['Divide\n18 @ 9'] = 2;


                    break;
                case '1C16':
                    //type :
                    eq = '-20 + 3(x + 20)';
                    ca1 = ['E$pand\n3(x + 20)'];
                    ca2 = ['Add\n-20 + 60'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Add\n-20 + 20', 'Divide\nx @ 3', 'Add\n-20 + 3', 'Multiply\n20(x + 20)', 'Subtract\n20 - 20'];
                    distractors[1] = ['Add\n20 + (-60)', 'Divide\n60 @ 20', 'Subtract\n-20 - 60', 'Multiply\n3x'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['E$pand\n3(x + 20)'] = '-20 + 3x + 60';
                    qtexts[1] ={};
                    qtexts[1]['Add\n-20 + 60'] = '40 + 3x';


                    break;
                case '1C17':
                    //type :
                    eq = '9 + 66 @ (2 * 5 + 23)';
                    ca1 = ['Multiply\n2 * 5'];
                    ca2 = ['Add\n10 + 23'];
                    ca3 = ['Divide\n66 @ 33'];
                    ca4 = ['Add\n9 + 2'];
                    steps = 4;
                    distractors={};
                    distractors[0] = ['Add\n9 @ 66', 'Divide\n66 @ (2 * 5+23)', 'Multiply\n2 * 28'];
                    distractors[1] = ['Add\n9 + 66', 'Divide\n66 @ 33', 'Subtract\n10 - 23'];
                    distractors[2] = ['Add\n9 + 66', 'Add\n9 + 2', 'Divide\n9 @ 2', 'Multiply\n66 * 33'];
                    distractors[3] = ['Add\n9 + 31', 'Add\n-9 + 2', 'Subtract\n9 - 2', 'Multiply\n9 * 2', 'Add\n9 + 2', 'Subtract\n2 - 9'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Multiply\n2 * 5'] = '9 + 66 @ (10 + 23)';
                    qtexts[1] ={};
                    qtexts[1]['Add\n10 + 23'] = '9 + 66 @ 33';
                    qtexts[2] ={};
                    qtexts[2]['Divide\n66 @ 33'] = '9 + 2';
                    //qtexts[2]['9 + 2'] = 11;
                    qtexts[3] ={};
                    qtexts[3]['Add\n9 + 2'] = 11;

                    break;
                case '1C18':
                    //type :
                    eq = '13 + 50 @ (8 * 3 + 1)';
                    ca1 = ['Multiply\n8 * 3'];
                    ca2 = ['Add\n24 + 1'];
                    ca3 = ['Divide\n50 @ 25'];
                    ca4 = ['Add\n13 + 2'];
                    steps = 4;
                    distractors={};
                    distractors[0] = ['Add\n13 + 50', 'Divide\n50 @ 8', 'Add\n3 + 1', 'Divide\n50 @ 24', 'Subtract\n24 - 1', 'Divide\n50 @ 1', 'Divide\n63 @ 8', 'Divide\n63 @ 24', 'Multiply\n8 * 1', 'Add\n8 + 1'];
                    distractors[1] = ['Divide\n50 @ 24', 'Add\n13 + 50', 'Divide\n63 @ 24', 'Add\n13 + 50', 'Divide\n50 @ 1', 'Divide\n63 @ 1', 'Divide\n63 @ 25', 'Add\n13 + 25'];
                    distractors[2] = ['Divide\n25 @ 50', 'Add\n13 + 50', 'Add\n13 + 2', 'Multiply\n50 * 25'];
                    distractors[3] = ['Add\n13 + 15', 'Add\n-13 + 2', 'Subtract\n13 - 2', 'Multiply\n13 * 2', 'Add\n15 + 2', 'Subtract\n2 - 13'];

                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Multiply\n8 * 3'] = '13 + 50 @ (24 + 1)';
                    qtexts[1] ={};
                    qtexts[1]['Add\n24 + 1'] = '13 + 50 @ 25';
                    qtexts[2] ={};
                    qtexts[2]['Divide\n50 @ 25'] = '13 + 2';
                    //qtexts[2]['13 + 2'] = 15;
                    qtexts[3] ={};
                    qtexts[3]['Add\n13 + 2'] = 15;
                    break;
                case '1C19':
                    //type :
                    eq = '20 + 16 @ (3< - 1)';
                    ca1 = ['Evaluate\n3<'];
                    ca2 = ['Subtract\n9 - 1'];
                    ca3 = ['Divide\n16 @ 8'];
                    ca4 = ['Add\n20 + 2'];
                    steps = 4;
                    distractors={};
                    distractors[0] = ['Add\n3 + 2', 'Add\n20 + 16', 'Multiply\n16 * 3<', 'Divide\n16 @ 3<', 'Subtract\n2 - 1'];
                    distractors[1] = ['Add\n20 + 16', 'Divide\n16 @ 9', 'Multiply\n16 * 9', 'Divide\n9 @ 1', 'Subtract\n1 - 9', 'Subtract\n16 - 8'];
                    distractors[2] = ['Add\n20 + 16', 'Divide\n20 @ 8', 'Add\n16 + 8', 'Multiply\n16 * 8'];
                    distractors[3] = ['Add\n2 + 2', 'Add\n22 + 2', 'Add\n20 + 22', 'Subtract\n20 - 2', 'Subtract\n22 - 2', 'Multiply\n20 * 2', 'Subtract\n2 - 20'];

                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Evaluate\n3<'] = '20 + 16 @ (9 - 1)';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n9 - 1'] = '20 + 16 @ 8';
                    qtexts[2] ={};
                    qtexts[2]['Divide\n16 @ 8'] = '20 + 2';
                    //qtexts[2]['20 + 2'] = 22;
                    qtexts[3] ={};
                    qtexts[3]['Add\n20 + 2'] = 22;

                    break;
                case '1C20':
                    //type :
                    eq = '16 - 12 @ (15 - 3<)';
                    ca1 = ['Evaluate\n3<'];
                    ca2 = ['Subtract\n15 - 9'];
                    ca3 = ['Divide\n12 @ 6'];
                    ca4 = ['Subtract\n16 - 2'];
                    steps = 4;
                    distractors={};
                    distractors[0] = ['Add\n3 + 2', 'Add\n16 + 12', 'Multiply\n3 * 2', 'Divide\n3 @ 2', 'Divide\n12 @ 15', 'Subtract\n16 - 12', 'Evaluate\n4<'];
                    distractors[1] = ['Subtract\n16 - 9', 'Add\n15 + 9', 'Divide\n12 @ 15', 'Multiply\n12 * 6', 'Divide\n15 @ 9'];
                    distractors[2] = ['Divide\n24 @ 12', 'Add\n12 + 6', 'Divide\n12 @ 2', 'Multiply\n16 * 6', 'Subtract\n16 - 12'];
                    distractors[3] = ['Subtract\n2 - 16', 'Subtract\n-2 - 16', 'Add\n16 + 2', 'Multiply\n16 * 2', 'Multiply\n16 * -2', 'Subtract\n16 - 14', 'Subtract\n14 - 2', 'Subtract\n-16 - 2'];

                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Evaluate\n3<'] = '16 - 12 @ (15 - 9)';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n15 - 9'] = '16 - 12 @ 6';
                    qtexts[2] ={};
                    qtexts[2]['Divide\n12 @ 6'] = '16 - 2';
                    //qtexts[2]['16 - 2'] = 14;
                    qtexts[3] ={};
                    qtexts[3]['Subtract\n16 - 2'] = 14;

                    break;
                case '1C21':
                    //type :
                    eq = '3(x - 3) + 3';
                    ca1 = ['E$pand\n3(x - 3)'];
                    ca2 = ['Add\n-9 + 3'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Subtract\n3x - 9', 'Add\n3 + 3', 'Add\n(x - 3) + 3', 'Multiply\n3 * 3', 'Subtract\nx - 3', 'Subtract\n3 - x'];
                    distractors[1] = ['Multiply\n3x', 'Multiply\n3*(3)', 'Subtract\n3x - 9', 'Divide\n9 @ 3', 'Subtract\n-9 - 3', 'Add\n9 + (-3)', 'Add\n9 + 3'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['E$pand\n3(x - 3)'] = '3x - 9 + 3';
                    qtexts[1] ={};
                    qtexts[1]['Add\n-9 + 3'] = '3x - 6';


                    break;
                case '1C22':
                    //type :
                    eq = '5 + 7(5 - Y)';
                    ca1 = ['E$pand\n7(5 - Y)'];
                    ca2 = ['Add\n5 + 35'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Subtract\n5 - Y', 'Add\n5 + 7','Add\n7 + 5', 'Add\n7 + Y', 'Multiply\n5 * 7', 'Subtract\n7 - Y']
                    distractors[1] = ['Add\n35 + (-7Y)', 'Subtract\n35 - 7Y', 'Subtract\n5 - 35', 'Multiply\n7Y', 'Add\n7Y + 5', 'Subtract\n35 - 5', 'Divide\n35 @ 5'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['E$pand\n7(5 - Y)'] = '5 + 35 - 7Y';
                    qtexts[1] ={};
                    qtexts[1]['Add\n5 + 35'] = '40 - 7Y';



                    break;
                case '1C23':
                    //type :
                    eq = '~(6x - 10) - 5'; //~ means 1/2 here
                    ca1 = ['E$pand\n~ (6x - 10)'];
                    ca2 = ['Subtract\n-5 - 5'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Subtract\n~ - 5', 'Subtract\n3x - 5', 'Add\n~ + 10', 'Multiply\n6x'];
                    distractors[1] = ['Multiply\n3x', 'Subtract\n5 - 5', 'Add\n5 + (-5)', 'Divide\n-5@5'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['E$pand\n~ (6x - 10)'] = '3x - 5 - 5';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n-5 - 5'] = '3x - 10';



                    break;
                case '1C24':
                    //type :
                    eq = '6.5 - 8(1 - 2x)';
                    ca1 = ['E$pand\n8(1 - 2x)'];
                    ca2 = ['Subtract\n6.5 - 8'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Subtract\n6.5 - 8', 'Subtract\n1 - 2x', 'Add\n8 + 1', 'Divide\n8 @ 2x', 'Multiply\n2x']
                    distractors[1] = ['Add\n6.5 + 8', 'Subtract\n8 - 16x', 'Multiply\n16x', 'Divide\n16x @ 8'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['E$pand\n8(1 - 2x)'] = '6.5 - 8 - 16x';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n6.5 - 8'] = '-1.5 - 16x';


                    break;
                case '1C25':
                    //type :
                    eq = '4(x + Y) - Y';
                    ca1 = ['E$pand\n4(x + Y)'];
                    ca2 = ['Subtract\n4Y - Y'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Multiply\nY(Y)', 'Add\nx + Y', 'Subtract\nY - Y', 'Add\n4x + 0'];
                    distractors[1] = ['Add\n4x + 4Y', 'Add\n4x + (-Y)', 'Subtract\n-4Y - Y', 'Multiply\n4x'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['E$pand\n4(x + Y)'] = '4x + 4Y - Y';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n4Y - Y'] = '4x + 3Y';


                    break;
                case '1C26':
                    //type :
                    eq = '7(x< - x) - x<';
                    ca1 = ['E$pand\n7(x< - x)'];
                    ca2 = ['Subtract\n7x< - x<'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Evaluate\nx<', 'Subtract\nx< - x', 'Subtract\nx< - x<', 'Add\n7 + (-x)', 'Divide\nx @ 2'];
                    distractors[1] = ['Evaluate\n7x<', 'Add\n7x< + x<', 'Subtract\n-7x - x<', 'Subtract\n-7x< - x<'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['E$pand\n7(x< - x)'] = '7x< - 7x - x<';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\n7x< - x<'] = '6x< - 7x';


                    break;
                case '1C27':
                    //type :
                    eq = '2(x< - 2#) - x<';
                    ca1 = ['Evaluate\n2#'];
                    ca2 = ['E$pand\n2(x< - 32)'];
                    ca3 = ['Subtract\n2x< - x<'];
                    steps = 3;
                    distractors={};
                    distractors[0] = ['Evaluate\n2x<', 'Multiply\n2 * 2#', 'Subtract\nx< - x<', 'Subtract\nx< - 32', 'Add\n2 + x<'];
                    distractors[1] = ['Evaluate\n2x<', 'Subtract\nx< - x<', 'Subtract\nx< - 32', 'Add\n2 + x<'];
                    distractors[2] = ['Add\n-2x< + (-x<)', 'Subtract\n-2x< - x<', 'Subtract\n2x< - 64', 'Multiply\n2x'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Evaluate\n2#'] = '2(x< - 32) - x<';
                    qtexts[1] ={};
                    qtexts[1]['E$pand\n2(x< - 32)'] = '2x< - 64 - x<';
                    qtexts[2] ={};
                    qtexts[2]['Subtract\n2x< - x<'] = 'x< - 64';

                    break;
                case '1C28':
                    //type :
                    eq = 'x< + (7 - 4) x<';
                    ca1 = ['Subtract\n7 - 4'];
                    ca2 = ['Add\nx< + 3x<'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Evaluate\nx<', 'Add\nx< + 3', 'Add\nx< + x<', 'Divide\n7 @ x<', 'Evaluate\n(x<)<'];
                    distractors[1] = ['Add\n-x< + 3x<', 'Subtract\nx< - 3x<', 'Divide\n3x< @ x<', 'Multiply\nx< * 3x<'];
                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Subtract\n7 - 4'] = 'x< + 3x<';
                    qtexts[1] ={};
                    qtexts[1]['Add\nx< + 3x<'] = '4x<';


                    break;

                case '1C29':
                    //type : 5.1x - 8.5 + 10x + 7
                    eq = '5.1x - 8.5 + 10x + 7';
                    ca1 = ['Add\n5.1x + 10x','Add\n-8.5 + 7'];
                    ca2 = {};
                    ca2['Add\n5.1x + 10x'] = ['Add\n-8.5 + 7'];
                    ca2['Add\n-8.5 + 7'] = ['Add\n5.1x + 10x'];
                    steps = 2;
                    distractors={};
                    distractors[0] = ['Subtract\n5.1x - 8.5', 'Subtract\n5.1 - 8.5', 'Subtract\n5.1x - 10x', 'Add\n5.1x + 8.5', 'Add\n8.5 + 10x', 'Add\n-8.5 + 10x', 'Add\n8.5 + 7', 'Subtract\n8.5 - 7', 'Subtract\n-8.5 - 7', 'Add\n10x + 7'];
                    distractors[1]={};
                    distractors[1]['Add\n5.1x + 10x'] = ['Subtract\n15.1x - 8.5', 'Add\n15.1x + 8.5', 'Subtract\n15.1x - 8.5x', 'Add\n8.5 + 7', 'Subtract\n-8.5 - 7', 'Subtract\n8.5 - 7', 'Subtract\n15.1 - 8.5'];
                    distractors[1]['Add\n-8.5 + 7'] = ['Subtract\n5.1x - 1.5', 'Add\n5.1x + 1.5', 'Subtract\n5.1x - 1.5x', 'Subtract\n5.1x - 10x', 'Subtract\n5.1 - 1.5', 'Add\n5.1 + 10', 'Add\n1.5 + 10', 'Add\n-1.5 + 10x', 'Add\n1.5 + 10x'];

                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Add\n5.1x + 10x'] = '15.1x - 8.5 + 7';
                    qtexts[0]['Add\n-8.5 + 7'] = '5.1x - 1.5 + 10x';
                    qtexts[1] ={};
                    qtexts[1]['Add\n-8.5 + 7']= qtexts[1]['Add\n5.1x + 10x'] = '15.1x - 1.5';

                    break;
                case '1C30':
                    //type :
                    eq = 'x< + Y< - 5x< - 2Y<';
                    ca1 = ['Subtract\nx< - 5x<','Subtract\nY< - 2Y<'];
                    ca2 = {};
                    ca2['Subtract\nx< - 5x<'] = ['Subtract\nY< - 2Y<'];
                    ca2['Subtract\nY< - 2Y<'] = ['Subtract\nx< - 5x<'];

                    steps = 2;
                    distractors={};
                    distractors[0] = ['Add\nx< + Y<', 'Multiply\nx(2)', 'Subtract\nx< - 3x', 'Add\nx< + 5x<', 'Subtract\nY< - 5x<', 'Multiply\nY(2)', 'Add\nY< + 2Y<', 'Subtract\nx - 5x', 'Subtract\nY - 2Y', '5x(5x)', 'Multiply\n2Y(2Y)'];
                    distractors[1]={};
                    distractors[1]['Subtract\nx< - 5x<'] = ['Add\n-4x< + Y<', 'Add\n4x< + Y<', 'Add\nY< + 2Y<', 'Subtract\nY - 2Y', 'Multiply\n-4x(2)', 'Multiply\n-4x(-4x)', 'Multiply\nY(2)', 'Subtract\n-Y< - 2Y<'];
                    distractors[1]['Subtract\nY< - 2Y<'] = ['Subtract\nx< - Y<', 'Add\nx< + Y<', 'Add\nx< + 5<', 'Subtract\nx - 5x', 'Multiply\nx(2)', 'Multiply\nY(2)', 'Multiply\n-5x(-5x)', 'Multiply\n5x(5x)', 'Subtract\nY< - 5x<', 'Subtract\n-Y< - 5x<'];

                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Subtract\nx< - 5x<'] = '-4x< + Y< - 2Y<';
                    qtexts[0]['Subtract\nY< - 2Y<'] = 'x< -Y< - 5x<';
                    qtexts[1] ={};
                    qtexts[1]['Subtract\nY< - 2Y<'] =  qtexts[1]['Subtract\nx< - 5x<'] = '-4x< - Y<';


                    break;
                case '1C31':
                    //type :
                    eq = '-xY - Y + 7Y + 5xY';
                    ca1 = ['Add\n-xY + 5xY','Add\n-Y + 7Y'];
                    ca2 = {};
                    ca2['Add\n-xY + 5xY'] = ['Add\n-Y + 7Y'];
                    ca2['Add\n-Y + 7Y'] = ['Add\n-xY + 5xY'];

                    steps = 2;
                    distractors={};
                    distractors[0] = ['Subtract\n-xY - Y', 'Subtract\nxY - Y', 'Add\n-xY + Y', 'Add\nxY + 5xY', 'Add\nxY + 7xY', 'Subtract\n-xY - 5xY', 'Add\nY + 7Y', 'Multiply\n-xY(Y)', 'Multiply\nxY(-Y)', 'Add\n7Y + 5xY', 'Add\n7xY + 5xY'];
                    distractors[1]={};
                    distractors[1]['Add\n-xY + 5xY'] = ['Subtract\n-4xY - Y', 'Subtract\n4xY - Y', 'Subtract\n-4Y - Y', 'Add\nY + 7Y','Subtract\n-Y - 7Y', 'Add\n-4xY + 7Y', 'Add\n4xY + 7Y', 'Add\n4xY + Y', 'Add\n-4xY + Y'];
                    distractors[1]['Add\n-Y + 7Y'] = ['Add\n-xY + 6Y', 'Add\nxY + 6Y', 'Subtract\n-xY - 6Y', 'Add\n6Y + 5xY', 'Subtract\n6Y - 5xY', 'Add\n6Y + 5Y','Add\nxY + 5xY', 'Subtract\nxY - 5xY', 'Subtract\n-xY - 5xY', 'Add\n-x + 6'];

                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Add\n-xY + 5xY'] = '-4xY - Y + 7Y';
                    qtexts[0]['Add\n-Y + 7Y'] = '-xY + 6Y + 5xY';

                    qtexts[1] ={};
                    qtexts[1]['Add\n-Y + 7Y'] = qtexts[1]['Add\n-xY + 5xY'] = '-4xY + 6Y';


                    break;
                case '1C32':
                    //type :
                    eq = '-2xY - Y + 4Y + xY';
                    ca1 = ['Add\n-2xY + xY','Add\n-Y + 4Y'];
                    ca2 = {};
                    ca2['Add\n-2xY + xY'] = ['Add\n-Y + 4Y'];
                    ca2['Add\n-Y + 4Y'] = ['Add\n-2xY + xY'];

                    steps = 2;
                    distractors={};
                    distractors[0] = ['Subtract\n-2xY - Y', 'Subtract\n2xY - Y', 'Add\n-2xY + Y', 'Add\n2xY + xY', 'Add\n2xY + 4xY', 'Subtract\n-2xY - xY', 'Add\nY + 4Y', 'Multiply\n-2xY(Y)', 'Multiply\n-2xY(-Y)', 'Add\n4Y + xY', 'Add\n4xY + xY'];
                    distractors[1]={};
                    distractors[1]['Add\n-2xY + xY'] = ['Subtract\n-xY - Y', 'Subtract\nxY - Y', 'Subtract\n-Y - 4Y', 'Add\nY + 4Y', 'Add\n-xY + 4Y', 'Add\nxY + 4Y', 'Add\nxY + Y', 'Add\n-xY + Y'];
                    distractors[1]['Add\n-Y + 4Y'] = ['Add\n-2xY + 3Y', 'Add\n2xY + 3Y', 'Subtract\n-2xY - 3Y', 'Add\n3Y + xY', 'Subtract\n3Y - xY', 'Add\n2Y + 3Y', 'Add\n-2Y + 3Y', 'Add\n2xY + xY', 'Subtract\n2xY - xY', 'Subtract\n-2xY - xY', 'Add\n-2x + 3'];

                    qtexts ={};
                    qtexts[0] ={};
                    qtexts[0]['Add\n-2xY + xY'] = '-xY - Y + 4Y';
                    qtexts[0]['Add\n-Y + 4Y'] = '-2xY + 3Y + xY';

                    qtexts[1] ={};
                    qtexts[1]['Add\n-Y + 4Y'] =  qtexts[1]['Add\n-2xY + xY'] = '-xY + 3Y';


                    break;


            }

            console.log('P,Q,R,eq >',P,Q,R,eq);
            quesObj= {};
            quesObj.text = eq;
            quesObj.a = P;
            quesObj.b = Q;
            quesObj.c = R;
            quesObj.d = S;
            quesObj.distractors = distractors;
            quesObj.ca1 = ca1;
            quesObj.ca2 = ca2;
            quesObj.ca3 = ca3;
            quesObj.ca4 = ca4;
            quesObj.type = type;
            quesObj.randArr= randArr;
            quesObj.steps = steps;
            quesObj.qtexts = qtexts;

            return quesObj;
        },

        getRandNumber:function(a,b){
            return (a + Math.round(Math.random()*(b-a)));

        }

    }
);
