puremvc.define(
    // CLASS INFO
    {
        name: 'helper.EvalHelper'
    },

    // INSTANCE MEMBERS
    {



    },

    // STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */

        eval: function (expr) {

            var ret;
            expr = expr.toString();
            console.log(expr);

            if(expr.indexOf('/') == -1)return Parser.evaluate(expr).toString();

            if (expr.charAt(0) == '+')expr = expr.replace('+', '');
            if (expr.charAt(0) == '-')expr = expr.replace('-', '');
            if (expr.charAt(0) == '*')expr = expr.replace('*', '');

            if (expr.indexOf('+') != -1) {
                var arr = expr.split('+');
                var a, b = 1, c, d = 1;
                if (arr[0].indexOf('/') != -1) {
                    var arr1 = arr[0].split('/');
                    a = arr1[0];
                    b = arr1[1];

                }
                else {
                    a = arr[0];
                }


                if (arr[1].indexOf('/') != -1) {
                    var arr1 = arr[1].split('/');
                    c = arr1[0];
                    d = arr1[1];

                }
                else {
                    c = arr[1];
                }


                if (b == 1 && d == 1) {
                    ret = (Parser.evaluate(a + '*' + d) + Parser.evaluate(b + '*' + c)).toString();

                }
                else {
                    ret = Parser.evaluate(a + '*' + d) + Parser.evaluate(b + '*' + c) + '/' + Parser.evaluate(b + '*' + d);
                }
            }
            else if (expr.indexOf('-') != -1) {

                var arr = expr.split('-');
                var a, b = 1, c, d = 1;
                if (arr[0].indexOf('/') != -1) {
                    var arr1 = arr[0].split('/');
                    a = arr1[0];
                    b = arr1[1];

                }
                else {
                    a = arr[0];
                }


                if (arr[1].indexOf('/') != -1) {
                    var arr1 = arr[1].split('/');
                    c = arr1[0];
                    d = arr1[1];

                }
                else {
                    c = arr[1];
                }

                if (b == 1 && d == 1) {
                    ret = (Parser.evaluate(a + '*' + d) - Parser.evaluate(b + '*' + c)).toString();

                }
                else {
                    ret = Parser.evaluate(a + '*' + d) - Parser.evaluate(b + '*' + c) + '/' + Parser.evaluate(b + '*' + d);
                }
            }
            else if (expr.indexOf('*') != -1) {

                var arr = expr.split('*');
                var a, b = 1, c, d = 1;
                if (arr[0].indexOf('/') != -1) {
                    var arr1 = arr[0].split('/');
                    a = arr1[0];
                    b = arr1[1];

                }
                else {
                    a = arr[0];
                }


                if (arr[1].indexOf('/') != -1) {
                    var arr1 = arr[1].split('/');
                    c = arr1[0];
                    d = arr1[1];

                }
                else {
                    c = arr[1];
                }

                if (b == 1 && d == 1) {
                    ret = Parser.evaluate(a + '*' + c).toString();

                }
                else {
                    ret = Parser.evaluate(a + '*' + c) + '/' + Parser.evaluate(b + '*' + d);
                }

            }
            else
            {
                ret = expr;
            }

            var evalRet = Parser.evaluate(ret);
            if(evalRet % 1 == 0)ret=evalRet;
            console.log("EVEL>"+evalRet);

            return ret.toString();

        },

        reduce: function reduce(numerator, denominator) {
            var gcd = function gcd(a, b) {
                return b ? gcd(b, a % b) : a;
            };
            gcd = gcd(numerator, denominator);
            return [numerator / gcd, denominator / gcd];
        },


        isInt: function (n) {
            return n % 1 === 0;
        },

        simplify: function (qText, opTxt) {


            var retObj = new Object();
            retObj.correct = false;
            var a, b,c;

            var arr = qText.split('=');
            var splitChar;
            if(arr[0].indexOf('+') != -1)
            {
                splitChar ='+';
            }
            else if(arr[0].indexOf('-') != -1)
            {
                splitChar = '-';
            }

            var leftArr = arr[0].split(splitChar);
            leftArr[0]= leftArr[0].replace('x','');
            if (leftArr[0].indexOf('/') != -1) {
                var larr = leftArr[0].split('/');

                if (larr[0] == '')larr[0] = '1';
                a = larr[0] + '/' + larr[1];
            }
            else {

                a = leftArr[0];

            }

            if(this.a == "")this.a ='1';
            b= (splitChar) ? splitChar + leftArr[1] : '';
            c= arr[1];


            if (opTxt.indexOf('*') != -1 || opTxt.indexOf('/') != -1 || opTxt.indexOf('@') != -1) {

                if (b == 0 || b == "")
                {
                    opTxt = opTxt.replace('@', '/');
                    if (Parser.evaluate(a + opTxt) == 1) {
                        retObj.correct = true;
                    }

                }
                else{

                    if(opTxt.indexOf('/') != -1)
                    {
                        opTxt = opTxt.replace('@', '*');


                    }
                    else
                    {
                        opTxt = opTxt.replace('@', '/');
                    }

                    console.log(b,opTxt);
                    console.log(Parser.evaluate(Parser.evaluate(b) + opTxt));
                    if (Parser.evaluate(b + opTxt) == 0) {
                        retObj.correct = true;
                    }

                }

            }
            else {

                if (b != 0) {
                    if (Parser.evaluate(b + opTxt) == 0) {
                        retObj.correct = true;
                    }

                }

            }


            return retObj;
        },

        eval1B: function (qtext, selectedOption,step,quesObj) {

            var ret ={};
            ret.correct = false;
            var ansObj;
            if(step == 0)
            {
                //1B
                for(var i=0;i<quesObj.ca1.length;i++)
                {
                    ansObj= quesObj.ca1[i];//.replace(' ','');
                    if(selectedOption == ansObj)
                    {
                        ret ={};
                        ret.ca = selectedOption;

                        if(quesObj.text.indexOf(selectedOption) > 0 || quesObj.text.indexOf(selectedOption.split("").reverse().join("").toString()) > 0)
                        {
                            ret.text = quesObj.ca2[selectedOption][1];
                        }
                        else
                        {
                            ret.text = quesObj.ca2[selectedOption][0];
                        }

                        ret.correct = true;
                        break;

                    }

                }
            }
            else if(step == 1)
            {
                var arr = quesObj.ca2[quesObj.lastAnswer];
                console.log('step 1 arr ',arr.toString());
                console.log('step 1 selection',selectedOption);

                for(var j=0;j<arr.length;j++)
                {
                    ansObj= arr[j]; //.replace(' ','');
                    if(selectedOption == ansObj)
                    {
                        ret ={};
                        ret.ca = selectedOption;

                        //1B
                        console.log('step1 1B');

                        ret.text = eval(selectedOption);
                        console.log('ret.text',ret.text);


                        ret.correct = true;

                        break;
                    }

                }

            }
            else if(step == 2)
            {
                console.log('step 2');
                //1C
                ret ={};
                ret.ca = selectedOption;
                ret.text = eval(quesObj.ca3[0]);
                ret.correct = true;
            }

            if(ret.text == undefined)
            {
                console.log('ret.text',qtext);

            }

            return ret;


        },

        eval1C: function (qtext, selectedOption,step,quesObj) {

            var ret ={};
            ret.correct = false;
            if(step == 0)
            {
                //1C
                var arr = (Array.isArray(quesObj.ca1)) ? quesObj.ca1 : quesObj.ca1[quesObj.lastAnswer];
                for(var j=0;j<arr.length;j++)
                {
                    ansObj= arr[j];
                    if(selectedOption == ansObj)
                    {
                        ret.correct  = true;
                        ret.ca = selectedOption;
                        ret.text = quesObj.qtexts[0][selectedOption];
                        ret.finaltext = quesObj.qtexts[0][ret.text];

                        break;
                    }

                }

            }
            else if(step == 1)
            {
                var arr = (Array.isArray(quesObj.ca2)) ? quesObj.ca2 : quesObj.ca2[quesObj.lastAnswer];
                for(var j=0;j<arr.length;j++)
                {
                    ansObj= arr[j];
                    if(selectedOption == ansObj)
                    {
                        ret.correct  = true;
                        ret.ca = selectedOption;
                        ret.text = quesObj.qtexts[1][selectedOption];
                        ret.finaltext = quesObj.qtexts[1][ret.text];


                        break;
                    }

                }
            }
            else if(step == 2)
            {
                var arr = (Array.isArray(quesObj.ca3)) ? quesObj.ca3 : quesObj.ca3[quesObj.lastAnswer];
                for(var j=0;j<arr.length;j++)
                {
                    ansObj= arr[j];
                    if(selectedOption == ansObj)
                    {
                        ret.correct  = true;
                        ret.ca = selectedOption;
                        ret.text = quesObj.qtexts[2][selectedOption];
                        ret.finaltext = quesObj.qtexts[2][ret.text];

                        break;
                    }

                }
            }
            else if(step == 3)
            {
                var arr = (Array.isArray(quesObj.ca4)) ? quesObj.ca4 : quesObj.ca4[quesObj.lastAnswer];
                for(var j=0;j<arr.length;j++)
                {
                    ansObj= arr[j];
                    if(selectedOption == ansObj)
                    {
                        ret.correct  = true;
                        ret.ca = selectedOption;
                        ret.text = quesObj.qtexts[3][selectedOption];
                        ret.finaltext = quesObj.qtexts[3][ret.text];

                        break;
                    }

                }
            }


            return ret;


        }





    }
);
