/**
 * @class controller.PrepareW3CViewCommand
 * @extends puremvc.SimpleCommand
 */
puremvc.define
(
	// CLASS INFO
	{
		name: 'controller.command.PrepareViewCommand',
		parent: puremvc.SimpleCommand
	},

	// INSTANCE TRAITS
	{
		/** @override */
		execute: function (note)
		{
			// register the TextComponentMediator
			this.facade.registerMediator( new view.mediator.SplashScreenMediator );
			//this.sendNotification( view.mediator.SplashScreenMediator.START_GAME );
		}
	}
);