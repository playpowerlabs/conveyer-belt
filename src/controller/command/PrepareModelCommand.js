/**
 * @class controller.PrepareModelCommand.js
 */
puremvc.define
(
	// CLASS INFO
	{
		name: 'controller.command.PrepareModelCommand',
		parent: puremvc.SimpleCommand
	},

	// INSTANCE MEMBERS
	{
		/** @override */
		execute: function (note)
		{
			// register the TextProxy
			this.facade.registerProxy( new model.proxy.LevelProxy );

		}
	}
)