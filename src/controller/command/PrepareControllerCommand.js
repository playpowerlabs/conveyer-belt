/**
 * @class controller.PrepareControllerCommand.js
 */
puremvc.define
(
	// CLASS INFO
	{
		name: 'controller.command.PrepareControllerCommand',
		parent: puremvc.SimpleCommand
	},

	// INSTANCE MEMBERS
	{
		/** @override */
		execute: function (note)
		{
			// register teh ProcessTextCommand to handle the PROCESS_TEXT notification
			this.facade.registerCommand( AppConstants.PROCESS_TEXT, controller.command.ProcessTextCommand );
		}
	}
);
