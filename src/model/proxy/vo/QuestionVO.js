/**
 * Created by ankit on 10/30/13.
 */

/**
 * @class model.TextProxy
 * @extends puremvc.Proxy
 */
puremvc.define
(
    // CLASS INFO
    {
        name: 'model.proxy.vo.QuestionVO',
        constructor: function (type,obj)
        {

            this.distractors = obj.distractors;
            this.text = obj.text;

            switch(type)
            {
                case 'mcq':
                    this.a = obj.a;
                    this.b = obj.b;
                    this.c = obj.c;
                    this.d= obj.d;
                    this.text = obj.text;
                    this.steps = obj.steps;
                    this.ca1= obj.ca1;
                    this.ca2= obj.ca2;
                    this.ca3= obj.ca3;
                    this.ca4= obj.ca4;
                    this.type = obj.type;
                    this.randArr = obj.randArr;
                    this.qtexts = obj.qtexts;

                    break;
                case 'eq':

                    var arr = this.text.split('=');
                    var splitChar;
                    if(arr[0].indexOf('+') != -1)
                    {
                        splitChar ='+';
                    }
                    else if(arr[0].indexOf('-') != -1)
                    {
                        splitChar = '-';
                    }

                    var leftArr = arr[0].split(splitChar);
                    leftArr[0]= leftArr[0].replace('x','');
                    if (leftArr[0].indexOf('/') != -1) {
                        var larr = leftArr[0].split('/');

                        if (larr[0] == '')larr[0] = '1';
                        this.a = larr[0] + '/' + larr[1];
                    }
                    else {

                        this.a = leftArr[0];

                    }

                    if(this.a == "")this.a ='1';
                    this.b= (splitChar) ? splitChar + leftArr[1] : '';
                    this.c= arr[1];


                    this.steps = this.distractors.length;
                    this.ca1= obj.ca1;
                    this.ca2= obj.ca2;
                    this.type = obj.type;

                    console.log("NO OF STEPS IN EQ>>",this.steps);

                break;
            }

        }
    },

    // INSTANCE MEMBERS
    {
         text:null,
         distractors:null,
         a:null,
         b:null,
         c:null
    },

    // CLASS MEMBERS
    {
        /**
         * The TextProxy's name.
         *
         * @static
         * @type {string}
         */
        NAME: 'QuestionVO'

    }
);