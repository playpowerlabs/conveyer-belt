/**
 * @class model.TextProxy
 * @extends puremvc.Proxy
 */
puremvc.define
(
    // CLASS INFO
    {
        name: 'model.proxy.LevelProxy',
        parent: puremvc.Proxy
    },

    // INSTANCE MEMBERS
    {
        levelConfig: null,
        questionArr: null,
        demoFlag: null,
        levelType: null,
        levelQues: null,
        star1: null,
        star2: null,
        star3: null,
        sequence: null,
        steps: 0,


        /** @override */
        onRegister: function () {

            console.log('register levelproxy');

        },

        makeEqLevelConfig: function () {

            this.levelType = 'static';
            this.levelQues = 10;//10
            this.sequence = 'simple';
            this.demoFlag = true;

            var getRandType = function(arr){
                return arr[parseInt(Math.random()*arr.length)];
            };

            var typeArr = [
                'C',
                'D',
                getRandType(['C','D']),
                'E',
                'F',
                getRandType(['A','B','G','H']),
                getRandType(['A','B','G','H']),
                getRandType(['A','B','G','H']),
                getRandType(['A','B','G','H']),
                getRandType(['A','B','G','H'])

            ];

            this.questionArr = new Array();

            for (var i = 0; i < typeArr.length; i++) {
                var quesVO = new model.proxy.vo.QuestionVO('eq',helper.LevelHelper.getEqQuestion(typeArr[i]));
                this.steps += quesVO.steps;
                this.questionArr.push(quesVO);
            }

            if (this.sequence == "random") {
                //shuffle array elements
                this.questionArr = helper.GameHelper.shuffle(this.questionArr);
            }

            console.log('make config'+this.questionArr.length);

            this.onLevelLoaded.bind(this)();
        },

        makeMcqLevelConfig: function () {

            this.levelType = 'static';
            this.levelQues = 10;//10
            this.sequence = 'simple';
            this.demoFlag = true;

            var getRandType = function(arr){
                return arr[parseInt(Math.random()*arr.length)];
            };

            var typeArr;
            if(AppConstants.LEVEL == '1C')
            {
                this.sequence = 'random';
                if(AppConstants.GRADE == 3)
                {
                    typeArr = [
                        '1C1',
                        '1C2',
                        '1C3',
                        '1C4',
                        '1C5',
                        '1C6',
                        '1C7',
                        '1C8',
                        '1C9',
                        '1C10',
                        '1C11',
                        '1C12'


                    ];

                }
                else  if(AppConstants.GRADE == 6)
                {
                    typeArr = [
                        '1C1',
                        '1C2',
                        '1C3',
                        '1C4',
                        '1C5',
                        '1C6',
                        '1C7',
                        '1C8',
                        '1C9',
                        '1C10',
                        '1C11',
                        '1C12',
                        '1C13',
                        '1C14',
                        '1C15',
                        '1C16',
                        '1C17',
                        '1C18',
                        '1C19',
                        '1C20'


                    ];

                }
                else  if(AppConstants.GRADE == 7)
                {
                    typeArr = [
                        '1C13',
                        '1C14',
                        '1C15',
                        '1C16',
                        '1C17',
                        '1C18',
                        '1C19',
                        '1C20',
                        '1C21',
                        '1C22',
                        '1C23',
                        '1C24',
                        '1C25',
                        '1C26',
                        '1C27',
                        '1C28',
                        '1C29',
                        '1C30',
                        '1C31',
                        '1C32'

                    ];

                }


            }
            else if (AppConstants.LEVEL == '1B')
            {
                if(AppConstants.GRADE == 1)
                {
                    typeArr = [
                        'A',
                        'A',
                        'A',
                        'A',
                        'A',
                        'A',
                        'A',
                        'A',
                        'A',
                        'A'

                    ];


                }
                else if(AppConstants.GRADE == 2)
                {
                    typeArr = [
                        'A',
                        'A',
                        'A',
                        'B',
                        'B',
                        'B',
                        'C',
                        'C',
                        'C',
                        'C'

                    ];

                }



            }


            this.questionArr = new Array();

            for (var i = 0; i < typeArr.length; i++) {
                var quesVO = new model.proxy.vo.QuestionVO('mcq',helper.LevelHelper.getMcqQuestion(typeArr[i]));
                this.steps += quesVO.steps;
                this.questionArr.push(quesVO);
            }

            if (this.sequence == "random") {
                //shuffle array elements
                this.questionArr = helper.GameHelper.shuffle(this.questionArr);
            }

            console.log('make config'+this.questionArr.length);

            this.onLevelLoaded.bind(this)();
        },


        getQuestionVO: function () {
            var quesVO = this.questionArr.shift();
            this.questionArr.push(quesVO);
            return quesVO;

        },

        onLevelLoaded: function (text) {
            this.sendNotification(model.proxy.LevelProxy.LEVEL_LOADED, text);
        },


        onReset: function () {
            if(AppConstants.MECHANIC == 'mcq')
            {
                this.makeMcqLevelConfig.bind(this)();
            }
            else
            {
                this.makeEqLevelConfig.bind(this)();
            }


        },

        /** @override */
        onRemove: function () {
            this.levelConfig = null;
        }
    },

    // CLASS MEMBERS
    {
        /**
         * The TextProxy's name.
         *
         * @static
         * @type {string}
         */
        NAME: 'LevelProxy',
        LEVEL_LOADED: 'LEVEL_LOADED'

    }
);