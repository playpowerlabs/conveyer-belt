puremvc.define
(
    // CLASS INFO
    {
        name: 'view.component.McqQuestionPanel',
        parent: createjs.Container
        /** @constructor */

    },

    // INSTANCE MEMBERS
    {
        init: function (text) {
            //SET TEXT VIEW HERE//
            this.txtContainer = new createjs.Container();
            this.addChild(this.txtContainer);
            this.quesTxt = new view.component.TextView();
            //text,space,size,width,height,color,font
            if(AppConstants.LEVEL == '1C')
            {
                this.quesTxt.init(text,false,50,300,60,'#333333','Dimbo',false);
            }
            else
            {
                this.quesTxt.init(text,false,50,300,50,'#333333','Dimbo',false);
            }
            this.quesTxt.x -= 15;
            this.txtContainer.addChild(this.quesTxt);


            this.updateText.bind(this)(text,false);


        },

        updateText: function (text,smoke,result) {

            console.log('updatetext',text,text.length)
            smoke = (smoke == undefined) ? false : smoke;
            this.prevText = this.text;
            this.setText.bind(this)(text.toString());


            if(smoke)
            {
                //if(AppConstants.LEVEL == '1C')this.updateSplitText(result);
                setTimeout(this.addSmokeAnim.bind(this), 1500, this.quesTxt.x , this.quesTxt.y, 'left', function () {

                    var twn = TweenMax.to(this.quesTxt,0.1,{alpha:0});
                    this.quesTxt.update(this.text);
                    twn.reverse();


                }.bind(this));
            }
            else
            {
                this.quesTxt.update(this.text);

            }

        },

        updateSplitText:function(result){
            //for 1c only
            if(AppConstants.LEVEL == '1C')
            {
                var splitChar = result.ca.split('\n')[1];
                if(this.prevText.indexOf('('+splitChar+')') != -1)
                {
                    splitChar = '('+splitChar+')';
                }

                var splitArr = this.prevText.split(splitChar);
                if(splitArr.length == 1)
                {
                    this.quesTxt1.update(splitChar);
                    this.quesTxt1.setColor('#00CC33');
                    this.quesTxt2.update(splitArr[0]);
                }
                else if(splitArr.length == 2)
                {
                    this.quesTxt1.update(splitArr[0]);
                    this.quesTxt2.update(splitChar);
                    this.quesTxt2.setColor('#00CC33');
                    this.quesTxt3.update(splitArr[1]);

                }

                this.dupContainer.visible = true;
                this.quesTxt.visible= false;
                console.log(this.text);
            }
        },

        setText: function (_txt) {
            var arr = _txt.split('+');
            this.text = '';
            for(var i=0;arr && i<arr.length;i++)
            {
                this.text += arr[i];
                if(i<(arr.length -1))this.text += ' + ';
            }
            //this.text = _txt;

        },

        setColor: function (color) {

            this.color = color;
            if (this.quesTxt)this.quesTxt.setColor(color);


        },



        addSmokeAnim: function (posX, posY, type, callback) {

            var smoke = new lib.AppearAnim();
            smoke.x = posX;
            smoke.y = posY;
            smoke.scaleX=1.8;
            smoke.scaleY=0.9;

            this.addChild(smoke);

            helper.GameHelper.playSound('wipe');

            setTimeout(helper.GameHelper.remove, 200, smoke);
            if (callback) setTimeout(callback, 200);
        }


    },

    // STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */
        NAME: 'McqQuestionPanel'
    }
);


/**
 * Created by ankit on 3/8/14.
 */
