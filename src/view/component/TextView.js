
puremvc.define
(
    // CLASS INFO
    {
        name: 'view.component.TextView',
        parent: createjs.Container
        /** @constructor */

    },

    // INSTANCE MEMBERS
    {
        init: function (text,space,size,width,height,color,font,autoscale)
        {
            this.size =  size || '45';
            this.text = text || '';
            this.color = color || '#333333';
            this.width = width || 80;
            this.height = height || 45;
            this.font = font || 'Dimbo';
            this.autoscale = autoscale || true;

            space = (space == undefined) ? false : space;


            //SET TEXT VIEW HERE//
            this.txtContainer = new createjs.Container();
            this.addChild(this.txtContainer);

            this.txt1 = helper.GameHelper.getBitmapTextField(this.width,this.height,"",this.font,this.size-5,this.color,"center","middle",this.autoscale);
            this.txtContainer.addChild(this.txt1);

            this.txt2 = helper.GameHelper.getBitmapTextField(this.width,this.height,"",this.font,this.size,this.color,"center","middle",this.autoscale);
            this.txtContainer.addChild(this.txt2);

            this.txt3 = helper.GameHelper.getBitmapTextField(this.width,this.height,"",this.font,this.size-5,this.color,"center","middle",this.autoscale);
            this.txtContainer.addChild(this.txt3);

            //SIGN TEXT IF NEEDED
            this.txt4 = helper.GameHelper.getBitmapTextField(this.width,this.height,"",this.font,this.size,this.color,"center","middle",this.autoscale);
            this.txtContainer.addChild(this.txt4);


            console.log('textview update',text);
            this.update.bind(this)(this.text,space);

        },

        update:function(text,space){

            console.log('textview update',text);
            if(text == undefined)return;

            text = text.toString();
            space = (space == undefined) ? false : space;

            this.text = text;
            if((text.indexOf('/') != -1 && AppConstants.LEVEL != '1C'))
            {
                var arr = text.split('/');
                var t1,t2,t3,t4;
                if(arr[0].charAt(0) == '+')
                {
                    t1 = arr[0].replace('+','');
                    t2 = "__";
                    t3 =arr[1];
                    t4 = space ?  '+ ' : '+';
                }
                else if(arr[0].charAt(0) == '-')
                {
                    t1 = arr[0].replace('-','');
                    t2 = "__";
                    t3 =arr[1];
                    t4 = space ?  '- ' : '-';
                }
                else if(arr[0].charAt(0) == '*')
                {
                    t1 = arr[0].replace('*','');
                    t2 = "__";
                    t3 =arr[1];
                    t4 = space ? '* ' : '*';
                }
                else if(arr[0].charAt(0) == '@')
                {
                    t1 = arr[0].replace('@','');
                    t2 = "__";
                    t3 =arr[1];
                    t4 = space ? '@ ' : '@';
                }
                else
                {
                    t1 = arr[0];
                    t2 = "__";
                    t3 =arr[1];
                    t4 = '';
                }

                helper.GameHelper.setBitmapText(this.txt1,t1);
                helper.GameHelper.setBitmapText(this.txt2,t2);
                helper.GameHelper.setBitmapText(this.txt3,t3);
                helper.GameHelper.setBitmapText(this.txt4,t4);

                helper.GameHelper.setPos(this.txt1,28,0);
                helper.GameHelper.setPos(this.txt2,30,22);
                helper.GameHelper.setPos(this.txt3,28,49);
                helper.GameHelper.setPos(this.txt4,0,20);

                if(t4 == '')
                {
                    this.txt1.x -=10;
                    this.txt2.x -=10;
                    this.txt3.x -=10;

                }


                this.txtContainer.x = 10;
                helper.GameHelper.setPivot(this,this.getBounds().width/2 ,this.getBounds().height/2);

                this.width = (t4 == '') ? this.txt2.getActualWidth() : this.txt4.getActualWidth()+this.txt2.getActualWidth();



            }
            else
            {
                console.log('B4 width',this.txt2.getActualWidth())

                t1='';
                t2=text;
                t3='';
                t4='';

                if(AppConstants.LEVEL != '1C')
                {
                    if(t2.charAt(0) == '+')
                    {
                        //var arr = t2.split('+');
                        t2 = (space ? '+ ': '+') + t2.substring(1, t2.length);
                    }
                    else if(t2.charAt(0) == '-')
                    {
                        //var arr = t2.split('-');
                        t2 = (space ? '- ': '-') + t2.substring(1, t2.length);
                    }
                    else if(t2.charAt(0) == '*')
                    {
                        //var arr = t2.split('*');
                        t2 = (space ? '* ': '*') + t2.substring(1, t2.length);
                    }
                    else if(t2.charAt(0) == '@')
                    {
                        //var arr = t2.split('@');
                        t2 = (space ? '@ ': '@') + t2.substring(1, t2.length);
                    }
                }


                helper.GameHelper.setBitmapText(this.txt1,t1);
                helper.GameHelper.setBitmapText(this.txt2,t2);
                helper.GameHelper.setBitmapText(this.txt3,t3);
                helper.GameHelper.setBitmapText(this.txt4,t4);


                helper.GameHelper.setPos(this.txt1,28,0);
                helper.GameHelper.setPos(this.txt2,30,22);
                helper.GameHelper.setPos(this.txt3,28,49);
                helper.GameHelper.setPos(this.txt4,0,20);


                this.txtContainer.x = 0;
                helper.GameHelper.setPivot(this,this.getBounds().width/2,this.getBounds().height/2);

                this.width = this.txt2.getActualWidth();


                console.log('A4 width',this.txt2.getActualWidth())

            }

            helper.GameHelper.setPivot(this,this.getBounds().width/2,this.getBounds().height/2);


        },

        setColor:function(color){
            this.txt1.setColor(color);
            this.txt2.setColor(color);
            this.txt3.setColor(color);
            this.txt4.setColor(color);


        }



    },




    // STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */
        NAME: 'TextView'
    }
);



/**
 * Created by ankit on 3/8/14.
 */
