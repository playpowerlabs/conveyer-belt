
puremvc.define
(
    // CLASS INFO
    {
        name: 'view.component.Tile',
        parent: createjs.Container
        /** @constructor */

    },

    // INSTANCE MEMBERS
    {

        init: function (text,index,type,size, width,height,space,autoscale)
        {
            console.log('tile.text',text);

            this.type =  type || '0';
            this.text = text || '';
            this.index = index || '0';
            this.size = size || 45;
            this.width = width || 80;
            this.height = height || 45;
            this.space = (space == undefined) ? true : space;
            this.autoscale = autoscale || true;

            //SET TEXT VIEW HERE//
            this.textView = new view.component.TextView();
            this.textView.init(this.text,this.space,this.size,this.width,this.height,'#333333','Dimbo',this.autoscale);

            var tileBG;
            if(this.type == '1'){
                tileBG= new lib.SplitTile();
                helper.GameHelper.setPos(this.textView,-15,0);

            }
            else if(this.type == '2'){
                tileBG= new lib.Robot1Tile();
                helper.GameHelper.setPos(this.textView,-15,0);

            }
            else if(this.type == '3'){
                tileBG= new lib.Robot2Tile();
                helper.GameHelper.setPos(this.textView,-15,0);

            }
            else if(this.type == '4'){
                tileBG= new lib.BoxTile();
                helper.GameHelper.setPos(this.textView,-15,0);

            }
            else{
                //type ->0
                tileBG= new lib.Tile();
                helper.GameHelper.setPos(this.textView,-30,0);

            }
            this.addChild(tileBG);
            this.addChild(this.textView);

        },

        setText:function(txt){
            this.text = txt;
            this.textView.update(this.text,true);

        }



    },

    // STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */
        NAME: 'Tile'
    }
);



/**
 * Created by ankit on 3/8/14.
 */
