puremvc.define
(
    // CLASS INFO
    {
        name: 'view.component.EqQuestionPanel',
        parent: createjs.Container
        /** @constructor */

    },

    // INSTANCE MEMBERS
    {
        init: function (text) {
            //SET TEXT VIEW HERE//
            this.txtContainer = new createjs.Container();
            this.addChild(this.txtContainer);

            this.equalTxt = new view.component.TextView();
            this.equalTxt.init('=');

            this.txtContainer.addChild(this.equalTxt);

            this.updateText.bind(this)(text);


        },

        updateText: function (text, op) {

            this.op = op;
            text.replace('(', '').replace(')', '');


            // to avoid @1/13 case with *13
            if(this.op && this.op.charAt(0) == '@' && this.op.indexOf('/') != -1)
            {
                this.op = this.op.replace('@','/');

                var divArr = this.op.split('/');
                if((this.op.split("/").length - 1) > 1 && this.op.charAt(0) == '/')
                {
                    this.op = this.op.replace('/','*');
                    this.op = '*' + divArr[2]+'/'+divArr[1];
                }
            }
            ////



            if (this.op && this.op.charAt(0) == '@') {
                this.op = this.op.replace('@', '/');

                if (this.txtE && this.txtF) {


                    var arrA = this.txtA.text.split('x');
                    var c1;
                    if(this.txtA.text.indexOf('/') != -1)
                    {
                        //i.e x/6 + 4
                        c1 = ((arrA[0] == '') ? '1' : arrA[0]) + '/'+ arrA[1].split('/')[1]*this.op.split('/')[1];
                    }
                    else
                    {
                        c1 = Parser.evaluate(((arrA[0] == '') ? '1' : arrA[0]) + this.op);
                    }
                    var c2 = (this.txtB.text == "") ? 0 : Parser.evaluate(this.txtB.text + this.op);
                    var c3 = Parser.evaluate(this.txtC.text + this.op);


                    if (c1 % 1 != 0) {
                        c1 = c1+'';
                        if(c1.indexOf('/') != -1)
                        {
                            this.textA = ((c1.split('/')[0] == '1') ? '':c1.split('/')[0]) + 'x/' + c1.split('/')[1];
                        }
                        else
                        {
                            this.textA = arrA[0] + 'x' + this.op;
                        }
                    }
                    else {
                        this.textA = (c1 == 1) ? 'x' : c1 + 'x';
                    }

                    if (c2 % 1 != 0) {
                        this.textB = this.txtB.text + this.op;
                    }
                    else {
                        this.textB = c2 == 0 ? '' : c2.toString();

                    }


                    if (this.textB != '' && Parser.evaluate(this.textB) > 0) {
                        if (this.textB.indexOf('+') == -1)this.textB = '+' + this.textB;
                    }
                    else if (this.textB != '' && Parser.evaluate(this.textB) < 0) {
                        if (this.textB.indexOf('-') == -1)this.textB = '-' + this.textB;
                    }


                    if (c3 % 1 != 0) {
                        this.textC = this.txtC.text + this.op;
                    }
                    else {
                        this.textC = c3.toString();

                    }


                    setTimeout(this.addSmokeAnim.bind(this), 700, this.equalTxt.x - 70, this.equalTxt.y, 'left', function () {
                        //this.addSmokeAnim(this.equalTxt.x - 70, this.equalTxt.y, 'left', function () {

                        this.txtA.update(this.textA);
                        this.txtB.update(this.textB,true);
                        this.txtA.scaleX =1;
                        this.txtA.scaleY =1;
                        this.txtB.scaleX =1;
                        this.txtB.scaleY =1;

                        console.log('/ this.textA: ' + this.textA);
                        console.log('/ this.textB: ' + this.textB);

                        helper.GameHelper.remove(this.txtE);
                        this.txtE = null;

                        helper.GameHelper.remove(this.txtD1);
                        this.txtD1 = null;


                        TweenMax.to([this.txtA, this.txtB], 0, {y: "+=20"});

                        this.setPos('L');
                        this.setText();

                    }.bind(this));


                    setTimeout(this.addSmokeAnim.bind(this), 1400, this.equalTxt.x + 70, this.equalTxt.y, 'right', function () {
                        console.log('+_ this.textC: ' + this.textC);

                        this.txtC.update(this.textC);

                        helper.GameHelper.remove(this.txtF);
                        this.txtF = null;

                        helper.GameHelper.remove(this.txtD2);
                        this.txtD2 = null;


                        this.setPos('R');
                        this.setText();


                    }.bind(this));

                }
                else {

                    var divArr = this.op.split('/');
                    var posE, posF;
                    posE = (this.txtB.text == "") ? this.txtA.x : this.equalTxt.x - this.txtB.width - 10;
                    posF = this.txtC.x;

                    var div1Width,div2Width;
                    div1Width = (this.txtB.text == "") ? this.txtA.width : this.txtA.width + this.txtB.width + 3;
                    div2Width = this.txtC.width + 3;

                    //////remove this later
                    if(this.txtA.text.indexOf('/') != -1){
                        this.txtA.scaleX =0.6;
                        this.txtA.scaleY =0.6;

                        this.txtB.scaleX =0.6;
                        this.txtB.scaleY =0.6;

                    }
                    /////

                    var div1Text='';
                    var div2Text='';
                    for(var i=0;i<parseInt(div1Width/16);i++)
                    {
                        div1Text += '_';
                    }

                    for(var i=0;i<parseInt(div2Width/16);i++)
                    {
                        div2Text += '_';
                    }

                    this.txtD1 = new view.component.TextView();
                    this.txtD1.init(div1Text,false,55,200,50);
                    helper.GameHelper.setPos(this.txtD1, posE, 10);

                    this.txtD2 = new view.component.TextView();
                    this.txtD2.init(div2Text,false,55,200,50);

                    helper.GameHelper.setPos(this.txtD2, posF, 10);


                    this.txtE = new view.component.TextView();
                    this.txtE.init(divArr[1]);

                    this.txtF = new view.component.TextView();
                    this.txtF.init(divArr[1]);


                    helper.GameHelper.setPos(this.txtE, posE, 30);
                    helper.GameHelper.setPos(this.txtF, posF, 30);


                    this.txtContainer.addChild(this.txtD1);
                    this.txtContainer.addChild(this.txtD2);
                    this.txtContainer.addChild(this.txtE);
                    this.txtContainer.addChild(this.txtF);


                    var divTwn = new TimelineMax({});
                    divTwn.append(TweenMax.to([this.txtA, this.txtB, this.txtC], 0.3, {y: "-=15"}));
                    divTwn.append(TweenMax.from([this.txtD1, this.txtD2], 0.3, {y: "-=10", alpha: 0}));
                    divTwn.append(TweenMax.from([this.txtE, this.txtF], 0.3, {delay: 0.3, y: "-=50", alpha: 0}));


                }

            }
            else if (this.op && (this.op.indexOf('*') != -1 || this.op.indexOf('-') != -1 || this.op.indexOf('+') != -1)) {

                if (this.txtE && this.txtF) {
                    //simplify equation both sides after poof
                    this.removeBrackets.bind(this)();

                    if (this.op.indexOf('-') != -1 || this.op.indexOf('+') != -1) {
                        setTimeout(this.addSmokeAnim.bind(this), 700, this.equalTxt.x - 70, this.equalTxt.y, false, function () {
                            // this.addSmokeAnim(this.equalTxt.x - 70, this.equalTxt.y, false, function () {

                            if (Parser.evaluate(this.txtB.text + this.txtE.text) == 0) {
                                this.txtB.update('');

                                var offset = this.txtE.width + this.txtB.width - 5;

                            }
                            else {

                                var c2 = Parser.evaluate(this.txtB.text + this.txtE.text);
                                if (c2 > 0) {
                                    this.textB = helper.EvalHelper.eval(this.txtB.text + this.txtE.text);
                                    if (this.textB.indexOf('+') == -1)this.textB = '+' + this.textB;
                                }
                                else {
                                    this.textB = helper.EvalHelper.eval(this.txtB.text + this.txtE.text);
                                    if (this.textB.indexOf('-') == -1) this.textB = '-' + this.textB;

                                }

                                this.txtB.update(this.textB,true);


                            }


                            console.log('+_ this.textB: ' + this.txtB.text);

                            helper.GameHelper.remove(this.txtE);
                            this.txtE = null;

                            this.setPos('L');
                            this.setText();

                        }.bind(this));

                        setTimeout(this.addSmokeAnim.bind(this), 1400, this.equalTxt.x + 70, this.equalTxt.y, false, function () {


                            if (Parser.evaluate(this.txtC.text + this.txtF.text) > 0) {
                                this.textC = '' + helper.EvalHelper.eval(this.txtC.text + this.txtF.text);
                            }
                            else {
                                this.textC = helper.EvalHelper.eval(this.txtC.text + this.txtF.text);
                                if (this.textC.indexOf('-') == -1)this.textC = '-' + this.textC;


                            }

                            console.log('+_ this.textC: ' + this.textC);
                            this.textC = Parser.evaluate(this.textC) == 0 ? '0' : this.textC;
                            this.txtC.update(this.textC);

                            helper.GameHelper.remove(this.txtF);
                            this.txtF = null;

                            this.setPos('R');
                            this.setText();


                        }.bind(this));

                    }
                    else if (this.op.indexOf('*') != -1) {
                        setTimeout(this.addSmokeAnim.bind(this), 700, this.equalTxt.x - 70, this.equalTxt.y, false, function () {
                            //this.addSmokeAnim(this.equalTxt.x - 70, this.equalTxt.y, false, function () {

                            var arrA = this.txtA.text.split('x');
                            if (arrA[1].indexOf('/') != -1) {
                                if (arrA[0] == '') {
                                    arrA[0] = '1/' + arrA[1].split('/')[1];
                                }
                                else {
                                    arrA[0] = arrA[0] + '/' + arrA[1].split('/')[1];
                                }
                            }

                            if (arrA[0] == '')arrA[0] = '1';
                            this.textA = helper.EvalHelper.eval(arrA[0] + this.txtE.text).toString();
                            if (this.textA.indexOf('/') != -1) {
                                var larr = this.textA.split('/');
                                this.textA = larr[0] + 'x/' + larr[1];
                            }
                            else {

                                var c1 = parseInt(this.textA);
                                this.textA = (c1 == 1) ? 'x' : c1 + 'x';

                            }

                            if (this.txtB.text != '') {
                                console.log('textB>>' + Parser.evaluate(this.txtB.text + this.txtE.text));

                                if (Parser.evaluate(this.txtB.text + this.txtE.text) > 0) {
                                    this.textB = helper.EvalHelper.eval(this.txtB.text + this.txtE.text);
                                    if (this.textB.indexOf('+') == -1)this.textB = '+' + this.textB;
                                }
                                else {
                                    this.textB = helper.EvalHelper.eval(this.txtB.text + this.txtE.text);
                                    if (this.textB.indexOf('-') == -1)this.textB = '-' + this.textB;

                                }

                            }
                            else {
                                this.textB = '';
                            }

                            console.log('* txtA: ' + this.textA);
                            console.log('* txtB: ' + this.textB);


                            this.txtA.update(this.textA);
                            this.txtB.update(this.textB,true);

                            helper.GameHelper.remove(this.txtE);
                            this.txtE = null;

                            this.setPos('L');
                            this.setText();

                        }.bind(this));

                        setTimeout(this.addSmokeAnim.bind(this), 1400, this.equalTxt.x + 70, this.equalTxt.y, false, function () {

                            console.log('* txtC: ' + helper.EvalHelper.eval(this.txtC.text + this.txtF.text).toString());
                            if (Parser.evaluate(this.txtC.text + this.txtF.text) > 0) {
                                this.textC = helper.EvalHelper.eval(this.txtC.text + this.txtF.text);
                            }
                            else {
                                this.textC = helper.EvalHelper.eval(this.txtC.text + this.txtF.text);
                                if (this.textC.indexOf('-') == -1)this.textC = '-' + this.textC;


                            }

                            this.txtC.update(this.textC);

                            helper.GameHelper.remove(this.txtF);
                            this.txtF = null;

                            this.setPos('R');
                            this.setText();


                        }.bind(this));
                    }


                }
                else {

                    this.txtE = new view.component.TextView();
                    this.txtE.init(this.op);

                    this.txtF = new view.component.TextView();
                    this.txtF.init(this.op);

                    this.txtE.text = this.txtE.text.replace('-(+', '-').replace('+(-', '-').replace('-(-', '+').replace('+(+', '+').replace('(', '').replace(')', '').replace('+-', '-').replace('-+', '-');
                    this.txtF.text = this.txtF.text.replace('-(+', '-').replace('+(-', '-').replace('-(-', '+').replace('+(+', '+').replace('(', '').replace(')', '').replace('+-', '-').replace('-+', '-');
                    this.txtE.update(this.txtE.text,true);
                    this.txtF.update(this.txtF.text,true);

                    if (this.op.charAt(0) == '/') {
                        helper.GameHelper.setPos(this.txtE, this.equalTxt.x - this.txtE.width / 2, 0);
                        helper.GameHelper.setPos(this.txtF, this.txtC.x + this.txtC.width / 2 + 25, 0);

                    }
                    else {
                        helper.GameHelper.setPos(this.txtE, this.equalTxt.x - this.txtE.width / 2 - this.equalTxt.width / 2, 0);
                        helper.GameHelper.setPos(this.txtF, this.txtC.x + this.txtC.width / 2 + this.txtF.width / 2 + 10, 0);
                    }
                    this.txtContainer.addChild(this.txtE);
                    this.txtContainer.addChild(this.txtF);


                    var offset = this.txtE.width -5;
                    if(this.op.indexOf('*') != -1)
                    {
                        offset += 10;
                    }


                    if (this.txtB.text == '') {
                        TweenMax.to([this.txtA], 0.3, {x: "-=" + offset, onComplete: this.addBrackets.bind(this)});
                    }
                    else {
                        TweenMax.to([this.txtA, this.txtB], 0.3, {x: "-=" + offset, onComplete: this.addBrackets.bind(this)});
                    }


                    TweenMax.from([this.txtE, this.txtF], 0.3, {delay: 0.3, y: "-=50", alpha: 0});


                }


            }
            else {

                helper.GameHelper.remove(this.txtA);
                helper.GameHelper.remove(this.txtB);
                helper.GameHelper.remove(this.txtC);

                helper.GameHelper.remove(this.txtE);
                helper.GameHelper.remove(this.txtF);
                helper.GameHelper.remove(this.txtD1);
                helper.GameHelper.remove(this.txtD2);
                if (this.txtA)this.txtA = null;
                if (this.txtB)this.txtB = null;
                if (this.txtC)this.txtC = null;
                if (this.txtE)this.txtE = null;
                if (this.txtF)this.txtF = null;
                if (this.txtD1)this.txtD1 = null;
                if (this.txtD2)this.txtD2 = null;

                var arr = text.split('=');

                var splitChar;
                if(arr[0].indexOf('+') != -1)
                {
                    splitChar ='+';
                }
                else if(arr[0].indexOf('-') != -1)
                {
                    splitChar = '-';
                }

                var leftArr = arr[0].split(splitChar);
                leftArr[0]= leftArr[0].replace('x','');


                if (leftArr[0].indexOf('/') != -1) {
                    var larr = leftArr[0].split('/');

                    this.txtA = new view.component.TextView();
                    if (larr[0] == '1')larr[0] = '';
                    this.txtA.init(larr[0] + 'x/' + larr[1]);
                }
                else {

                    this.txtA = new view.component.TextView();
                    this.txtA.init(leftArr[0] + 'x');

                }


                this.txtB = new view.component.TextView();
                this.textB = (splitChar) ? splitChar + leftArr[1] : '';
                this.txtB.init(this.textB,true);

                this.txtC = new view.component.TextView();
                this.txtC.init(arr[1]);


                this.txtContainer.addChild(this.txtA);
                this.txtContainer.addChild(this.txtB);
                this.txtContainer.addChild(this.txtC);

                this.setPos('A');

            }


            this.setText();


        },

        setPos: function (type) {

            type = (type == undefined) ? '' : type;
            if(type == 'A'){
                helper.GameHelper.setPos(this.equalTxt, 0, 0);
            }

            if (this.txtB.text != '') {

                if(type != 'R')helper.GameHelper.setPos(this.txtB, this.equalTxt.x - this.equalTxt.width / 2 - this.txtB.width / 2 -5, 0);
                if(type != 'R')helper.GameHelper.setPos(this.txtA, this.txtB.x - this.txtA.width / 2 - this.txtB.width / 2 -5, 0);
                if(type != 'L')helper.GameHelper.setPos(this.txtC, this.equalTxt.x + this.txtC.width / 2 + this.equalTxt.width / 2 +10, 0);


            }
            else {
                if(type == 'A'){
                    helper.GameHelper.setPos(this.equalTxt, -20, 0);
                }

                if(type != 'R')helper.GameHelper.setPos(this.txtA, this.equalTxt.x - this.equalTxt.width / 2 - this.txtA.width / 2 -5, 0);
                if(type != 'R')helper.GameHelper.setPos(this.txtB, this.equalTxt.x - this.equalTxt.width / 2 - this.txtB.width / 2 -5, 0);
                if(type != 'L')helper.GameHelper.setPos(this.txtC, this.equalTxt.x + this.txtC.width / 2 + this.equalTxt.width / 2 + 10, 0);

            }


            if (this.txtF && type != 'L') helper.GameHelper.setPos(this.txtF, this.txtC.x + this.txtC.width / 2 + this.txtF.width / 2 + 10, 0);

            setTimeout(this.removeBrackets.bind(this), 0);

        },

        setColor: function (color) {

            this.color = color;

            if (this.txtA)this.txtA.setColor(color);
            if (this.txtB)this.txtB.setColor(color);
            if (this.txtC)this.txtC.setColor(color);
            if (this.txtE)this.txtE.setColor(color);
            if (this.txtF)this.txtF.setColor(color);
            if (this.txtD1)this.txtD1.setColor(color);
            if (this.txtD2)this.txtD2.setColor(color);
            if (this.br1)this.br1.setColor(color);
            if (this.br2)this.br2.setColor(color);
            if (this.br3)this.br3.setColor(color);
            if (this.br4)this.br4.setColor(color);


            if (this.equalTxt)this.equalTxt.setColor(color);


        },


        addBrackets: function () {

            if (this.op.indexOf('*') == -1) {
                return;
            }


            this.br1 = new view.component.TextView();
            this.br1.init('(');

            this.br2 = new view.component.TextView();
            this.br2.init(')');

            this.br3 = new view.component.TextView();
            this.br3.init('(');

            this.br4 = new view.component.TextView();
            this.br4.init(')');

            if (this.br1)this.br1.setColor(this.color);
            if (this.br2)this.br2.setColor(this.color);
            if (this.br3)this.br3.setColor(this.color);
            if (this.br4)this.br4.setColor(this.color);

            this.txtContainer.addChild(this.br1);
            this.txtContainer.addChild(this.br2);
            this.txtContainer.addChild(this.br3);
            this.txtContainer.addChild(this.br4);


            helper.GameHelper.setPos(this.br1, this.txtA.x - this.txtA.width / 2 - 3, 0);
            helper.GameHelper.setPos(this.br2, (this.txtB.text == '') ? this.txtA.x + this.txtA.width / 2 + 2 : this.txtB.x + this.txtB.width / 2 + 2, 0);
            helper.GameHelper.setPos(this.br3, this.txtC.x - this.txtC.width / 2 - 3, 0);
            helper.GameHelper.setPos(this.br4, this.txtC.x + this.txtC.width / 2 + 5, 0);


        },

        removeBrackets: function () {

            helper.GameHelper.remove(this.br1);
            if (this.br1)this.br1 = null;
            helper.GameHelper.remove(this.br2);
            if (this.br2)this.br2 = null;
            helper.GameHelper.remove(this.br3);
            if (this.br3)this.br3 = null;
            helper.GameHelper.remove(this.br4);
            if (this.br4)this.br4 = null;

        },


        setText: function () {

            var textA = this.txtA ? this.txtA.text : '';
            var textB = this.txtB ? this.txtB.text : '';
            var textC = this.txtC ? this.txtC.text : '';
            var textE = this.txtE ? this.txtE.text : '';
            var textF = this.txtF ? this.txtF.text : '';
            var textEqual = this.equalTxt ? this.equalTxt.text : '';


            this.text = textA + textB + textE + textEqual + textC + textF;


        },


        addSmokeAnim: function (posX, posY, type, callback) {

            var smoke = new lib.AppearAnim();
            smoke.x = posX;
            smoke.y = posY;
            smoke.scaleX=0.9;
            smoke.scaleY=0.9;

            this.addChild(smoke);

            helper.GameHelper.playSound('wipe');

            setTimeout(helper.GameHelper.remove, 200, smoke);
            if (callback) setTimeout(callback, 200);
        }


    },

    // STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */
        NAME: 'EqQuestionPanel'
    }
);


/**
 * Created by ankit on 3/8/14.
 */
