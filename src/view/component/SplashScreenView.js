
puremvc.define
(
	// CLASS INFO
	{
		name: 'view.component.SplashScreenView',
		/** @constructor */
		constructor: function (_medView)
		{
			//add all view colponenets to medView
			this.medView=_medView;
			this.addSplashView.bind(this)();
		}
	},

	// INSTANCE MEMBERS
	{
		medRef:null,

		addSplashView: function ()
		{
			this.startPlayAreaView.bind(this)();

		},


		startPlayAreaView: function (splash)
		{
			helper.GameHelper.remove(splash);
			AppConstants.STAGE.dispatchEvent(new createjs.Event('startPlayAreaView'));

		},


		handleEvent: function (evt)
		{
			switch(evt.type)
			{

				case 'click':
					break;

			}
		}


	},

	// STATIC MEMBERS
	{
		/**
		 * @static
		 * @type {string}
		 */
		NAME: 'SplashScreenView'
	}
);



