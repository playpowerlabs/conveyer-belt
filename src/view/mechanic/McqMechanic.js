
puremvc.define
(
    // CLASS INFO
    {
        name: 'view.mechanic.McqMechanic',
        constructor: function (_levelProxy,_playAreaView)
        {
            this.viewComponent = new createjs.MovieClip();
            this.levelProxy=_levelProxy;
            this.playAreaView = _playAreaView;
            this.playAreaView.quesBG.inefficientTxt.visible=false;

            this.quesVO=null;
            this.playAreaView.addChild(this.viewComponent);

            this.changeQuestion.bind(this)();


            if(this.levelProxy.demoFlag)
            {
                this.addTutorial();
            }



        }
    },

    // INSTANCE MEMBERS
    {
        quesCount:0,
        stepCount:0,
        wrgCount:0,

        changeQuestion:function ()
        {
            this.quesVO= this.levelProxy.getQuestionVO();
            window.quesText = this.quesVO.text;
            this.stepCount=0;
            this.quesCount += 1;

            if(this.quesVO.steps == 1)
            {
                this.playAreaView.quesBG.light2.visible=false;
            }
            else
            {
                this.playAreaView.quesBG.light2.visible=true;
            }


            if(this.quesCount <= this.levelProxy.levelQues)
            {
                this.tileCount = 0;

                if(!this.tileContainer){
                    this.addTiles.bind(this)();
                }
                this.addQuestionPanel.bind(this)();


            }
            else
            {
                //init scorecard
                setTimeout(this.startScoreScreenView.bind(this),100);
            }



        },

        addTutorial:function(){


            this.objectiveBox = new lib.ObjectiveBox();
            if(AppConstants.LEVEL == '1B')
            {
                this.objectiveBox.gotoAndStop(1);
            }
            else
            {
                this.objectiveBox.gotoAndStop(2);
            }

            helper.GameHelper.setPos(this.objectiveBox,509.65,-770);
            this.viewComponent.addChild(this.objectiveBox);

            this.objectiveTwn = new TimelineMax({delay:1});
            if(AppConstants.LEVEL == '1B')
            {
                this.objectiveTwn.append(TweenMax.to(this.objectiveBox,0.7,{y:-188,ease:Back.easeOut,onComplete: helper.GameHelper.playSound,onCompleteParams:['_1B_VO45']}));
            }
            else
            {
                this.objectiveTwn.append(TweenMax.to(this.objectiveBox,0.7,{y:-188,ease:Back.easeOut}));
            }

            this.objectiveBox.goBtn.addEventListener('mousedown', this.onGoClick.bind(this));

        },

        showInfoText:function(frame,sound){


            if(!this.infoMC)
            {
                this.infoMC = new lib.InfoMC();
                this.viewComponent.addChild(this.infoMC);
                this.infoMC.x = 512;
            }

            console.log('showInfoText',frame);
            this.infoMC.gotoAndStop(frame);
            this.infoMC.y = -300;
            this.infoTwn = TweenMax.fromTo(this.infoMC,0.4,{y:-300},{y:110,ease:Back.easeOut,onComplete: helper.GameHelper.playSound,onCompleteParams:[sound]});

        },

        onGoClick:function(){

            this.levelProxy.demoFlag=false;
            createjs.Sound.stop();

            helper.GameHelper.playSound('select');

            this.objectiveBox.goBtn.removeEventListener('mousedown', this.onGoClick.bind(this));
            TweenLite.to(this.objectiveBox.goBtn,0.15,{scaleX:"-=0.1",scaleY:"-=0.1",onComplete:TweenMax.to,onCompleteParams:[this.objectiveBox,0.7,{y:-770,ease:Back.easeIn,onComplete: this.removeObjectiveBox.bind(this)}]});

        },

        removeObjectiveBox:function(){

            if(this.objectiveBox && this.objectiveTwn)
            {
                helper.GameHelper.remove(this.objectiveBox);
                this.objectiveBox=null;
                this.objectiveTwn.stop();
                this.objectiveTwn =null;

            }

            if(AppConstants.LEVEL == '1B')
            {
                TweenMax.delayedCall(1,this.showInfoText.bind(this),[0,'_1B_VO2']);
            }
            else if(AppConstants.LEVEL == '1C')
            {
                TweenMax.delayedCall(1,this.showInfoText.bind(this),[2]);
            }
        },


        addQuestionPanel:function () {
            //load next question
            this.playAreaView.resetBtn.gotoAndStop(1);
            setTimeout(this.onResetClick.bind(this),1000);


            this.removeQuestionPanel.bind(this);
            this.quesPanel = new view.component.McqQuestionPanel();
            this.quesPanel.init(this.quesVO.text);
            this.viewComponent.addChild(this.quesPanel);
            helper.GameHelper.setPos(this.quesPanel,520,600);

            this.playAreaView.addChild(this.playAreaView.shutter);

            this.shutterTwn = TweenMax.to([ this.playAreaView.shutter.shutter1, this.playAreaView.shutter.shutter2],0.8,{delay:0.5,scaleX:0});
            this.prevQuesTxt = this.quesVO.text;



        },

        removeQuestionPanel:function(){
            if(this.quesPanel)
            {
                helper.GameHelper.remove(this.quesPanel);
                this.quesPanel=null;

            }

        },

        addTiles:function () {
            //load next question
            this.tileContainer = new createjs.MovieClip();
            this.viewComponent.addChild(this.tileContainer);
            helper.GameHelper.setPos(this.tileContainer,0,290);
            this.tileCount=0;
            this.wrgCount=0;
            //this.tileCount = parseInt(Math.random()*10);

            if(AppConstants.LEVEL == '1B')
            {
                this.tileSize = 45;
                this.tileWidth = 120;
                this.tileHeight = 45;
            }
            else if(AppConstants.LEVEL == '1C')
            {
                this.tileSize = 33;//40
                this.tileWidth = 150;//110
                this.tileHeight = 45;//45
            }

            for(var i=0;i<5;i++)
            {
                var tileValue = this.getRandTileValue.bind(this)();
                if(tileValue.indexOf('\n') != -1) this.tileHeight =70;
                var tile = new view.component.Tile();
                if(AppConstants.LEVEL == '1B')
                {
                    tile.init(tileValue,i,String(2+parseInt(Math.random()*2)),this.tileSize,this.tileWidth,this.tileHeight);
                    helper.GameHelper.setPos(tile,-100,12);

                }
                else if(AppConstants.LEVEL == '1C')
                {
                    tile.init(tileValue,i,'4',this.tileSize,this.tileWidth,this.tileHeight,false);
                    helper.GameHelper.setPos(tile,-100,0);

                }

                tile.addEventListener('mousedown', this.onTileClick.bind(this));
                this.tileContainer.addChild(tile);
                //helper.GameHelper.cache(tile);

                this.tileTwnTime = 13.6;
                tile.twn = TweenMax.to(tile,this.tileTwnTime,{repeat:-1,x:1200,delay:(this.tileTwnTime/5)*i,ease:Linear.easeNone,onUpdate:this.onTileTweenComplete.bind(this),onUpdateParams:[tile]});

            }



        },

        getRandTileValue:function(){
            var tileValue;
            var rand = Math.random();
            this.tileCount = parseInt(this.tileCount) + 1;
            var getRandType = function(arr,prevVal){
            var newval;

                if(prevVal == undefined || arr.length == 1)
                {
                    newval = arr[parseInt(Math.random()*arr.length)];

                }
                else
                {
                    newval= arr[parseInt(Math.random()*arr.length)];
                    while(newval == prevVal)
                    {
                        newval= arr[parseInt(Math.random()*arr.length)];
                    }
                }

                return newval;
            };


            console.log(' tilecount', this.tileCount);
            //if((this.tileCount % 3 == 0 || rand > 0.7) && (this.lastTileType == 'wrong'))
            if((this.tileCount % 3 == 0 ))
            {
                this.lastTileType= 'correct';
                //corect answer
                switch(this.quesVO.type)
                {
                    case 'A':
                    case 'B':
                    case 'C':
                        //for 1B
                        if(this.stepCount == 0)
                        {
                            tileValue = getRandType(this.quesVO.ca1,this.lastCorrectTileValue);
                        }
                        else if(this.stepCount == 1)
                        {
                            tileValue = getRandType(this.quesVO.ca2[this.quesVO.lastAnswer],this.lastCorrectTileValue);
                        }


                        break;
                    default :
                        //for 1C

                        if(this.stepCount == 0)
                        {
                            tileValue = (Array.isArray(this.quesVO.ca1)) ? getRandType(this.quesVO.ca1,this.lastCorrectTileValue) : getRandType(this.quesVO.ca1[this.quesVO.lastAnswer],this.lastCorrectTileValue);
                        }
                        else if(this.stepCount == 1)
                        {
                            tileValue = (Array.isArray(this.quesVO.ca2)) ? getRandType(this.quesVO.ca2,this.lastCorrectTileValue) : getRandType(this.quesVO.ca2[this.quesVO.lastAnswer],this.lastCorrectTileValue);
                        }
                        else if(this.stepCount == 2)
                        {
                            tileValue = (Array.isArray(this.quesVO.ca3)) ? getRandType(this.quesVO.ca3,this.lastCorrectTileValue) : getRandType(this.quesVO.ca3[this.quesVO.lastAnswer],this.lastCorrectTileValue);
                        }
                        else if(this.stepCount == 3)
                        {
                            tileValue = (Array.isArray(this.quesVO.ca4)) ? getRandType(this.quesVO.ca4,this.lastCorrectTileValue) : getRandType(this.quesVO.ca4[this.quesVO.lastAnswer],this.lastCorrectTileValue);
                        }

                        console.log('right tile value 1c step >',tileValue);

                        break;
                }

                this.lastCorrectTileValue = tileValue;
            }
            else
            {
                //generate wrong answer
                this.lastTileType= 'wrong';

                switch(this.quesVO.type)
                {
                    case 'A':
                    case 'B':
                    case 'C':
                        //for 1B
                        if(this.stepCount == 0)
                        {
                            tileValue = getRandType(this.quesVO.distractors[this.stepCount],this.lastWrongTileValue);

                            //just avoid correct answer distractors
                            while(Parser.evaluate(tileValue) == Parser.evaluate(window.quesText))
                            {
                                tileValue = getRandType(this.quesVO.distractors[this.stepCount],this.lastWrongTileValue);
                            }
                        }
                        else if(this.stepCount == 1)
                        {
                            console.log('new ques text',this.quesVO.result.text);
                            tileValue = getRandType(this.quesVO.distractors[this.stepCount][this.quesVO.result.text],this.lastWrongTileValue);

                            //just avoid correct answer distractors
                            while(Parser.evaluate(tileValue) == Parser.evaluate(window.quesText))
                            {
                                tileValue = getRandType(this.quesVO.distractors[this.stepCount][this.quesVO.result.text],this.lastWrongTileValue);
                            }
                        }



                        break;
                    default :
                        //for 1C
                        if(this.stepCount == 0)
                        {
                            if(this.quesVO.distractors[this.stepCount] != undefined){
                                tileValue = (Array.isArray(this.quesVO.distractors[this.stepCount])) ? getRandType(this.quesVO.distractors[this.stepCount],this.lastWrongTileValue) : getRandType(this.quesVO.distractors[this.stepCount][this.quesVO.lastAnswer],this.lastWrongTileValue);
                            }
                            console.log('wrong tile value 1c step 0>',tileValue);

                        }
                        else
                        {
                            if(this.quesVO.distractors[this.stepCount] != undefined){
                                tileValue = (Array.isArray(this.quesVO.distractors[this.stepCount])) ? getRandType(this.quesVO.distractors[this.stepCount],this.lastWrongTileValue) : getRandType(this.quesVO.distractors[this.stepCount][this.quesVO.lastAnswer],this.lastWrongTileValue);
                            }
                            console.log('wrong tile value 1c step > 0>',tileValue);
                        }

                        break;

                }

                if(tileValue != undefined)this.lastWrongTileValue = tileValue;

            }


            if(tileValue == undefined)
            {
                console.log('tile undefined break here');
            }

            return tileValue;
        },


        onTileTweenComplete:function(tile){

            if(tile.x >= 1150)//1150
            {
                tile.visible=true;
                var tileValue = this.getRandTileValue.bind(this)();
                console.log('new tile value',tileValue);
                tile.setText(tileValue);
            }

        },

        updateTileValues:function(){

            for(var i=0;i<5;i++)
            {
                var tile = this.tileContainer.getChildAt(i);
                console.log('updateTileValue',i,tile.x,tile.text);

                if(tile.x <= -100 || tile.x >= 1100)
                {
                    var tileValue = this.getRandTileValue.bind(this)();
                    tile.setText(tileValue);
                    //helper.GameHelper.cache(tile,true);
                    console.log('update tile',i,tile.x,tile.text);

                }

            }


        },

        onTileClick:function (e) {


            if(!this.levelProxy.demoFlag){


                helper.GameHelper.playSound('select');

                // body...
                this.deactivate();
                if(this.infoTwn){
                    this.infoTwn.reverse();
                }

                this.currentTile = e.currentTarget;
                this.startTileSplitAnim();
            }
        },

        startTileSplitAnim:function(){

            this.clamp1 = new lib.Clamp();
            this.viewComponent.addChild(this.clamp1);

            this.tile1 =  new view.component.Tile();

            this.tile1.init(this.currentTile.text,this.currentTile.index,this.currentTile.type,this.tileSize,this.tileWidth,this.tileHeight,(AppConstants.LEVEL == '1C') ? false : true);

            this.clamp1.x = 540;

            var clampTwn = new TimelineMax({});
            clampTwn.append(TweenMax.fromTo(this.clamp1,0.5,{x:this.currentTile.x,y:0},{y:(AppConstants.LEVEL == '1B') ? "+=250" : "+=280",onComplete:this.activateSplitTils.bind(this),onCompleteParams:[this.currentTile]}));
            clampTwn.append(TweenMax.to([this.clamp1,this.tile1],1,{y:"-=200"}));
            clampTwn.append(TweenMax.to([this.clamp1,this.tile1],1,{x:500}));
            clampTwn.append(TweenMax.to([this.clamp1,this.tile1],1,{y:(AppConstants.LEVEL == '1B') ? "+=280" : "+=250",alpha:1,ease:Back.easeOut,onComplete:this.addTileMask.bind(this)}));
            clampTwn.append(TweenMax.to([this.tile1],0.6,{y:"+=170",onComplete:this.removeTileMask.bind(this),onCompleteParams:[this.currentTile]}));
            clampTwn.append(TweenMax.to([this.clamp1],0.6,{y:0}));


        },

        activateSplitTils:function(){

            this.currentTile.visible=false;
            this.tile1.x = this.clamp1.x;
            if(AppConstants.LEVEL == '1B')
            {
                this.tile1.y= this.clamp1.y + 50;

            }
            else if(AppConstants.LEVEL == '1C')
            {
                this.tile1.y= this.clamp1.y + 20;
            }

            this.viewComponent.addChildAt(this.tile1,this.viewComponent.getChildIndex(this.clamp1) - 1);

        },

        addTileMask:function(){

            var mask1 = new createjs.Shape();
            mask1.graphics.beginFill("#ff0000").drawRect(338.05, 340, 338, 136);
            mask1.alpha=0;
            this.playAreaView.addChild(mask1);

            this.tile1.mask = mask1;


        },

        removeTileMask:function(){

            helper.GameHelper.playSound('drop');


            helper.GameHelper.remove(this.tile1.mask);
            this.tile1.mask = null;
            this.tile1.visible=false;

            this.onTileSplitAnimComplete(this.currentTile);

        },

        onTileSplitAnimComplete:function(){

            var result;
            if(AppConstants.LEVEL == '1B'){
                result = helper.EvalHelper.eval1B(this.quesPanel.text,this.currentTile.text,this.stepCount,this.quesVO);

            }
            else if(AppConstants.LEVEL == '1C'){
                result = helper.EvalHelper.eval1C(this.quesPanel.text,this.currentTile.text,this.stepCount,this.quesVO);
            }

            TweenLite.from(this.quesPanel,0.2,{/*scaleX:"-=0.1",scaleY:"-=0.1*/ alpha:0});

            if(result.correct)
            {
                this.quesVO.lastAnswer = result.ca;
                this.quesVO.result = result;
                this.quesPanel.updateText(result.text,true,result);
                this.quesPanel.setColor('#00CC33');
                this.finaltext = result.finaltext;

                setTimeout(this.addPositiveFeedback.bind(this),1000);
            }
            else
            {
                this.quesPanel.setColor('#ff0000');
                setTimeout(this.addNegativeFeedback.bind(this),0);

            }


        },

        addPositiveFeedback:function(){

            if(this.finaltext != undefined)
            {
                this.quesPanel.updateText(this.finaltext,true);
            }

            this.wrgCount =0;
            this.stepCount = this.stepCount+1;
            //if(this.stepCount == 1)this.updateTileValues.bind(this)();
            this.updateTileValues.bind(this)();

            AppConstants.STAGE.dispatchEvent(view.mediator.PlayAreaMediator.ON_CORRECT_ANS);

            setTimeout(this.onPositiveStepComplete.bind(this),1000);


        },


        onPositiveStepComplete:function(){

            helper.GameHelper.playSound('correct');

            this.prevQuesTxt = this.quesVO.result.text;

            if(this.stepCount == this.quesVO.steps){
                //Tween equation
                TweenMax.to(this.quesPanel,0.6,{x:"+=200",delay:1.3,alpha:0,ease:Back.easeIn,onComplete:this.updateProgressBar.bind(this)});
            }
            else
            {
                this.updateProgressBar();
            }

        },

        addNegativeFeedback:function(){


            this.wrgCount++;
            AppConstants.STAGE.dispatchEvent(view.mediator.PlayAreaMediator.ON_WRONG_ANS);
            //TweenLite.from(this.quesPanel,0.2,{/*scaleX:"+=0.2",scaleY:"+=0.2",ease:Back.easeIn*/ alpha:0});
            setTimeout(this.onNegativeStepComplete.bind(this),1000);

        },


        onNegativeStepComplete:function(){

            //Tween equation
            this.quesAnim = TweenMax.to(this.quesPanel,0.6,{x:"-=120",delay:1,alpha:0,ease:Back.easeIn,onComplete:this.startTrashAnim.bind(this)});

        },

        startTrashAnim:function(){
            helper.GameHelper.playSound('wrong');

            this.playAreaView.quesBG.scrapTxt.play();
            //setTimeout(this.playAreaView.quesBG.scrapTxt.gotoAndStop,100,1);

            TweenMax.staggerFrom([this.playAreaView.quesBG.trashBox.bubble1,this.playAreaView.quesBG.trashBox.bubble2,this.playAreaView.quesBG.trashBox.bubble3],0.1,{repeat:5,repeatDelay:0.01,y:"-=120"},0.05);
            setTimeout(this.onCompleteTrashAnim.bind(this),700);

        },

        onCompleteTrashAnim:function(){

            this.quesPanel.updateText(this.prevQuesTxt);
            this.quesPanel.setColor('#333333');

            this.playAreaView.resetBtn.gotoAndStop(1);
            setTimeout(this.onResetClick.bind(this),700);

            this.playAreaView.quesBG.scrapTxt.gotoAndStop(0);



            if(this.wrgCount >= 2)
            {
                this.wrgCount =0;
                this.shutterTwn.reverse();
                setTimeout(this.onQuestionComplete.bind(this),1000);

            }
            else
            {
                this.quesAnim.delay=0;
                this.quesAnim.reverse();
                setTimeout(this.activate.bind(this),1000);

            }



        },

        onResetClick:function(){
            this.playAreaView.resetBtn.gotoAndStop(0);


        },

        activate:function(){
            TweenMax.resumeAll();
            helper.GameHelper.activateObject(this.tileContainer);
            helper.GameHelper.activateObject(this.viewComponent);


        },

        deactivate:function(){
            TweenMax.pauseAll();
            helper.GameHelper.deactivateObject(this.tileContainer);
            helper.GameHelper.deactivateObject(this.viewComponent);

        },

        addSmokeAnim:function(posX,posY,mask,callback){

            var smoke = new lib.Smoke();
            smoke.x = posX;
            smoke.y=  posY;
            smoke.scaleX=1.2;
            smoke.scaleY=1.2;
            this.viewComponent.addChild(smoke);

            if(mask)
            {
                var smokeMask = new createjs.Shape();
                smokeMask.graphics.beginFill("#ff0000").drawRect(345,548,332,124);
                smokeMask.alpha=0;
                this.playAreaView.addChild(smokeMask);

                smoke.mask = smokeMask;
            }

            setTimeout(helper.GameHelper.remove,150,smoke);
            setTimeout(helper.GameHelper.remove,150,smokeMask);
            if(callback) setTimeout(callback,200);
        },


        updateProgressBar:function(){


            this.quesPanel.setColor('#333333');

            //var bgInc = 1.2/this.levelProxy.steps;
            //var bubbleInc = 450/this.levelProxy.steps;
            var noOfSteps;
            if(this.quesCount < 8)
            {
                noOfSteps =10*2;
            }
            else if(this.quesCount < 12)
            {
                noOfSteps =10*2;
            }
            else if(this.quesCount < 16)
            {
                noOfSteps = 10*2;
            }
            else
            {
                noOfSteps = 14*2;
            }
            var bgInc = 1.2/noOfSteps;
            var bubbleInc = 450/noOfSteps;


            var bgScale = (this.playAreaView.quesBG.progressBox.bg.scaleY + bgInc) < 1.2 ? (this.playAreaView.quesBG.progressBox.bg.scaleY + bgInc) : 1.2;

            TweenMax.staggerFrom([this.playAreaView.quesBG.progressBox.bubble1,this.playAreaView.quesBG.progressBox.bubble2,this.playAreaView.quesBG.progressBox.bubble3],0.1,{repeat:5,repeatDelay:0.01,y:"-=290"},0.05);
            TweenMax.to(this.playAreaView.quesBG.progressBox.bg,0.3,{scaleY:bgScale});
            TweenMax.to(this.playAreaView.quesBG.progressBox.bubbles,0.3,{y:"-="+bubbleInc});


            if(this.stepCount == 1)
            {
                this.playAreaView.quesBG.light1.gotoAndStop(1);
                if(this.quesCount == 1){
                    if(AppConstants.LEVEL == '1B')
                    {
                        this.showInfoText(1,'_1B_VO3');
                    }
                    else
                    {
                        this.showInfoText(3);

                    }
                }

            }
            else if(this.stepCount == 2)
            {
                this.playAreaView.quesBG.light2.gotoAndStop(1);
            }


            if(this.stepCount == this.quesVO.steps){

                helper.GameHelper.playSound('gloopy');

                this.shutterTwn.reverse();
                setTimeout(this.onQuestionComplete.bind(this),1000);

            }
            else
            {
                setTimeout(this.quesPanel.setColor,500,'#333333');
                this.activate();

            }




        },

        addRobotAnim:function(){
            var robot = new lib.Robot();
            this.viewComponent.addChild(robot);
            robot.x=904.3;
            robot.y=0;

            var box = new lib.Box();
            box.y=-235;
            box.x =811;
            this.viewComponent.addChild(box);

            var robotTwn = new TimelineMax({onComplete:this.onRobotAnimComplete.bind(this)});
            robotTwn.append(TweenMax.to(box,0.5,{y:501,onComplete:this.addSmokeAnim.bind(this),onCompleteParams:[920.25,728.45]}));
            robotTwn.append(TweenMax.to(robot,1,{delay:0.3,y:545,onStart:this.resetProgress.bind(this)}));
            robotTwn.append(TweenMax.to([box,robot],1,{y:-500}));


        },

        resetProgress:function(){
            this.playAreaView.quesBG.progressBox.bg.scaleY = 0.08;
            this.playAreaView.quesBG.progressBox.bubbles.y =  148.6;


        },

        onRobotAnimComplete:function(){

            //end level
            if(this.quesCount == this.levelProxy.levelQues)
            {
                this.startScoreScreenView();
            }
            else
            {
                this.changeQuestion();
                setTimeout(this.activate.bind(this),1000);

            }

        },

        onQuestionComplete:function(){

            this.playAreaView.quesBG.light1.gotoAndStop(0);
            this.playAreaView.quesBG.light2.gotoAndStop(0);
            this.removeQuestionPanel();

            if(this.quesCount % 4 == 0 || this.quesCount == this.levelProxy.levelQues)
            {
                this.deactivate();
                //add robot anim here
                this.addRobotAnim();
            }
            else
            {
                //AppConstants.STAGE.dispatchEvent(view.mediator.PlayAreaMediator.ON_QUESTION_COMPLETE);
                this.changeQuestion();
                setTimeout(this.activate.bind(this),1000);

            }

        },


        startScoreScreenView: function (argument) {
            // body...
            if(this.viewComponent)
            {
                helper.GameHelper.remove(this.viewComponent);
                this.viewComponent=null;
            }
            AppConstants.STAGE.dispatchEvent(view.mediator.PlayAreaMediator.START_SCORECARD,view.mediator.PlayAreaMediator.START_SCORECARD);

        }


    },

// STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */
        NAME : 'McqMechanic'

    }
);
