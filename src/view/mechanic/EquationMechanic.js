
puremvc.define
(
	// CLASS INFO
	{
		name: 'view.mechanic.EquationMechanic',
        constructor: function (_levelProxy,_playAreaView)
		{
		    this.viewComponent = new createjs.MovieClip();
            this.levelProxy=_levelProxy;
            this.playAreaView = _playAreaView;
            this.quesVO=null;
            this.playAreaView.addChild(this.viewComponent);

            this.changeQuestion.bind(this)();


            if(this.levelProxy.demoFlag)
            {
                this.addTutorial();
            }



        }
	},

	// INSTANCE MEMBERS
	{
        quesCount:0,
        stepCount:0,
        wrgCount:0,

		changeQuestion:function ()
		{
            this.quesVO= this.levelProxy.getQuestionVO();
            this.stepCount=0;
			this.quesCount += 1;
            this.distractors = helper.GameHelper.shuffle(this.quesVO.distractors[this.stepCount].split(','));

            if(this.quesVO.steps == 1)
            {
                this.playAreaView.quesBG.light2.visible=false;
            }
            else
            {
                this.playAreaView.quesBG.light2.visible=true;
            }


            if(this.quesCount <= this.levelProxy.levelQues)
			{
                this.tileCount = 0;
                if(!this.tileContainer){
                    this.addTiles.bind(this)();
                }

                this.addQuestionPanel.bind(this)();
                this.updateTileValues.bind(this)();

            }
			else
			{
				//init scorecard
				setTimeout(this.startScoreScreenView.bind(this),100);
			}



        },

        addTutorial:function(){


            this.objectiveBox = new lib.ObjectiveBox();
            this.objectiveBox.gotoAndStop(0);

            helper.GameHelper.setPos(this.objectiveBox,509.65,-770);
            this.viewComponent.addChild(this.objectiveBox);

            this.objectiveTwn = new TimelineMax({delay:1});
            this.objectiveTwn.append(TweenMax.to(this.objectiveBox,0.7,{y:-188,ease:Back.easeOut}));

            this.objectiveBox.goBtn.addEventListener('mousedown', this.onGoClick.bind(this));

        },


        onGoClick:function(){
            this.levelProxy.demoFlag=false;

            helper.GameHelper.playSound('select');

            this.objectiveBox.goBtn.removeEventListener('mousedown', this.onGoClick.bind(this));
            TweenLite.to(this.objectiveBox.goBtn,0.15,{scaleX:"-=0.1",scaleY:"-=0.1",onComplete:TweenMax.to,onCompleteParams:[this.objectiveBox,0.7,{y:-770,ease:Back.easeIn,onComplete: this.removeObjectiveBox.bind(this)}]});

        },

        removeObjectiveBox:function(){

            if(this.objectiveBox && this.objectiveTwn)
            {
                helper.GameHelper.remove(this.objectiveBox);
                this.objectiveBox=null;
                this.objectiveTwn.stop();
                this.objectiveTwn =null;

            }


        },


		addQuestionPanel:function () {
			//load next question
            this.playAreaView.resetBtn.gotoAndStop(1);
            setTimeout(this.onResetClick.bind(this),1000);


            this.removeQuestionPanel.bind(this);
            this.quesPanel = new view.component.EqQuestionPanel();
            this.quesPanel.init(this.quesVO.text);
            this.viewComponent.addChild(this.quesPanel);
            helper.GameHelper.setPos(this.quesPanel,520,600);

            this.playAreaView.addChild(this.playAreaView.shutter);

            this.shutterTwn = TweenMax.to([ this.playAreaView.shutter.shutter1, this.playAreaView.shutter.shutter2],0.8,{delay:0.5,scaleX:0});
            this.prevQuesTxt = this.quesPanel.text;



        },

        removeQuestionPanel:function(){
            if(this.quesPanel)
            {
                helper.GameHelper.remove(this.quesPanel);
                this.quesPanel=null;

            }

        },

        addTiles:function () {
            //load next question
            this.tileContainer = new createjs.MovieClip();
            this.viewComponent.addChild(this.tileContainer);
            helper.GameHelper.setPos(this.tileContainer,0,290);
            //this.distractors = helper.GameHelper.shuffle(this.quesVO.distractors[this.stepCount].split(','));
            this.tileCount=0;
            this.wrgCount=0;
            //this.tileCount = parseInt(Math.random()*10);

            for(var i=0;i<5;i++)
            {
                    var tileValue = this.getRandTileValue();
                    var tile = new view.component.Tile();
                    tile.init(tileValue,i);
                    tile.addEventListener('mousedown', this.onTileClick.bind(this));
                    this.tileContainer.addChild(tile);
                    helper.GameHelper.cache(tile);

                    helper.GameHelper.setPos(tile,-100,0);
                    this.tileTwnTime = 13.6;
                    tile.twn = TweenMax.to(tile,this.tileTwnTime,{repeat:-1,x:1100,delay:(this.tileTwnTime/5)*i,ease:Linear.easeNone,onUpdate:this.onTileTweenComplete.bind(this),onUpdateParams:[tile]});

            }


            console.log('distractors',this.stepCount,this.distractors.toString());

        },


        getRandTileValue:function(){
            var tileValue;
            var rand = Math.random();
            this.tileCount++;

            console.log(this.tileCount,rand,this.lastTileType);
            if((this.tileCount % 3 == 0 || rand > 0.8) && (this.lastTileType == 'wrong') && ((this.stepCount == 0 && this.lastTileValue && this.lastTileValue != this.quesVO.ca1) || ( this.stepCount> 0 && this.lastTileValue && this.lastTileValue != this.quesVO.ca2)))
            {
                this.lastTileType = 'correct';
                if(this.stepCount == 0)
                {
                    tileValue = this.quesVO.ca1;
                }
                else
                {
                    tileValue = this.quesVO.ca2;
                }

            }
            else
            {

                this.lastTileType = 'wrong';
                tileValue = this.distractors[parseInt(Math.random()*this.distractors.length)];
                while(tileValue == '@0' || tileValue == '+1/0' || tileValue == '-1/0' || tileValue == '*1/0' || tileValue == '*0' || tileValue == '@1/0')
                {
                    rand = Math.random()*this.distractors.length;
                    tileValue = this.distractors[parseInt(Math.random()*this.distractors.length)];
                }

            }

            this.lastTileValue = tileValue;

            return tileValue;
        },


        onTileTweenComplete:function(tile){

            //console.log(tile.text+"twn complete.");

            ///helper.GameHelper.setPos(tile,-100,0);

            if(tile.x >= 1095)
            {
                tile.visible=true;
                var tileValue = this.getRandTileValue();
                tile.setText(tileValue);
                helper.GameHelper.cache(tile,true);
            }
            ///tile.twn.delay=0;
            ///tile.twn.restart();


        },

        updateTileValues:function(){

            for(var i=0;i<5;i++)
            {
                var tile = this.tileContainer.getChildAt(i);
                console.log('tile',i,tile.x,tile.text);

                if(tile.x < 0 || tile.x > 1000)
                {
                    var tileValue = this.getRandTileValue();
                    tile.setText(tileValue);
                    helper.GameHelper.cache(tile,true);
                    console.log('update tile',i,tile.x,tile.text);

                }

            }

        },

        onTileClick:function (e) {

            if(!this.levelProxy.demoFlag){
                this.removeObjectiveBox();

                helper.GameHelper.playSound('select');

                // body...
                this.deactivate();

                this.currentTile = e.currentTarget;
                this.startTileSplitAnim();
            }


		},

        startTileSplitAnim:function(){

            this.clamp2 = new lib.Clamp();
            this.viewComponent.addChild(this.clamp2);

            this.clamp1 = new lib.Clamp();
            this.viewComponent.addChild(this.clamp1);

            this.tile1 =  new view.component.Tile();
            this.tile1.init(this.currentTile.text,this.currentTile.index,'1');
            this.tile2 =  new view.component.Tile();
            this.tile2.init(this.currentTile.text,this.currentTile.index,'1');


            this.clamp1.x = 540;
            this.clamp2.x = 580;

            var clampTwn = new TimelineMax({});
            clampTwn.appendMultiple([TweenMax.fromTo(this.clamp1,0.5,{x:this.currentTile.x - 30,y:0},{y:"+=300"}),
                TweenMax.fromTo(this.clamp2,0.5,{x:this.currentTile.x + 30,y:0},{y:"+=300",onComplete:this.activateSplitTils.bind(this),onCompleteParams:[this.currentTile]})],0,"simple",0);
            clampTwn.append(TweenMax.to([this.clamp1,this.clamp2,this.tile1,this.tile2],1,{y:"-=200"}));
            clampTwn.appendMultiple([TweenMax.to([this.clamp1,this.tile1],1,{x:410}),TweenMax.to([this.clamp2,this.tile2],1,{x:610})],0,"simple",0);
            clampTwn.append(TweenMax.to([this.clamp1,this.clamp2,this.tile1,this.tile2],1,{y:"+=250",alpha:1,ease:Back.easeOut,onComplete:this.addTileMask.bind(this)}));
            clampTwn.append(TweenMax.to([this.tile1,this.tile2],0.6,{y:"+=170",onComplete:this.removeTileMask.bind(this),onCompleteParams:[this.currentTile]}));
            clampTwn.append(TweenMax.to([this.clamp1,this.clamp2],0.6,{y:0}));


        },

        activateSplitTils:function(){

          
            this.currentTile.visible=false;
            this.tile1.x = this.clamp1.x;
            this.tile1.y= this.clamp1.y;
            this.tile2.x = this.clamp2.x;
            this.tile2.y= this.clamp2.y;

            this.viewComponent.addChildAt(this.tile1,this.viewComponent.getChildIndex(this.clamp1) - 1);
            this.viewComponent.addChildAt(this.tile2,this.viewComponent.getChildIndex(this.clamp2) - 1);
        },

        addTileMask:function(){

            var mask1 = new createjs.Shape();
            mask1.graphics.beginFill("#ff0000").drawRect(338.05, 345.1, 138, 136);
            mask1.alpha=0;
            this.playAreaView.addChild(mask1);

            var mask2 = new createjs.Shape();
            mask2.graphics.beginFill("#ff0000").drawRect(543.1, 345.10, 138, 136);
            mask2.alpha=0;
            this.playAreaView.addChild(mask2);

            this.tile1.mask = mask1;
            this.tile2.mask = mask2;


        },

        removeTileMask:function(){

            helper.GameHelper.playSound('drop');


            helper.GameHelper.remove(this.tile1.mask);
            helper.GameHelper.remove(this.tile2.mask);

            this.tile1.mask = null;
            this.tile2.mask = null;

            this.tile1.visible=false;
            this.tile2.visible=false;

            this.onTileSplitAnimComplete(this.currentTile);

        },

        onTileSplitAnimComplete:function(){

            this.equation = helper.EvalHelper.simplify(this.quesPanel.text,this.currentTile.text);
            this.quesPanel.updateText(this.quesPanel.text,this.currentTile.text);
            TweenLite.from(this.quesPanel,0.2,{/*scaleX:"-=0.1",scaleY:"-=0.1*/ alpha:0});

            if(this.equation.correct)
            {
                this.quesPanel.setColor('#00CC33');

                setTimeout(this.addPositiveFeedback.bind(this),1500);
            }
            else
            {
                this.quesPanel.setColor('#ff0000');

                setTimeout(this.addNegativeFeedback.bind(this),1500);

            }


        },

        addPositiveFeedback:function(){

            this.quesPanel.updateText(this.quesPanel.text,this.currentTile.text);
            this.wrgCount =0;
            this.stepCount = this.stepCount+1;
            if(this.quesVO.distractors[this.stepCount])
            {
                this.distractors = helper.GameHelper.shuffle(this.quesVO.distractors[this.stepCount].split(','));
            }

            console.log('distractors',this.stepCount,this.distractors.toString());
            AppConstants.STAGE.dispatchEvent(view.mediator.PlayAreaMediator.ON_CORRECT_ANS);

            setTimeout(this.onPositiveStepComplete.bind(this),2000);


        },


        onPositiveStepComplete:function(){

            helper.GameHelper.playSound('correct');

            this.prevQuesTxt = this.quesPanel.text;

            if(this.stepCount == this.quesVO.distractors.length){
                //Tween equation
                TweenMax.to(this.quesPanel,0.6,{x:"+=200",delay:1.3,alpha:0,ease:Back.easeIn,onComplete:this.updateProgressBar.bind(this)});
            }
            else
            {
                this.updateProgressBar();
            }

        },

        addNegativeFeedback:function(){


            this.wrgCount++;
            AppConstants.STAGE.dispatchEvent(view.mediator.PlayAreaMediator.ON_WRONG_ANS);

            this.quesPanel.updateText(this.quesPanel.text,this.currentTile.text);
            TweenLite.from(this.quesPanel,0.2,{/*scaleX:"+=0.2",scaleY:"+=0.2",ease:Back.easeIn*/ alpha:0});
            setTimeout(this.onNegativeStepComplete.bind(this),1500);

        },


        onNegativeStepComplete:function(){

            //Tween equation
           this.quesAnim = TweenMax.to(this.quesPanel,0.6,{x:"-=120",delay:1,alpha:0,ease:Back.easeIn,onComplete:this.startTrashAnim.bind(this)});

        },

        startTrashAnim:function(){
            helper.GameHelper.playSound('wrong');

            this.playAreaView.quesBG.inefficientTxt.play();
            //setTimeout(this.playAreaView.quesBG.inefficientTxt.gotoAndStop,100,1);

            TweenMax.staggerFrom([this.playAreaView.quesBG.trashBox.bubble1,this.playAreaView.quesBG.trashBox.bubble2,this.playAreaView.quesBG.trashBox.bubble3],0.1,{repeat:5,repeatDelay:0.01,y:"-=120"},0.05);
            setTimeout(this.onCompleteTrashAnim.bind(this),700);

        },

        onCompleteTrashAnim:function(){
            this.quesPanel.updateText(this.prevQuesTxt);
            this.quesPanel.setColor('#333333');

            this.playAreaView.resetBtn.gotoAndStop(1);
            setTimeout(this.onResetClick.bind(this),700);

            this.playAreaView.quesBG.inefficientTxt.gotoAndStop(0);


            if(this.wrgCount >= 2)
            {
                this.wrgCount =0;
                this.shutterTwn.reverse();
                setTimeout(this.onQuestionComplete.bind(this),1000);


            }
            else
            {
                this.quesAnim.delay=0;
                this.quesAnim.reverse();
                setTimeout(this.activate.bind(this),1000);

            }


        },

        onResetClick:function(){
            this.playAreaView.resetBtn.gotoAndStop(0);


        },

        activate:function(){
            TweenMax.resumeAll();
            helper.GameHelper.activateObject(this.tileContainer);
            helper.GameHelper.activateObject(this.viewComponent);


        },

        deactivate:function(){
            TweenMax.pauseAll();
            helper.GameHelper.deactivateObject(this.tileContainer);
            helper.GameHelper.deactivateObject(this.viewComponent);

        },

        addSmokeAnim:function(posX,posY,mask,callback){

            var smoke = new lib.Smoke();
            smoke.x = posX;
            smoke.y=  posY;
            smoke.scaleX=1.2;
            smoke.scaleY=1.2;
            this.viewComponent.addChild(smoke);

            if(mask)
            {
                var smokeMask = new createjs.Shape();
                smokeMask.graphics.beginFill("#ff0000").drawRect(345,548,332,124);
                smokeMask.alpha=0;
                this.playAreaView.addChild(smokeMask);

                smoke.mask = smokeMask;
            }

            setTimeout(helper.GameHelper.remove,150,smoke);
            setTimeout(helper.GameHelper.remove,150,smokeMask);
            if(callback) setTimeout(callback,200);
        },


        updateProgressBar:function(){


            this.quesPanel.setColor('#333333');

            //var bgInc = 1.2/this.levelProxy.steps;
            //var bubbleInc = 450/this.levelProxy.steps;
            var noOfSteps;
            if(this.quesCount < 8)
            {
                noOfSteps =4*2;
            }
            else if(this.quesCount < 12)
            {
                noOfSteps =7*2;
            }
            else if(this.quesCount < 16)
            {
                noOfSteps = 10*2;
            }
            else
            {
                noOfSteps = 4*2;
            }
            var bgInc = 1.2/noOfSteps;
            var bubbleInc = 450/noOfSteps;


            var bgScale = this.playAreaView.quesBG.progressBox.bg.scaleY + bgInc;
            TweenMax.staggerFrom([this.playAreaView.quesBG.progressBox.bubble1,this.playAreaView.quesBG.progressBox.bubble2,this.playAreaView.quesBG.progressBox.bubble3],0.1,{repeat:5,repeatDelay:0.01,y:"-=290"},0.05);
            TweenMax.to(this.playAreaView.quesBG.progressBox.bg,0.3,{scaleY:bgScale});
            TweenMax.to(this.playAreaView.quesBG.progressBox.bubbles,0.3,{y:"-="+bubbleInc});


            if(this.stepCount == 1)
            {
                this.playAreaView.quesBG.light1.gotoAndStop(1);

            }
            else if(this.stepCount == 2)
            {
                this.playAreaView.quesBG.light2.gotoAndStop(1);
            }

            if(this.stepCount == this.quesVO.distractors.length){

                helper.GameHelper.playSound('gloopy');

                this.shutterTwn.reverse();
                setTimeout(this.onQuestionComplete.bind(this),1000);

            }
            else
            {
                setTimeout(this.quesPanel.setColor,500,'#333333');
                this.activate();

            }



        },

        addRobotAnim:function(){
            var robot = new lib.Robot();
            this.viewComponent.addChild(robot);
            robot.x=904.3;
            robot.y=0;

            var box = new lib.Box();
            box.y=-235;
            box.x =811;
            this.viewComponent.addChild(box);

            var robotTwn = new TimelineMax({onComplete:this.onRobotAnimComplete.bind(this)});
            robotTwn.append(TweenMax.to(box,0.5,{y:501,onComplete:this.addSmokeAnim.bind(this),onCompleteParams:[920.25,728.45]}));
            robotTwn.append(TweenMax.to(robot,1,{delay:0.3,y:545,onStart:this.resetProgress.bind(this)}));
            robotTwn.append(TweenMax.to([box,robot],1,{y:-500}));


        },

        resetProgress:function(){
            this.playAreaView.quesBG.progressBox.bg.scaleY = 0.08;
            this.playAreaView.quesBG.progressBox.bubbles.y =  148.6;


        },

        onRobotAnimComplete:function(){

            //end level
            if(this.quesCount == this.levelProxy.levelQues)
            {
                this.startScoreScreenView();
            }
            else
            {
                this.changeQuestion();
                setTimeout(this.activate.bind(this),1000);

            }

        },

        onQuestionComplete:function(){

            this.playAreaView.quesBG.light1.gotoAndStop(0);
            this.playAreaView.quesBG.light2.gotoAndStop(0);
            this.removeQuestionPanel();

            if(this.quesCount % 4 == 0 || this.quesCount == this.levelProxy.levelQues)
            {
                this.deactivate();

                //add robot anim here
                this.addRobotAnim();
            }
            else
            {
                //AppConstants.STAGE.dispatchEvent(view.mediator.PlayAreaMediator.ON_QUESTION_COMPLETE);
                this.changeQuestion();
                setTimeout(this.activate.bind(this),1000);

            }

        },


		startScoreScreenView: function (argument) {
			// body...
            if(this.viewComponent)
            {
                helper.GameHelper.remove(this.viewComponent);
                this.viewComponent=null;
            }
            AppConstants.STAGE.dispatchEvent(view.mediator.PlayAreaMediator.START_SCORECARD,view.mediator.PlayAreaMediator.START_SCORECARD);

		}


	},

	// STATIC MEMBERS
	{
		/**
		 * @static
		 * @type {string}
		 */
		NAME : 'EquationMechanic'

	}
);
