/**
 * @class view.TextComponentMediator
 * @extends puremvc.Mediator
 */
puremvc.define(
    // CLASS INFO
    {
        name: 'view.mediator.SplashScreenMediator',
        parent: puremvc.Mediator
    },

    // INSTANCE MEMBERS
    {

        /** @override */
        listNotificationInterests: function() {
            return [
                model.proxy.LevelProxy.LEVEL_LOADED
            ]
        },

        /** @override */
        handleNotification: function(note) {
            switch ( note.getName() )
            {
            	case model.proxy.LevelProxy.LEVEL_LOADED:
                    //on level loaded
                    this.addSplashView.bind(this)();

                break;

            }
        },

        /** @override */
        onRegister: function() {

            this.setViewComponent(new createjs.Container());
            AppConstants.STAGE_REF.addChild(this.viewComponent);

            //load game level
            var levelProxy=this.facade.retrieveProxy(model.proxy.LevelProxy.NAME);

            var levelno = helper.GameHelper.getQueryVariable('level') || '1A';
            AppConstants.LEVEL = levelno;
            AppConstants.GRADE = parseInt(helper.GameHelper.getQueryVariable('grade')) || 3;
            AppConstants.TARGET = helper.GameHelper.getQueryVariable('target');
            console.log('load level:'+levelno);
            //levelProxy.loadLevel('xml/level'+levelno+'.xml');
            if(AppConstants.LEVEL == '1A')
            {
                AppConstants.MECHANIC = 'eq';
                levelProxy.makeEqLevelConfig();

            }
            else
            {
                AppConstants.MECHANIC = 'mcq';
                levelProxy.makeMcqLevelConfig();

            }

        },


        addSplashView: function() {

            //AppConstants.STAGE.addEventListener( 'startPlayAreaView', this.startPlayAreaView.bind(this));
            //var splash= new view.component.SplashScreenView(this.viewComponent);
            this.startPlayAreaView.bind(this)();

        },


        startPlayAreaView: function() {

            ApplicationFacade.getInstance().removeMediator(view.mediator.SplashScreenMediator.NAME);
            ApplicationFacade.getInstance().registerMediator(new view.mediator.PlayAreaMediator);

        },



        /** @override */
        onRemove: function() {
           this.viewComponent.removeAllEventListeners();
           helper.GameHelper.removeAll(this.viewComponent);
           this.setViewComponent(null);
           helper.GameHelper.removeAll();


        }


    },

    // STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */
        NAME: 'SplashScreenMediator',
        START_GAME:'StartGame'
    }
);
