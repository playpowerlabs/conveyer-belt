/**
 * @class view.TextComponentMediator
 * @extends puremvc.Mediator
 */
puremvc.define
(
	// CLASS INFO
	{
		name: 'view.mediator.ScoreScreenMediator',
		parent: puremvc.Mediator
	},

	// INSTANCE MEMBERS
	{
		/** @override */
		listNotificationInterests: function ()
		{
			return [
					// model.proxy.TextProxy.TEXT_CHANGED,
					// AppConstants.PALINDROME_DETECTED
					]
		},

		/** @override */
		handleNotification: function (note)
		{
			// switch ( note.getName() )
			// {
			// 	case model.proxy.TextProxy.TEXT_CHANGED:
			// 		this.viewComponent.setOutputText( note.getBody() );
			// 		break;

			// 	case AppConstants.PALINDROME_DETECTED:
			// 		this.viewComponent.setIsPalindrome( true );
			// 		break;
			// }
		},

		/** @override */
		onRegister: function ()
		{
			this.setViewComponent( new createjs.MovieClip());
			AppConstants.STAGE_REF.addChild(this.viewComponent);
			// add the TextComponentMediator as a listener for TEXT_CHANGED events

			this.initScoreCardView.bind(this)();


		},

		initScoreCardView:function (argument) {
			scoreScreen = new lib.ScoreScreen();
			AppConstants.STAGE_REF.addChild(scoreScreen);
			scoreScreen.scoreTxt.text = view.component.Scorebar.SCORE+"";
			scoreScreen.bestScoreTxt.text=view.component.Scorebar.SCORE+"";
			scoreScreen.nextBtn.alpha=0.3;
			scoreScreen.menuBtn.alpha=0.3;
			scoreScreen.replayBtn.addEventListener('click', this.onClickReplay.bind(this));

			helper.GameHelper.setFontStyle(scoreScreen.scoreTxt,"CasualMac","60",'#ffff00');
			helper.GameHelper.setFontStyle(scoreScreen.bestScoreTxt,"CasualMac","40",'#ffffff');
			helper.GameHelper.setFontStyle(scoreScreen.starpoint1,"CasualMac","20",'#ffffff');
			helper.GameHelper.setFontStyle(scoreScreen.starpoint2,"CasualMac","20",'#ffffff');
			helper.GameHelper.setFontStyle(scoreScreen.starpoint3,"CasualMac","20",'#ffffff');


			helper.GameHelper.scaleObject(scoreScreen.star1,0,0)
			helper.GameHelper.scaleObject(scoreScreen.star2,0,0)
			helper.GameHelper.scaleObject(scoreScreen.star3,0,0)

			var score = view.component.Scorebar.SCORE;
            this.levelProxy=ApplicationFacade.getInstance().retrieveProxy(model.proxy.LevelProxy.NAME);
            scoreScreen.starpoint1.text =this.levelProxy.star1;
            scoreScreen.starpoint2.text =this.levelProxy.star2;
            scoreScreen.starpoint3.text =this.levelProxy.star3;

            if(score >=this.levelProxy.star3 )
			{
				helper.GameHelper.scaleObject(scoreScreen.star1,1,1)
				helper.GameHelper.scaleObject(scoreScreen.star2,1,1)
				helper.GameHelper.scaleObject(scoreScreen.star3,1,1)

			}
			else if(score>=this.levelProxy.star2)
			{
				helper.GameHelper.scaleObject(scoreScreen.star1,1,1)
				helper.GameHelper.scaleObject(scoreScreen.star2,1,1)
			}
			else if(score>=this.levelProxy.star1)
			{
				helper.GameHelper.scaleObject(scoreScreen.star1,1,1)
			}



		},

		onClickReplay:function (e) {
			// body...
			var bub=e.currentTarget;
			bub.visible=false;

			view.mediator.PlayAreaMediator.addExplosion(bub.x-60,bub.y-60,this.startPlayAreaView.bind(this));

		},

		startPlayAreaView: function (argument) {
			// body...
			this.facade.removeMediator( view.mediator.ScoreScreenMediator.NAME );
            this.facade.registerMediator( new view.mediator.PlayAreaMediator );

		},

		/** @override */
		onRemove: function ()
		{
			this.viewComponent.removeAllEventListeners();
			helper.GameHelper.removeAll(this.viewComponent);
			this.setViewComponent(null);
			helper.GameHelper.removeAll();
		}


	},

	// STATIC MEMBERS
	{
		/**
		 * @static
		 * @type {string}
		 */
		NAME: 'ScoreScreenMediator'
	}
);