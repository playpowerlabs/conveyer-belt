/**
 * @class view.TextComponentMediator
 * @extends puremvc.Mediator
 */
puremvc.define(
    // CLASS INFO
    {
        name: 'view.mediator.PlayAreaMediator',
        parent: puremvc.Mediator

    },

    // INSTANCE MEMBERS
    {
        levelProxy:null,
        playAreaView:null,
        curMechanicView:null,



        /** @override */
        listNotificationInterests: function() {
            return [
                // model.proxy.TextProxy.TEXT_CHANGED,
                // AppConstants.PALINDROME_DETECTED

            ]
        },

        /** @override */
        handleNotification: function(note) {
            switch (note.getName()) {
                /*case AppConstants.PALINDROME_DETECTED:
                    this.viewComponent.setIsPalindrome(true);
                    break;
                    */
            }
        },

        /** @override */
        onRegister: function() {
            //medRef = this;
            this.setViewComponent(new createjs.MovieClip());
            AppConstants.STAGE_REF.addChild(this.viewComponent);

            this.levelProxy=ApplicationFacade.getInstance().retrieveProxy(model.proxy.LevelProxy.NAME);
            this.levelProxy.onReset();

            //INIT PLAY AREA
            this.initPlayAreaView.bind(this)();

        },

        initPlayAreaView: function(argument) {

            console.log('initPlayAreaView');
            this.playAreaView = new lib.PlayAreaView();
            this.viewComponent.addChild(this.playAreaView);
            this.playAreaView.clickToClose.visible=false;

            if(AppConstants.MECHANIC == 'mcq')
            {
                this.playAreaView.quesBG.inefficientTxt.visible=false;
            }
            else
            {
                this.playAreaView.quesBG.chute.visible=false;
                this.playAreaView.quesBG.scrapTxt.visible=false;
            }

            //add play btn
            this.titlePage = new lib.TitlePage();
            if(AppConstants.LEVEL == '1C')
            {
                this.titlePage.TitleMC.gotoAndStop(2);
            }
            else if(AppConstants.LEVEL == '1B')
            {
               this.titlePage.TitleMC.gotoAndStop(1);
            }
            else
            {
                this.titlePage.TitleMC.gotoAndStop(0);
            }
            helper.GameHelper.setPos(this.titlePage,0,0);
            this.viewComponent.addChild(this.titlePage);
            this.titlePage.playBtn.addEventListener('mousedown', this.onPlayClick.bind(this));

            this.addTweenEffects();
            this.onPause();

        },

        addTweenEffects:function(){

            this.playAreaView.resetBtn.stop();
            this.playAreaView.quesBG.light1.stop();
            this.playAreaView.quesBG.light2.stop();
            if(AppConstants.MECHANIC == 'mcq'){
                this.playAreaView.quesBG.scrapTxt.gotoAndStop(0);
            }
            else
            {
                this.playAreaView.quesBG.inefficientTxt.gotoAndStop(0);

            }

            var shape = new createjs.Shape();
            shape.graphics.beginFill("#ff0000").drawRect(261.2, 28.05, 556, 70);
            shape.alpha=0;
            this.viewComponent.addChild(shape);

            this.playAreaView.box1.mask = shape;
            this.playAreaView.box2.mask = shape;

            this.beltTwn = TweenMax.to([this.playAreaView.belt.belt1,this.playAreaView.belt.belt2,this.playAreaView.belt.belt3,this.playAreaView.belt.belt4], 1, {ease:Linear.easeNone,repeat:-1,x:'-=180'});
            this.tierTwn = TweenMax.to([this.playAreaView.tiers.tier1,this.playAreaView.tiers.tier2,this.playAreaView.tiers.tier3,this.playAreaView.tiers.tier4,this.playAreaView.belt.w1,this.playAreaView.belt.w2,this.playAreaView.belt.w3,this.playAreaView.belt.w4,this.playAreaView.belt.w5,this.playAreaView.belt.w6,this.playAreaView.belt.w7,this.playAreaView.belt.w8,this.playAreaView.belt.w9,this.playAreaView.belt.w10,this.playAreaView.belt.w11,this.playAreaView.belt.w12], 1.8, {ease:Linear.easeNone,repeat:-1,rotation:'-=360'});

            var boxTwn = new TimelineMax({repeatDelay:2,repeat:-1});
            boxTwn.append(TweenMax.to([this.playAreaView.box1],10,{x:"-=700",ease:Linear.easeNone}));

            var boxTwn = new TimelineMax({delay:6,repeatDelay:2,repeat:-1});
            boxTwn.append(TweenMax.to([this.playAreaView.box2],10,{x:"-=700",ease:Linear.easeNone}));


            helper.GameHelper.cache(this.playAreaView.box1);
            helper.GameHelper.cache(this.playAreaView.box2);

            this.playAreaView.belt.belt1.cache(0,0, this.playAreaView.belt.belt1.getBounds().width,this.playAreaView.belt.belt1.getBounds().height);
            this.playAreaView.belt.belt2.cache(0,0, this.playAreaView.belt.belt2.getBounds().width,this.playAreaView.belt.belt2.getBounds().height);
            this.playAreaView.belt.belt3.cache(0,0, this.playAreaView.belt.belt3.getBounds().width,this.playAreaView.belt.belt3.getBounds().height);
            this.playAreaView.belt.belt4.cache(0,0, this.playAreaView.belt.belt4.getBounds().width,this.playAreaView.belt.belt4.getBounds().height);


        },

        onPlayClick:function(){
            this.titlePage.playBtn.removeEventListener('mousedown', this.onPlayClick.bind(this));

            TweenLite.to(this.titlePage.playBtn,0.15,{scaleX:"-=0.1",scaleY:"-=0.1",onComplete:this.removePlayBtn.bind(this)});

        },

        onReplayClick:function(e){
            this.replayBtn.removeEventListener('mousedown', this.onPlayClick.bind(this));

            TweenLite.to(this.replayBtn,0.15,{scaleX:"-=0.1",scaleY:"-=0.1",onComplete:this.removeReplayBtn.bind(this)});
            this.initMechanicView();

        },

        initMechanicView:function(){

            this.playAreaView.clickToClose.visible=false;

            AppConstants.STAGE.addEventListener(view.mediator.PlayAreaMediator.ON_CORRECT_ANS,this.onCorrectAns.bind(this));
            AppConstants.STAGE.addEventListener(view.mediator.PlayAreaMediator.ON_WRONG_ANS,this.onWrongAns.bind(this));
            AppConstants.STAGE.addEventListener(view.mediator.PlayAreaMediator.ON_QUESTION_COMPLETE,this.onQuestionComplete.bind(this));
            AppConstants.STAGE.addEventListener(view.mediator.PlayAreaMediator.START_SCORECARD,this.startScoreScreenView.bind(this));

            if(AppConstants.MECHANIC == 'mcq')
            {
                this.curMechanicView = new view.mechanic.McqMechanic(this.levelProxy,this.playAreaView);
            }
            else
            {
                this.curMechanicView = new view.mechanic.EquationMechanic(this.levelProxy,this.playAreaView);
            }

            this.onResume();

        },

        onPause:function(){
            TweenMax.pauseAll();

        },

        onResume:function(){
            TweenMax.resumeAll();
        },

        removePlayBtn:function(){
            if(this.titlePage.playBtn)
            {
                helper.GameHelper.remove(this.titlePage.playBtn);

            }

           TweenMax.to(this.titlePage,1,{y:"-500",ease:Back.easeOut,onComplete:this.removeTitlePage.bind(this)});

        },

        removeTitlePage:function(){
            if(this.titlePage)
            {
                helper.GameHelper.remove(this.titlePage);
                this.titlePage=null;

            }

            this.initMechanicView();


        },

        removeReplayBtn:function(){
            if(this.replayBtn)
            {
                helper.GameHelper.remove(this.replayBtn);
                this.replayBtn=null;

            }

        },

        onCorrectAns:function(){

        },

        onWrongAns:function(){

        },

        onQuestionComplete:function(){

        },

        startScoreScreenView: function (argument) {
            AppConstants.STAGE.removeEventListener(view.mediator.PlayAreaMediator.ON_CORRECT_ANS,this.onCorrectAns.bind(this));
            AppConstants.STAGE.removeEventListener(view.mediator.PlayAreaMediator.ON_WRONG_ANS,this.onWrongAns.bind(this));
            AppConstants.STAGE.removeEventListener(view.mediator.PlayAreaMediator.ON_QUESTION_COMPLETE,this.onQuestionComplete.bind(this));
            AppConstants.STAGE.removeEventListener(view.mediator.PlayAreaMediator.START_SCORECARD,this.startScoreScreenView.bind(this));

            //ApplicationFacade.getInstance().removeMediator( view.mediator.PlayAreaMediator.NAME );
            //ApplicationFacade.getInstance().registerMediator( new view.mediator.ScoreScreenMediator );

            if(this.curMechanicView)
            {
                this.curMechanicView = null;
            }

            if(AppConstants.TARGET == 'website'){
                this.playAreaView.clickToClose.visible=false;
            }
            else
            {
                this.playAreaView.clickToClose.visible=true;
            }

            //add replay btn
            if(!this.replayBtn && AppConstants.TARGET == 'website')
            {
                this.replayBtn = new lib.ReplayBtn();
                helper.GameHelper.setPos(this.replayBtn,512,334);
                this.viewComponent.addChild(this.replayBtn);
                this.replayBtn.addEventListener('mousedown', this.onReplayClick.bind(this));

                this.onPause();
            }
        },


        /** @override */
        onRemove: function() {
            this.viewComponent.removeAllEventListeners();
            helper.GameHelper.removeAll(this.viewComponent);
            this.setViewComponent(null);
            helper.GameHelper.removeAll();

        }

    },

    // STATIC MEMBERS
    {
        /**
         * @static
         * @type {string}
         */

        NAME: 'PlayAreaMediator',
        START_SCORECARD: 'START_SCORECARD',
        ON_CORRECT_ANS: 'ON_CORRECT_ANS',
        ON_WRONG_ANS: 'ON_WRONG_ANS',
        ON_QUESTION_COMPLETE: 'ON_QUESTION_COMPLETE',

        addExplosion: function(x, y, callback) {
            helper.GameHelper.playSound('PopSound');
            var explosion = new lib.BubbleBlast();
            explosion.x = x;
            explosion.y = y;
            AppConstants.STAGE_REF.addChild(explosion);
            setTimeout(helper.GameHelper.remove, 80, explosion);
            if (callback) {
                setTimeout(callback, 90);
            }

        }



    }
);
