// create the demo namespace
window.AppConstants= 
    {
	/**
		 * A notification that will trigger StartupCommand
		 */
		STARTUP: 'startup',


		////GAME CONSTANTS///////
		CANVAS: null,
		STAGE: null,
		STAGE_REF: null,
		CANVAS_WIDTH: null,
		CANVAS_HEIGHT: null,
		FPS: 60,
        MECHANIC: 'regular', // equation,mcq
        LEVEL: '1A', // equation-1A,mcq-1B,1C
        GRADE: '3',
        TARGET: 'website'


    };
