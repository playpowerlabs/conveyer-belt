
function initGame() {

    setResolution();
    AppConstants.CANVAS = document.getElementById("canvas");
	AppConstants.CONTEXT = AppConstants.CANVAS.getContext('2d');

    images = images||{};
    fonts = {};

    var manifest = lib.properties.manifest;
    manifest.push(
        {src:"lib/fonts/Dimbo_m.png", id:"DimboPNG"},
        {src:"lib/fonts/Dimbo_m.xml", id:"DimboXML"}


    );

    setConstants();

    loaderTxt = new createjs.Text("Loading...", "bold 50px Arial", "#ffffff");
    helper.GameHelper.setPivot(loaderTxt,loaderTxt.getBounds().width/2,loaderTxt.getBounds().height/2);
    AppConstants.STAGE_REF.addChild(loaderTxt);
    loaderTxt.x = 512;
    loaderTxt.y = 334;

    var loader = new createjs.LoadQueue(false);
	loader.setUseXHR(false);
	loader.installPlugin(createjs.Sound);
	loader.addEventListener("fileload", handleFileLoad);
	loader.addEventListener("complete", handleComplete);
    loader.addEventListener("progress",handleProgress);
	loader.loadManifest(manifest);



}

function make_base()
{
  base_image = new Image();
  base_image.src = 'splash.png';

  base_image.onload = function(){
    AppConstants.CONTEXT.drawImage(base_image,0,0,AppConstants.CANVAS_WIDTH, AppConstants.CANVAS_HEIGHT);
  }
}

function handleProgress(e){

    console.log(e.progress);
    loaderTxt.text = "Loading... \n\n     " + Math.floor(e.progress*100) +"%";
}

function setConstants(){

    AppConstants.STAGE_REF = new lib.assets();
    AppConstants.STAGE_REF.scaleY =(AppConstants.CANVAS_HEIGHT/768);
    AppConstants.STAGE_REF.scaleX =(AppConstants.CANVAS_WIDTH/1024);

    AppConstants.STAGE = new createjs.Stage(AppConstants.CANVAS);
    AppConstants.STAGE.addChild(AppConstants.STAGE_REF);
    AppConstants.STAGE.update();

    AppConstants.STAGE.snapToPixelsEnabled = false; //added later for bitmap font
    createjs.Touch.enable(AppConstants.STAGE);

    createjs.Ticker.setFPS(AppConstants.FPS);
    createjs.Ticker.useRAF = true;
    createjs.Ticker.addEventListener("tick", onTick);

    AppConstants.CONTEXT.clearRect(0, 0,AppConstants.CANVAS_WIDTH, AppConstants.CANVAS_HEIGHT);



}

function setResolution () {
	// body...

    var C = 1;        // CANVAS width to viewport width ratio
    var W_TO_H = 4/3;   // CANVAS width to CANVAS height ratio
    var el = document.getElementById("canvas");

    // For IE compatibility http://www.google.com/search?q=get+viewport+size+js
    var viewportWidth = window.innerWidth;
    var viewportHeight = window.innerHeight;

    AppConstants.CANVAS_HEIGHT = viewportHeight;
    AppConstants.CANVAS_WIDTH = (viewportHeight * W_TO_H);
    el.style.position = "fixed";
    el.setAttribute("width",  AppConstants.CANVAS_WIDTH);
    el.setAttribute("height",  AppConstants.CANVAS_HEIGHT);

    //el.style.top = Math.round((viewportHeight - AppConstants.CANVAS_HEIGHT) / 2);
    //el.style.left = Math.round((viewportWidth - CANVAS_WIDTH) / 2);
    el.style.marginLeft=Math.round((viewportWidth - AppConstants.CANVAS_WIDTH) / 2) -8+"px";



    addFocusEvents();
}

function addFocusEvents(){
    $( window ).on( "blur", function(e){
        console.log('on blur');
        onDeactivate();
    });

    $( window ).on( "focus", function(e){
        console.log('on focus');
        onActivate();
    });
}

function onDeactivate(){
    TweenMax.pauseAll();

}

function onActivate(){
    TweenMax.resumeAll();

}

function handleFileLoad(evt) {
	if (evt.item.type == "image")
	{
       	images[evt.item.id] = evt.result;
	}
    if(evt.item.type == "xml"){
        fonts[evt.item.id] = evt.result;
    }

}

function handleComplete() {

    AppConstants.STAGE_REF.removeChild(loaderTxt);
    AppConstants.CONTEXT.clearRect(0, 0,AppConstants.CANVAS_WIDTH, AppConstants.CANVAS_HEIGHT);
    //addFPSMeter();
    startMVC();


}

function addFPSMeter(){
    // add a text object to output the current FPS:
    fpsLabel = new createjs.Text("-- fps", "bold 14px Arial", "#ff0000");
    AppConstants.STAGE_REF.addChild(fpsLabel);
    fpsLabel.x = 10;
    fpsLabel.y = 10;

}

function startMVC () {

    //reg bitmap fonts
    var bitmapFont = new BitmapFont(images["DimboPNG"],fonts["DimboXML"],50);
    BitmapTextField.registerBitmapFont(bitmapFont,"Dimbo");

    // body...
	ApplicationFacade.getInstance().startup();

}



function onTick()
{
    window.ind = window.ind || 0;
    window.ind++;
    //console.log('tick: '+ window.ind);
    if(window.fpsLabel){
        fpsLabel.text = Math.round(createjs.Ticker.getMeasuredFPS()) + " fps";
        AppConstants.STAGE_REF.addChild(fpsLabel);
    }
	AppConstants.STAGE.update();

}





