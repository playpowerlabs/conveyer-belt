(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1024,
	height: 768,
	fps: 60,
	color: "#666666",
	manifest: [
		{src:"images/Bitmap1.png", id:"Bitmap1"},
		{src:"images/Bitmap1_1.png", id:"Bitmap1_1"},
		{src:"images/Bitmap1_2.png", id:"Bitmap1_2"},
		{src:"images/Bitmap1_3.png", id:"Bitmap1_3"},
		{src:"images/Bitmap1_4.png", id:"Bitmap1_4"},
		{src:"images/Bitmap1copy.png", id:"Bitmap1copy"},
		{src:"images/Bitmap1copy_1.png", id:"Bitmap1copy_1"},
		{src:"images/Bitmap1copy2.png", id:"Bitmap1copy2"},
		{src:"images/Bitmap1copy3.png", id:"Bitmap1copy3"},
		{src:"images/Bitmap1copy4.png", id:"Bitmap1copy4"},
		{src:"images/Bitmap10.png", id:"Bitmap10"},
		{src:"images/Bitmap11.png", id:"Bitmap11"},
		{src:"images/Bitmap15.png", id:"Bitmap15"},
		{src:"images/Bitmap16.png", id:"Bitmap16"},
		{src:"images/Bitmap17.png", id:"Bitmap17"},
		{src:"images/Bitmap18.png", id:"Bitmap18"},
		{src:"images/Bitmap2.png", id:"Bitmap2"},
		{src:"images/Bitmap2_1.png", id:"Bitmap2_1"},
		{src:"images/Bitmap2_2.png", id:"Bitmap2_2"},
		{src:"images/Bitmap2_3.png", id:"Bitmap2_3"},
		{src:"images/Bitmap2copy.png", id:"Bitmap2copy"},
		{src:"images/Bitmap2copy2.png", id:"Bitmap2copy2"},
		{src:"images/Bitmap2copy3.png", id:"Bitmap2copy3"},
		{src:"images/Bitmap20.png", id:"Bitmap20"},
		{src:"images/Bitmap21.png", id:"Bitmap21"},
		{src:"images/Bitmap22.png", id:"Bitmap22"},
		{src:"images/Bitmap23.png", id:"Bitmap23"},
		{src:"images/Bitmap24.png", id:"Bitmap24"},
		{src:"images/Bitmap25.png", id:"Bitmap25"},
		{src:"images/Bitmap26.png", id:"Bitmap26"},
		{src:"images/Bitmap28.png", id:"Bitmap28"},
		{src:"images/Bitmap3.png", id:"Bitmap3"},
		{src:"images/Bitmap3_1.png", id:"Bitmap3_1"},
		{src:"images/Bitmap3_2.png", id:"Bitmap3_2"},
		{src:"images/Bitmap3copy.png", id:"Bitmap3copy"},
		{src:"images/Bitmap3copy2.png", id:"Bitmap3copy2"},
		{src:"images/Bitmap3copy3.png", id:"Bitmap3copy3"},
		{src:"images/Bitmap30.png", id:"Bitmap30"},
		{src:"images/Bitmap31.png", id:"Bitmap31"},
		{src:"images/Bitmap4.png", id:"Bitmap4"},
		{src:"images/Bitmap5.png", id:"Bitmap5"},
		{src:"images/Bitmap5_1.png", id:"Bitmap5_1"},
		{src:"images/Bitmap5copy.png", id:"Bitmap5copy"},
		{src:"images/Bitmap6.png", id:"Bitmap6"},
		{src:"images/Bitmap6_1.png", id:"Bitmap6_1"},
		{src:"images/Bitmap6copy.png", id:"Bitmap6copy"},
		{src:"images/Bitmap7.png", id:"Bitmap7"},
		{src:"images/Bitmap8.png", id:"Bitmap8"},
		{src:"sounds/_1B_VO1.mp3", id:"_1B_VO1"},
		{src:"sounds/_1B_VO2.mp3", id:"_1B_VO2"},
		{src:"sounds/_1B_VO3.mp3", id:"_1B_VO3"},
		{src:"sounds/_1B_VO4.mp3", id:"_1B_VO4"},
		{src:"sounds/_1B_VO45.mp3", id:"_1B_VO45"},
		{src:"sounds/correct.mp3", id:"correct"},
		{src:"sounds/drop.mp3", id:"drop"},
		{src:"sounds/gloopy.mp3", id:"gloopy"},
		{src:"sounds/pull.mp3", id:"pull"},
		{src:"sounds/select.mp3", id:"select"},
		{src:"sounds/wipe.mp3", id:"wipe"},
		{src:"sounds/wrong.mp3", id:"wrong"}
	]
};



// symbols:



(lib.Bitmap1 = function() {
	this.initialize(img.Bitmap1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,290,108);


(lib.Bitmap1_1 = function() {
	this.initialize(img.Bitmap1_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,302,100);


(lib.Bitmap1_2 = function() {
	this.initialize(img.Bitmap1_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,461,574);


(lib.Bitmap1_3 = function() {
	this.initialize(img.Bitmap1_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,461,572);


(lib.Bitmap1_4 = function() {
	this.initialize(img.Bitmap1_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,122,131);


(lib.Bitmap1copy = function() {
	this.initialize(img.Bitmap1copy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,181,159);


(lib.Bitmap1copy_1 = function() {
	this.initialize(img.Bitmap1copy_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,411,51);


(lib.Bitmap1copy2 = function() {
	this.initialize(img.Bitmap1copy2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,136,55);


(lib.Bitmap1copy3 = function() {
	this.initialize(img.Bitmap1copy3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,418,126);


(lib.Bitmap1copy4 = function() {
	this.initialize(img.Bitmap1copy4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,681,502);


(lib.Bitmap10 = function() {
	this.initialize(img.Bitmap10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,157,157);


(lib.Bitmap11 = function() {
	this.initialize(img.Bitmap11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2139,98);


(lib.Bitmap15 = function() {
	this.initialize(img.Bitmap15);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,101,542);


(lib.Bitmap16 = function() {
	this.initialize(img.Bitmap16);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1169,10);


(lib.Bitmap17 = function() {
	this.initialize(img.Bitmap17);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1059,768);


(lib.Bitmap18 = function() {
	this.initialize(img.Bitmap18);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,60,60);


(lib.Bitmap2 = function() {
	this.initialize(img.Bitmap2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,681,501);


(lib.Bitmap2_1 = function() {
	this.initialize(img.Bitmap2_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,681,501);


(lib.Bitmap2_2 = function() {
	this.initialize(img.Bitmap2_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,147,195);


(lib.Bitmap2_3 = function() {
	this.initialize(img.Bitmap2_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,179,156);


(lib.Bitmap2copy = function() {
	this.initialize(img.Bitmap2copy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,134,40);


(lib.Bitmap2copy2 = function() {
	this.initialize(img.Bitmap2copy2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,418,44);


(lib.Bitmap2copy3 = function() {
	this.initialize(img.Bitmap2copy3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,376,89);


(lib.Bitmap20 = function() {
	this.initialize(img.Bitmap20);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,215,235);


(lib.Bitmap21 = function() {
	this.initialize(img.Bitmap21);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,141,564);


(lib.Bitmap22 = function() {
	this.initialize(img.Bitmap22);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,109);


(lib.Bitmap23 = function() {
	this.initialize(img.Bitmap23);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,311,405);


(lib.Bitmap24 = function() {
	this.initialize(img.Bitmap24);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,311,405);


(lib.Bitmap25 = function() {
	this.initialize(img.Bitmap25);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,140,428);


(lib.Bitmap26 = function() {
	this.initialize(img.Bitmap26);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,140,428);


(lib.Bitmap28 = function() {
	this.initialize(img.Bitmap28);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,319,122);


(lib.Bitmap3 = function() {
	this.initialize(img.Bitmap3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,155,119);


(lib.Bitmap3_1 = function() {
	this.initialize(img.Bitmap3_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,312,145);


(lib.Bitmap3_2 = function() {
	this.initialize(img.Bitmap3_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,181,159);


(lib.Bitmap3copy = function() {
	this.initialize(img.Bitmap3copy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,136,55);


(lib.Bitmap3copy2 = function() {
	this.initialize(img.Bitmap3copy2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,134,40);


(lib.Bitmap3copy3 = function() {
	this.initialize(img.Bitmap3copy3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,416,150);


(lib.Bitmap30 = function() {
	this.initialize(img.Bitmap30);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,107,24);


(lib.Bitmap31 = function() {
	this.initialize(img.Bitmap31);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,107,24);


(lib.Bitmap4 = function() {
	this.initialize(img.Bitmap4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,466,181);


(lib.Bitmap5 = function() {
	this.initialize(img.Bitmap5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1057,282);


(lib.Bitmap5_1 = function() {
	this.initialize(img.Bitmap5_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,311,121);


(lib.Bitmap5copy = function() {
	this.initialize(img.Bitmap5copy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,466,181);


(lib.Bitmap6 = function() {
	this.initialize(img.Bitmap6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,311,227);


(lib.Bitmap6_1 = function() {
	this.initialize(img.Bitmap6_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,783,363);


(lib.Bitmap6copy = function() {
	this.initialize(img.Bitmap6copy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,466,181);


(lib.Bitmap7 = function() {
	this.initialize(img.Bitmap7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,466,181);


(lib.Bitmap8 = function() {
	this.initialize(img.Bitmap8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,466,181);


(lib.TitleMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap2_1();

	this.instance_1 = new lib.Bitmap1copy4();
	this.instance_1.setTransform(0,-1);

	this.instance_2 = new lib.Bitmap2();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,681,501);


(lib.Tile = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap3();
	this.instance.setTransform(-82,-59.1,1.226,1);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-82,-59.1,190,119);


(lib.SplitTile = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap22();
	this.instance.setTransform(-59,-56.3);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-59,-56.3,117,109);


(lib.Robot2Tile = function() {
	this.initialize();

	// Layer 3
	this.instance = new lib.Bitmap2_3();
	this.instance.setTransform(-89.2,-117.1);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-89.2,-117.1,179,156);


(lib.Robot1Tile = function() {
	this.initialize();

	// Layer 4
	this.instance = new lib.Bitmap3_2();
	this.instance.setTransform(-89.3,-118.4);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-89.3,-118.4,181,159);


(lib.Robot = function() {
	this.initialize();

	// Layer 6
	this.instance = new lib.Bitmap21();
	this.instance.setTransform(-70,-566.1);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-70,-566.1,141,564);


(lib.InfoMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4
	this.instance = new lib.Bitmap8();
	this.instance.setTransform(-233.1,-121.9);

	this.instance_1 = new lib.Bitmap7();
	this.instance_1.setTransform(-233.1,-121.9);

	this.instance_2 = new lib.Bitmap6copy();
	this.instance_2.setTransform(-233.1,-121.9);

	this.instance_3 = new lib.Bitmap5copy();
	this.instance_3.setTransform(-233.1,-121.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-233.1,-121.9,466,181);


(lib.Hand = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap2_2();
	this.instance.setTransform(0,0,0.6,0.6);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,88.2,117);


(lib.Clamp = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap15();
	this.instance.setTransform(-51.1,-541.2);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-51.1,-541.2,101,542);


(lib.Bubble = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFCF2E","rgba(255,255,255,0)"],[0,0.78],0.7,-34.8,0,0.7,-34.8,55.1).s().de(-31.6,-31.6,63.3,63.3);
	this.shape.setTransform(-0.1,0.4,0.454,0.454);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#26CB01").s().de(-31.6,-31.6,63.3,63.3);
	this.shape_1.setTransform(-0.1,0.4,0.5,0.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-15.9,-15.4,31.7,31.7);


(lib.BoxTile = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap22();
	this.instance.setTransform(-91,-55.3,1.607,1);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-91,-55.3,188,109);


(lib.Box2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap18();
	this.instance.setTransform(-30,-30);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-30,-30,60,60);


(lib.Box = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap20();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,215,235);


(lib.Tween29 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#26CB01").s().p("A4T8vMAwmgBCMAAAA5KMgwmACZg");
	this.shape.setTransform(-0.6,1.7,1,1,0,0,0,0,187.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-156.2,-376.8,311.2,381.4);


(lib.Tween27 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap28();
	this.instance.setTransform(-153.1,-58.1,0.96,0.96);

	this.instance_1 = new lib.Bitmap28();
	this.instance_1.setTransform(-148,-58.6,0.96,0.96);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-153.1,-58.6,311.4,117.7);


(lib.scrapMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Bitmap2copy();

	this.instance_1 = new lib.Bitmap3copy2();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},4).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,134,40);


(lib.replaybutton = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#151514").ss(2.5,1,1).p("Aigi/QhQBRAABxQAAAfAFAbQABACAAADIhNAZIC+BuIBsi+IhTAbQAAgUAAgPQAAg1AngnQAngnA1AAQA3AAAnAnQAmAnAAA1QAAA3gmAnQgdAcglAIIAkCMQBLgRA4g5QBShRAAhzQAAhxhShRQhQhRhzAAQhxAAhSBRg");
	this.shape.setTransform(-7.9,2.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5E2C0").s().p("AA/CFQAlgIAdgcQAmgnABg3QgBg1gmgnQgngmg3AAQg1AAgnAmQgnAnAAA1IAAAjIBTgcIhrC+Ii/huIBNgZIgBgEQgFgcAAgeQAAhxBQhRQBShRBxAAQBzAABRBRQBRBRAABxQAABzhRBQQg5A6hLARg");
	this.shape_1.setTransform(-7.9,2.4);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-40.3,-26.2,64.9,57.1);


(lib.Okbutton = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.Bitmap1copy2();
	this.instance.setTransform(-69,-27.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-69,-27.5,136,55);


(lib.gameBG = function() {
	this.initialize();

	// Layer 5
	this.instance = new lib.Bitmap17();
	this.instance.setTransform(-24.3,0);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-24.3,0,1059,768);


(lib.Frontwall = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap11();
	this.instance.setTransform(-3,-3);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3,-3,2139,98);


(lib.bsnl_BN_3 = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgDAbIgBgBIgGABQgDAAgEgCQgDgDAAgEIABgHQABgDACgCQgFgEAAgGQAAAAAAgBQAAgBAAAAQABgBAAAAQABAAAAgBIABgCQAAAAAAgBQgBgBAAAAQAAgBAAAAQgBgBAAAAQABgEAEgEQADgEADAAQAJAAAAAGIACgHQAAAAABAAQAAgBABAAQAAAAABAAQAAAAABAAQAEAAADADQAAABAAAAQABABAAABQAAAAAAABQABAAAAABIAAABQAFABABAIQAAACgDACQACACAAAEQAAABgCACQAEAFAAAFQAAAHgEADQgDADgEAAIgFgBIAAgBQAAABAAABQAAAAgBABQAAAAAAABQgBAAAAAAIgEABQAAAAAAAAQgBAAgBAAQAAgBAAAAQgBAAAAgBg");
	this.shape.setTransform(-0.1,0.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2.4,-2.6,4.5,6);


(lib.bsnl_BN_2 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AADA0IgDgHQgCAEgGAAQgKAAAAgNQAAgBAAAAQAAgBAAgBQABAAAAgBQAAAAAAAAIADgCIgOgFQgIgFAAgMQAAgIAEgDQAFgFAFAAIAEABIABAAIAAAAIgBAAQgFgEAAgFQAAgJAIgCIgEgEQgEgDAAgDQAAgGADgEQADgEADAAIAFgBIAFABIACgBIADgCQAGAAADAIQADAGgBADQAEABAAAIIAHgBQAEAAAFAEQAFAFAAAEQAAAHgFAGQgFADgDAAIgBAAQADABAAAEQAAAGgGACIACACIABADIgEAFQADAAADAEQAEAFAAAFQAAAIgIAGQgGAEgEAAQgFAAgDgDg");
	this.shape.setTransform(-1.3,-0.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-5,-5.8,7.6,11.1);


(lib.bsnl_BN_1 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRCLQgggMAAgWQAAgaAFgHQgqgGAAgeQAAgKAFgTQAGgSAEAAIABAAIAAAAQgFgIAAgGQAAgGAEgGIAEgEQgLgDgEgCQgEgDAAgPQAAgcAOgMQAJgJALAAQAAgFAFgFQAGgIALAAIARADQAHgOACgBQADgDAJAAQAXAAASARQASARgGASIgHASIAOARQANAQAAAGIgCAIQAIAJAAALQAAAUgFAJQgGANgUAGQAPAQAAAMQAAAOgHAKQgHAMgNAAQAFAJgIARQgJASgRAAQgOAAgSgIg");
	this.shape.setTransform(-0.5,2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-9.2,-12.8,17.6,29.5);


(lib.Reset = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/////////* stop();
		////////*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ABLhOQg+hYikgEQDCg0BPCJIAEAHQBBCWhgByQAriwg/hYg");
	this.shape.setTransform(104.9,28.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AiXCrQCkgEA+hYQA/hYgriwQBgByhBCWIgEAHQg6Bkh0AAQgtAAg2gPg");
	this.shape_1.setTransform(105.4,282.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(1));

	// Layer 6
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DED1BD").s().p("AisCtQhIhIAAhlQAAhkBIhIQBIhIBkAAQBlAABIBIQBIBIAABkQAABlhIBIQhIBIhlAAQhkAAhIhIg");
	this.shape_2.setTransform(98.2,32.1);

	this.timeline.addTween(cjs.Tween.get(this.shape_2).wait(1).to({skewX:180,x:98.7,y:279},0).wait(1));

	// Layer 7
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#514B41").s().p("AkZEGIF9uKIC2A6IofTPQAIi7gcjEg");
	this.shape_3.setTransform(75.6,102.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#514B41").s().p("AkZkBQAcjEgNjDIIkTXIi2A6g");
	this.shape_4.setTransform(76.1,208.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3}]}).to({state:[{t:this.shape_4}]},1).wait(1));

	// Layer 1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#74695E").s().p("AnGM6QCvjKAopLQAopKj/kUIKdAAQEAFugSHRQgUIdjkEXgAiFKuIDkAAQFuprlasiIkRAAQFVLMk8LBg");
	this.shape_5.setTransform(48,155.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#151311").s().p("Alws5QD+EUgoJKQgoJLiuDKgAgwKuQE9rBlWrMIESAAQFaMiluJrg");
	this.shape_6.setTransform(39.5,155.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5}]}).to({state:[{t:this.shape_6},{t:this.shape_5}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(2.5,7.5,120.2,230.4);


(lib.Inefficientcopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.Bitmap31();
	this.instance.setTransform(1,0);

	this.instance_1 = new lib.Bitmap30();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},4).wait(4));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1,0,107,24);


(lib.asdasascopy2 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","rgba(0,0,0,0)"],[0,0.78],3.2,-50.1,0,3.2,-50.1,57.7).s("rgba(0,0,0,0.8)").ss(1,1,1).de(-31.6,-31.6,63.3,63.3);
	this.shape.setTransform(31.6,31.7,0.904,0.904);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s("rgba(0,0,0,0.8)").ss(1,1,1).de(-31.6,-31.6,63.3,63.3);
	this.shape_1.setTransform(31.6,31.7);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,65.3,65.3);


(lib.ClickToClose = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap1_4();
	this.instance.setTransform(-3,-1);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3,-1,122,131);


(lib.asdX = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#151514").ss(12.5,1,1).p("APckHI+ksTMgATAg1Iet0KIAFgMIAFgMg");
	this.shape.setTransform(98.9,105.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#D5E2C0").s().p("AvIwaIekMTIgFAMIgFAMI+tUKg");
	this.shape_1.setTransform(98.9,105.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#6B7162").s().p("AACgEIAAAAIgDAJg");
	this.shape_2.setTransform(197.5,79.3);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-6.2,-6.2,210.2,222.7);


(lib.dgaf = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbAdQgLgMAAgRQAAgQALgMQAMgMAPAAQAQAAAMAMQALAMAAAQQAAARgLAMQgMAMgQgBQgPABgMgMg");
	this.shape.setTransform(27.1,20.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#858585").s().p("AhahAIAAgyQCmBEAPChQgviViGgeg");
	this.shape_1.setTransform(35.3,14.6);

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3F3F3F").s().p("AioCpQhGhGgBhjQABhiBGhGQBGhGBigBQBjABBGBGQBGBGAABiQAABjhGBGQhGBGhjAAQhiAAhGhGgAhJhJQgeAfgBAqQABArAeAfQAfAeAqAAQArAAAfgeQAfgfgBgrQABgqgfgfQgfgegrgBQgqABgfAeg");
	this.shape_2.setTransform(24,24);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B1B1B1").s().p("AhJBKQgegfgBgrQABgqAegfQAfgeAqgBQArABAfAeQAfAfgBAqQABArgfAfQgfAegrAAQgqAAgfgeg");
	this.shape_3.setTransform(24,24);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,48,48);


(lib.chute = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap1copy_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,411,51);


(lib.cAGEDLIGHTS = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap25();
	this.instance.setTransform(-11,-9.9);

	this.instance_1 = new lib.Bitmap26();
	this.instance_1.setTransform(-11,-9.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11,-9.9,140,428);


(lib.submarineoriginal = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(0,0,0,0.149)").s().p("AKjKQQgFgKgBgRQAAghARgSQAQgQAWABQAWABARARQARASAAAdQAAAggKAJQgKAKgeAAQgqAAgNgXgAEzKQQgFgKAAgRQAAghARgSQAPgQAXABQAWABAQARQARASAAAdQAAAggJAJQgKAKgfAAQgqAAgNgXgAhkKQQgGgKAAgRQABghARgSQAPgQAWABQAXABAPARQAQASgBAdQAAAggHAJQgKAKgeAAQgqAAgNgXgAoJKQQgFgKAAgRQAAghARgSQAPgQAXABQAWABAQARQARASAAAdQAAAggKAJQgJAKgfAAQgqAAgNgXgAuFKQQgFgKAAgRQAAghARgSQAQgQAWABQAWABAQARQARASAAAdQAAAggKAJQgJAKgfAAQgqAAgNgXgAPSGQIgDgCIgCgEIAAgDIgaAAI7jgEQhsgCgYgCQgGAGgFAAIgLgBIgDgBQgDgDAAgMQAAgZAig3QAig4Awg1QB6iIBDAZQAxgCBlgBIWqgBQAigNBMgTQBFgRBPgQQATgDAkgTQAQgJB3grQgHAJAAANQAAAMhfBeQhwBvgKAOQgOAPhMBpIgzBFQgBAIgGAFIgDABIgMAQIgMgBgAgMEZQg2A7AAAYQAAARABABIAGAFIANAAQAGgQA5hJQAsg5gJgbQgxAwgPATgApWEmQgwAzAAAPQAAARADADQACACAKACQBjhjArg/Qg0ANg5A7gAMQEGQgiA4gNAnIAKAFQAHgPAvhKQAkg3ACgaQgSAHglA/gEghDAC8QgCgDgBgHIAAgEQgSAAgCgCIgCgIQCRgVBPgRQA1gMCEgjQA/gPAagRQAHgFAxgsQAbgaAgg9QAmhLAMgPIChgMQAbgFAOACIACAEQACABABAHQABAJgHAFQgEAFgKADIAEAnQADA4hCBeQhLBshTAMIgIAAIgCACQgFAHgJABIhUAFIgVAFIikALQgFgBgoAAQghAAgJgBIh3AGQheAEgNACgAfMgTQgEgSABgYIADgZQAAhrAWgcQATgYAfAjQAbAgAXA8QAWA9ABAuQgBAZgMAKQgPANgtAAQg8AAgMg4gAfOnTIAAhCQAAg8ARgpQAPgmAWgGQAWgGAOAhQARAlABBJQgBCJg0AAQguAAgJg/g");
	this.shape.setTransform(28.3,21.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,255,255,0.149)").s().p("AtNQHQhbABgagGQgQgEAAgMQAAgJAMgEQAcgKBdACIb8gDQAMAGAHAJQAGAGAAACQAAAMgZANgAg6PCIgGgEQgBgCAAgQQAAgZA2g7QAPgTAxgvQAIAagsA5Qg4BKgGAPgAqCO7QgDgDAAgQQAAgQAwgyQA4g8A1gNQgrA/hjBkQgKgCgCgDgALiOkQANgmAhg5QAlg/ATgHQgDAagjA4QgvBJgHAPgAfoLgIAAghQAAhIAqAUQAQAHANAQQAOAPAAAJQAAAigKAPQgLASgYAAQghAAgHgdgANzGwQgNg2gwiRQgqh+gPhVQA5APArCEQAmBzAABgQAAARgEANQgCAJgHANgA7vFXQjOgEhCgCQgzgCgJgEIgBgIQAHgDAAgFQgBgDAPgBIE4gFIGzgBQAXAFAAAOQAAATgXACgAnNCaQghhlgagmQA5AFAaBgQAOA2APBrQgWgbgfhggABaCEQgohXAAgMQAAgBAOgEQAOgHAAgTQAVAxAzCfIgKAWQgLgOgnhWgAXzDKQjTgHj0gjIiggSQgSgCgFgEQgFgDAAgLQAAgcCOgHQB9gHC9ALQC3ALCIAUQCVAWALAXIgIAWQgGANgXAAIj/AAgA5KClQirgDgugLQgUgFgFgJQgDgEAAgNQAAgOARgPQAOgNAOgGIOsAAQCGADAoALQAWAGATAZQgGAFgFAMQgDAKgLAFQggAOieAEgAAICbIkWAAQg+AEgXgSQgPgMAAgWQAAgVATgKQAXgMA6ABIEWAAQAcgFARAXQAMAQAAALQAAAKgMAQQgOAUgWAAIgJgBgALOCRIk2AAQhuACg7gHQgJAFgMAAQggAAgKgbQgGgIAAgNQAAgHADgFQAFgpAoAAQASAAANANIACACQA4gFBlABIE2AAQAfgFATAXQAMAQAAALQAAALgMAPQgQAUgYAAIgKgBgAfei1IAAglQAAglANgZQALgXARgDQARgEALATQANAVAAAoQAAAngCAKQgHAegcAAQgmAAgHgegAsqioQhagDg/AAQAAgBgFgFQgFgGAAgOQAAgTAXgGQAfgHBtABIPmAAQC/ADALAEQACABAAAHQABAGAUAGQgPAFADAHQADAHgCABQgPAIjHAFgAjLmGIgBAQQABACgHAAQgFAAgDgiIAAgIIgDgpIAAgLIgChKIAAirQgBhZABgrIgHhIIAAgSIolgCIgOgBIgGABQgGAAgCgFIgCgCIACgDIAAAAQAAgNAFgfQAFgeAAgNIADAAIAAgBQAFgCB7AOICCAQICZAPICtARIAAgBIACACQADAAACADQACgHAEAAQANACABCDQAABFgCBpIAACXQgCCUgKAAQgEAAgCgUg");
	this.shape_1.setTransform(28.2,-36);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-185.8,-139.5,428.2,229);


(lib.shape53 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","#FFFFFF","#DEDEDE","rgba(193,193,193,0)"],[0.204,0.325,0.498,0.984],0,0,0,0,0,131.9).s().p("AubOcQl/l/AAodQAAocF/l/QF/l/IcAAQIdAAF/F/QF/F/AAIcQAAIdl/F/Ql/F/odAAQocAAl/l/g");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-130.7,-130.7,261.5,261.5);


(lib.belt = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap16();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1169,10);


(lib.ATD = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.Bitmap10();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,157,157);


(lib.adsad = function() {
	this.initialize();

	// Layer 3
	this.instance = new lib.Bitmap6_1();
	this.instance.setTransform(-0.2,0);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.2,0,783,363);


(lib.Shutter = function() {
	this.initialize();

	// Layer 2
	this.shutter1 = new lib.adsad();
	this.shutter1.setTransform(-379.7,470.2,0.483,0.791,0,0,180,778,184.2);

	this.shutter2 = new lib.adsad();
	this.shutter2.setTransform(374.4,468.2,0.484,0.791,0,0,0,782,181.6);

	this.addChild(this.shutter2,this.shutter1);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-382,324.6,756.9,287.1);


(lib.ObjectiveBox = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.goBtn = new lib.Okbutton();
	this.goBtn.setTransform(0,514.6,1,1,0,0,0,-0.7,-0.9);

	this.timeline.addTween(cjs.Tween.get(this.goBtn).wait(3));

	// Layer 3
	this.instance = new lib.Bitmap1_1();
	this.instance.setTransform(-144,380.1);

	this.instance_1 = new lib.Bitmap1_2();
	this.instance_1.setTransform(-229.8,0);

	this.instance_2 = new lib.Bitmap2copy3();
	this.instance_2.setTransform(-185.1,376.5);

	this.instance_3 = new lib.Bitmap1();
	this.instance_3.setTransform(-141,382.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).to({state:[{t:this.instance_1},{t:this.instance_2}]},1).to({state:[{t:this.instance_1},{t:this.instance_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-229.8,0,461,574);


(lib.AppearAnim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_6 = function() {
		/////* stop();*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(6).call(this.frame_6).wait(2));

	// Layer 3
	this.instance = new lib.shape53("synched",0);
	this.instance.setTransform(0.5,0.3,0.432,0.432);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.61,scaleY:0.61},1).to({scaleX:0.86,scaleY:0.86},1).to({scaleX:1.12,scaleY:1.12},1).wait(1).to({scaleX:0.54,scaleY:0.54},0).wait(1).to({scaleX:0.1,scaleY:0.1},0).to({_off:true},1).wait(2));

	// Layer 1
	this.instance_1 = new lib.submarineoriginal();
	this.instance_1.setTransform(-5.7,2.4,0.07,0.064,0,0,0,-0.7,6.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).wait(3).to({_off:true},1).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-56,-56.2,113,113);


(lib.sdA = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.ATD();
	this.instance.setTransform(78.4,78.4,1,1,180,0,0,78.4,78.4);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.2,-0.2,157,157);


(lib.hgsad = function() {
	this.initialize();

	// Layer 4
	this.instance = new lib.Bitmap24();
	this.instance.setTransform(0,-177.7);

	// Layer 9
	this.bubbles = new lib.Tween27();
	this.bubbles.setTransform(158.7,146.7,1,1,0,0,0,5.9,-1.9);

	// Layer 5
	this.bg = new lib.Tween29();
	this.bg.setTransform(161.4,213.3,0.98,0.08,0,-1.7,-3.5);

	// moving bubbles
	this.bubble3 = new lib.Bubble();
	this.bubble3.setTransform(183.9,122.3,0.7,0.7,0,0,0,-0.1,0.5);

	this.bubble2 = new lib.Bubble();
	this.bubble2.setTransform(246.5,122.3,1.2,1.2,0,0,0,-0.1,0.4);

	this.bubble1 = new lib.Bubble();
	this.bubble1.setTransform(84.1,140.8,1,1,0,0,0,-0.1,0.4);

	// Layer 3
	this.instance_1 = new lib.Bitmap23();
	this.instance_1.setTransform(0,-177.7);

	this.addChild(this.instance_1,this.bubble1,this.bubble2,this.bubble3,this.bg,this.bubbles,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.4,-177.7,313.3,405);


(lib.asdvcopy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.asdasascopy2();
	this.instance.setTransform(28.7,28.7,0.906,0.906,0,0,0,31.7,31.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:24.7},9).to({y:28.7},10).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.4,-0.4,58.3,58.3);


(lib.asdXcopy = function() {
	this.initialize();

	// Layer 1
	this.replayBtn = new lib.replaybutton();
	this.replayBtn.setTransform(93.7,79,4.112,4.112,-15);

	this.addChild(this.replayBtn);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-94.5,-51.2,318.6,295.9);


(lib.Circle = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.dgaf();
	this.instance.setTransform(1,-1,0.792,0.792,179.8,0,0,24,24);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-18.1,-20.1,38.2,38.2);


(lib.bsnl_BN_blast = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_10 = function() {
		/////////*js
		////////
		////////stop();
		////////*/
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(10).call(this.frame_10).wait(1));

	// Layer 4
	this.instance = new lib.bsnl_BN_3("synched",0);
	this.instance.setTransform(2.9,0.1,0.5,0.5);
	this.instance.alpha = 0.66;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:48,x:4.5,y:2.1,alpha:0.77},2).to({scaleX:0.77,scaleY:0.77,x:6.5,y:1.9,alpha:0.449},3).to({scaleX:1.12,scaleY:1.12,x:7.3,y:2.6,alpha:0.16},3).to({scaleX:1.44,scaleY:1.44,x:7.7,y:3.6,alpha:0},2).wait(1));

	// Layer 3
	this.instance_1 = new lib.bsnl_BN_2("synched",0);
	this.instance_1.setTransform(2.9,0.1,0.5,0.5);
	this.instance_1.alpha = 0.859;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({scaleX:0.55,scaleY:0.55,rotation:-40.9,x:5,y:-1.3,alpha:0.781},2).to({scaleX:0.75,scaleY:0.75,rotation:-55.9,x:5.5,y:-2.8,alpha:0.52},3).to({scaleX:0.97,scaleY:0.97,x:6.8,y:-4.1,alpha:0.191},3).to({scaleX:1.25,scaleY:1.25,x:7.1,y:-4.9,alpha:0},2).wait(1));

	// Layer 1
	this.instance_2 = new lib.bsnl_BN_1("synched",0);
	this.instance_2.setTransform(2.9,0.1,0.196,0.196);
	this.instance_2.alpha = 0.359;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({scaleX:0.22,scaleY:0.22,x:3.4,y:1.3,alpha:0.781},2).to({scaleX:0.26,scaleY:0.26,x:2.7,y:-0.3,alpha:0.5},3).to({scaleX:0.33,scaleY:0.33,x:3.9,y:-0.6,alpha:0.211},3).to({scaleX:0.42,scaleY:0.42,x:3.3,y:-0.4,alpha:0},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0.4,-2.8,4.1,6.2);


(lib.asda = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#100E0D").s().p("EAjjAdeIAAAAMgH6g45MhBSAC6IgGiPMBEygCyMAGtA61IiMAPg");
	this.shape.setTransform(320.8,233.9);

	// Layer 5
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7D7D7D").s().p("EgvRAg4IAAAAIAAgBMAGKhBnMBRKgDvMAHPBI/gEAoJAdkIgBgPIABAAMgGng61MhEAACyIAAADIhoAFMgE6A1dMBO+ACpIABAEIAggDIBqADg");
	this.shape_1.setTransform(302.7,233.7);

	// Layer 2
	this.instance = new lib.asdX();
	this.instance.setTransform(311.5,248.8,1,1,0,0,0,98.9,105.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("EgriAA1IjvlkMBejADpIkdF2g");
	this.shape_2.setTransform(302.7,474.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#171513").s().p("Ag7A+IAMh9IBngEIAEB2IgxADIgBAPg");
	this.shape_3.setTransform(76.3,69.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#100E0D").s().p("Ai5ZtMAEsgzeIBHAGMgDRAzdg");
	this.shape_4.setTransform(58.7,241.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#2C2824").s().p("EgnXAaNIACgWMADTgzdIABgOIAxgDMBCegC5MAIJA5gg");
	this.shape_5.setTransform(308.1,240.6);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.instance,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,605.3,504.9);


(lib.TitlePage = function() {
	this.initialize();

	// Layer 2
	this.playBtn = new lib.asda();
	this.playBtn.setTransform(503.8,535,0.341,0.341,0,0,0,302.9,233.6);

	// Layer 1
	this.TitleMC = new lib.TitleMC();
	this.TitleMC.setTransform(520.2,248.4,1,1,0,0,0,340.5,250.5);

	this.addChild(this.TitleMC,this.playBtn);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(179.7,-2.1,681,629.5);


(lib.Smoke = function() {
	this.initialize();

	// bsnl_BN_blast
	this.instance = new lib.bsnl_BN_blast();
	this.instance.setTransform(-21.6,44.4,16.424,15.188,-84.3,0,0,-1.1,1.7);

	this.instance_1 = new lib.bsnl_BN_blast();
	this.instance_1.setTransform(49.5,98.8,16.423,15.188,-94.3,0,0,-1.1,1.7);

	this.instance_2 = new lib.bsnl_BN_blast();
	this.instance_2.setTransform(-21.6,88.7,16.424,15.188,-84.3,0,0,-1.1,1.7);

	this.instance_3 = new lib.bsnl_BN_blast();
	this.instance_3.setTransform(54.6,62.6,16.424,15.188,-84.3,0,0,-1.1,1.7);

	this.instance_4 = new lib.bsnl_BN_blast();
	this.instance_4.setTransform(37.1,2.1,11.963,11.064,149,0,0,-0.8,1.9);

	this.instance_5 = new lib.bsnl_BN_blast();
	this.instance_5.setTransform(-64.5,40.1,11.963,11.064,19.3,0,0,-0.8,1.9);

	this.instance_6 = new lib.bsnl_BN_blast();
	this.instance_6.setTransform(-29.2,50.1,11.963,11.064,-90,0,0,-1.1,1.7);

	this.instance_7 = new lib.bsnl_BN_blast();
	this.instance_7.setTransform(9.3,39.9,16.424,15.188,-84.3,0,0,-1.1,1.7);

	// bsnl_BN_blast
	this.instance_8 = new lib.bsnl_BN_blast();
	this.instance_8.setTransform(-3.9,-36.8,11.963,11.064,149,0,0,-0.8,1.9);

	// bsnl_BN_blast
	this.instance_9 = new lib.bsnl_BN_blast();
	this.instance_9.setTransform(-33.7,4.4,11.963,11.064,19.3,0,0,-0.8,1.9);

	// bsnl_BN_blast
	this.instance_10 = new lib.bsnl_BN_blast();
	this.instance_10.setTransform(9.1,40.3,11.963,11.064,-90,0,0,-1.1,1.7);

	this.addChild(this.instance_10,this.instance_9,this.instance_8,this.instance_7,this.instance_6,this.instance_5,this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-87.6,-58.3,176.6,138.1);


(lib.ReplayBtn = function() {
	this.initialize();

	// Layer 3
	this.instance = new lib.asdXcopy();
	this.instance.setTransform(0,-8.8,0.5,0.5,0,7.3,-172.7,85.5,87.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2C2824").s().p("Ay3M9IBn5tMAgOgBcID6cZg");
	this.shape.setTransform(-0.8,-9.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("A1wAaIh3ixMAvQAB0IiPC6g");
	this.shape_1.setTransform(-1.4,107.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#100E0D").s().p("APStrMggOABdIhmZsIhvgEICd6tMAiygBbIDUdVIhGAIg");
	this.shape_2.setTransform(-2.8,-13);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F6FE50").s().p("AgiACIBEgGIABAIg");
	this.shape_3.setTransform(123.6,81.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#7D7D7D").s().p("A3nQcIgBAAIAAgBMADGggyMAojgB4MADoAkfgAyCtSIidatIBuAEMAlvABRIBHACIgBgKIjU9Vg");
	this.shape_4.setTransform(-1.4,-13.1);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-152.7,-129.9,302.7,252.5);


(lib.tires = function() {
	this.initialize();

	// sdA
	this.tier1 = new lib.sdA();
	this.tier1.setTransform(46.6,47,0.5,0.5,0,0,0,78.4,78.4);
	this.tier1.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,10);

	// sdA
	this.tier2 = new lib.sdA();
	this.tier2.setTransform(329,47,0.5,0.5,0,0,0,78.4,78.4);
	this.tier2.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,10);

	// sdA
	this.tier3 = new lib.sdA();
	this.tier3.setTransform(601.4,47,0.5,0.5,0,0,0,78.4,78.4);
	this.tier3.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,10);

	// sdA
	this.tier4 = new lib.sdA();
	this.tier4.setTransform(858.8,47,0.5,0.5,0,0,0,78.4,78.4);
	this.tier4.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,10);

	this.addChild(this.tier4,this.tier3,this.tier2,this.tier1);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(1.3,1.7,906.2,95);


(lib.SVS = function() {
	this.initialize();

	// Circle
	this.w12 = new lib.Circle();
	this.w12.setTransform(66.1,-10.5);

	this.w11 = new lib.Circle();
	this.w11.setTransform(231.1,-10.5);

	this.w10 = new lib.Circle();
	this.w10.setTransform(411.1,-11);

	this.w9 = new lib.Circle();
	this.w9.setTransform(591.2,-10.5);

	this.w8 = new lib.Circle();
	this.w8.setTransform(767.2,-10.5);

	this.w7 = new lib.Circle();
	this.w7.setTransform(939.3,-10.5);

	this.w6 = new lib.Circle();
	this.w6.setTransform(1115.3,-10.5);

	this.w5 = new lib.Circle();
	this.w5.setTransform(1291.3,-10.5);

	this.w4 = new lib.Circle();
	this.w4.setTransform(1467.4,-11);

	this.w3 = new lib.Circle();
	this.w3.setTransform(1647.4,-10.5);

	this.w2 = new lib.Circle();
	this.w2.setTransform(1819.5,-10.5);

	this.w1 = new lib.Circle();
	this.w1.setTransform(2009.8,-11);

	// belt4
	this.belt4 = new lib.belt();
	this.belt4.setTransform(629.1,10.9,1,1,180,0,0,540.2,5);

	// belt1
	this.belt1 = new lib.belt();
	this.belt1.setTransform(1693.4,-35.6,1,1,0,0,0,540.2,5);

	// belt3
	this.belt3 = new lib.belt();
	this.belt3.setTransform(1782.3,10.9,1,1,180,0,0,540.2,5);

	// belt2
	this.belt2 = new lib.belt();
	this.belt2.setTransform(540.2,-35.5,1,1,0,0,0,540.2,5);

	this.addChild(this.belt2,this.belt3,this.belt1,this.belt4,this.w1,this.w2,this.w3,this.w4,this.w5,this.w6,this.w7,this.w8,this.w9,this.w10,this.w11,this.w12);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-40.6,2322.5,56.5);


(lib.hgsadcopy2 = function() {
	this.initialize();

	// Layer 4
	this.instance = new lib.Bitmap6();

	// bg
	this.instance_1 = new lib.Bitmap3_1();
	this.instance_1.setTransform(-0.4,82.1);

	// moving bubbles
	this.bubble2 = new lib.asdvcopy2();
	this.bubble2.setTransform(163.1,156.1,0.84,0.84,0,0,0,28.6,28.6);

	this.bubble3 = new lib.asdvcopy2();
	this.bubble3.setTransform(231.1,134.7,0.62,0.62,0,0,0,28.7,28.7);

	this.bubble1 = new lib.asdvcopy2();
	this.bubble1.setTransform(110.1,165.4,0.49,0.49,0,0,0,28.6,28.7);

	// Layer 1
	this.instance_2 = new lib.Bitmap5_1();

	this.addChild(this.instance_2,this.bubble1,this.bubble3,this.bubble2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.4,0,312,227.1);


(lib.FrontMachine = function() {
	this.initialize();

	// progressBOX
	this.progressBox = new lib.hgsad();
	this.progressBox.setTransform(1456,296.6,0.511,0.511,0,0,180,155.6,113.5);

	this.light2 = new lib.cAGEDLIGHTS();
	this.light2.setTransform(1509,98.2,0.294,0.294,0,0,0,57.1,107.2);

	this.light1 = new lib.cAGEDLIGHTS();
	this.light1.setTransform(1384,66.7,0.294,0.294);

	// scrapTxt
	this.scrapTxt = new lib.scrapMC();
	this.scrapTxt.setTransform(717.2,205.1,1,1,0,0,0,133.7,39.9);

	// inefficientTxt
	this.inefficientTxt = new lib.Inefficientcopy();
	this.inefficientTxt.setTransform(598.7,169.1,1,1,0,0,0,-1,-3);

	// trashBox
	this.trashBox = new lib.hgsadcopy2();
	this.trashBox.setTransform(578.6,243.9,0.5,0.5);

	// Layer 3
	this.chute = new lib.chute();
	this.chute.setTransform(1057.5,141.5,1,1,0,0,0,205.5,25.5);

	// Layer 2
	this.instance = new lib.Bitmap5();
	this.instance.setTransform(528.2,116);

	this.addChild(this.instance,this.chute,this.trashBox,this.inefficientTxt,this.scrapTxt,this.light1,this.light2,this.progressBox);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(528.2,63.8,1057,334.3);


(lib.PlayAreaView = function() {
	this.initialize();

	// boxmask
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#071218").ss(0.1,1,1).p("A59qoMAz7AAAIAAAUA5pKpIgUAA");
	this.shape.setTransform(504.7,606.9);

	// shutter
	this.shutter = new lib.Shutter();
	this.shutter.setTransform(519.2,393.5,0.5,0.5,0,0,0,24.1,34);

	// resetBtn
	this.resetBtn = new lib.Reset();
	this.resetBtn.setTransform(263.8,599.8,0.5,0.5,0,0,180,59.7,140.2);

	// questionPanelBG
	this.clickToClose = new lib.ClickToClose();
	this.clickToClose.setTransform(953.8,72.2,1,1,0,0,0,58,64.5);

	this.quesBG = new lib.FrontMachine();
	this.quesBG.setTransform(505.7,645.8,1,1,0,0,0,1056.9,283.4);

	// tiertopbg
	this.instance = new lib.Frontwall();
	this.instance.setTransform(510.4,541.8,0.5,0.5,0,0,0,1064.8,44.1);

	// moving box
	this.box1 = new lib.Box2();
	this.box1.setTransform(879.2,98.1,1,1,0,0,0,30,30);

	this.box2 = new lib.Box2();
	this.box2.setTransform(879.2,98.1,1,1,0,0,0,30,30);

	// belt
	this.belt = new lib.SVS();
	this.belt.setTransform(505.9,449.6,0.5,0.5,0,0,180,1056.5,179.2);
	this.belt.shadow = new cjs.Shadow("rgba(0,0,0,1)",0,0,10);

	// tires
	this.tiers = new lib.tires();
	this.tiers.setTransform(429.7,508,1,1,0,0,0,453.4,43.1);

	// gameBG
	this.instance_1 = new lib.gameBG("synched",0);
	this.instance_1.setTransform(512,384,1,1,0,0,0,512,384);

	this.addChild(this.instance_1,this.tiers,this.belt,this.box2,this.box1,this.instance,this.quesBG,this.clickToClose,this.resetBtn,this.shutter,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-133.1,0,1179.2,768);


// stage content:
(lib.assets = function() {
	this.initialize();

}).prototype = p = new cjs.Container();
p.nominalBounds = null;

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;